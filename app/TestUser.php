<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestUser extends Model
{
	use SoftDeletes;
	protected $fillable = ['user_id','test_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function test()
	{
		return $this->belongsTo('App\Test');
	} 
}
