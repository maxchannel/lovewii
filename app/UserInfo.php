<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInfo extends Model
{
	use SoftDeletes;
	
    protected $table = 'user_infos';
	protected $fillable = ['signo','user_id','home','kds','hvkds','tato','height','timido','muse', 'sch'];

    public function buscando()
    {
        return $this->belongsTo('App\Gender', 'search_id');
    }

    public function para()
    {
        return $this->belongsTo('App\Intention', 'para_id');
    }

    public function music()
    {
        return $this->hasOne('App\Music','id', 'muse');
    }

    public function school()
    {
        return $this->hasOne('App\Scholarship','id', 'sch');
    }

}
