<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLock extends Model
{
    use SoftDeletes;

    protected $table = 'user_locks';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
