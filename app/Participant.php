<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
	protected $table = 'participants';
	protected $fillable = ['conversation_id', 'user_id', 'last_read'];


	public function conversation()
	{
		return $this->belongsTo('App\Conversation');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function scopeMe($query, $user = null)
	{
		$user = $user ?: \Auth::user()->id;
		return $query->where('user_id', '=', $user);
	}
	
	public function scopeNotMe($query, $user = null)
	{
		$user = $user ?: \Auth::user()->id;
		return $query->where('user_id', '!=', $user);
	}
}