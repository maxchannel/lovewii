<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConversationSee extends Model
{
    use SoftDeletes;
    protected $fillable = ['conversation_id','user_id'];

    public function conversation()
	{
		return $this->belongsTo('App\Conversation');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	} 
}
