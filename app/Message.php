<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
	protected $table = 'messages';
	protected $touches = ['conversation'];
	protected $fillable = ['conversation_id', 'user_id', 'body'];

	public function conversation()
	{
		return $this->belongsTo('App\Conversation');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	} 

	public function participants()
	{
		return $this->hasMany('App\Participant', 'conversation_id', 'conversation_id');
	}

	public function recipients()
	{
		return $this->participants()->where('user_id', '!=', $this->user_id)->get();
	}
}