<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestParticipant extends Model
{
	protected $fillable = ['user_id','test_question_id','test_answer_id'];

	public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function question()
    {
        return $this->hasOne('App\TestQuestion');
    }

    public function answer()
    {
        return $this->hasOne('App\TestAnswer');
    }

}
