<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Verification extends Model
{
    use SoftDeletes;

    protected $table = 'verifications';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
