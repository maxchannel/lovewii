<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SearchUser extends Model
{
	use SoftDeletes;
	
    public function search()
    {
        return $this->belongsTo('App\Search');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
