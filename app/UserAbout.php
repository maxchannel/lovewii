<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAbout extends Model
{
    use SoftDeletes;

    protected $table = 'user_abouts';
    protected $fillable = ['prof', 'description'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
