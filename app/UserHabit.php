<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHabit extends Model
{
    use SoftDeletes;

    protected $table = 'user_habits';
    protected $fillable = ['user_id','smo','drin','gy','wo','drug','re','mo','con', 'pts','knd','her'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function smoke()
    {
        return $this->hasOne('App\Frequence','id', 'smo');
    }

    public function drink()
    {
        return $this->hasOne('App\Frequence','id', 'drin');
    }

    public function gym()
    {
        return $this->hasOne('App\Frequence','id', 'gy');
    }

    public function work()
    {
        return $this->hasOne('App\Frequence','id', 'wo');
    }

    public function drugs()
    {
        return $this->hasOne('App\Frequence','id', 'drug');
    }

    public function read()
    {
        return $this->hasOne('App\Frequence','id', 're');
    }

    public function movies()
    {
        return $this->hasOne('App\Frequence','id', 'mo');
    }

    public function concerts()
    {
        return $this->hasOne('App\Frequence','id', 'con');
    }

    public function pets()
    {
        return $this->hasOne('App\Frequence','id', 'pts');
    }

    public function kind()
    {
        return $this->hasOne('App\Frequence','id', 'knd');
    }

    public function listen()
    {
        return $this->hasOne('App\Frequence','id', 'her');
    }
}
