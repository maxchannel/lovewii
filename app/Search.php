<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Search extends Model
{
    use SoftDeletes;

    protected $table = 'searchs';
    protected $fillable = ['q'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
