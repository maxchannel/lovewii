<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotification extends Model 
{
	use SoftDeletes;
	
    protected $table = 'user_notifications';
	protected $fillable = ['m','user_id','v'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
