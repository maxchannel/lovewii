<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserNotificationConfig extends Model
{
	use SoftDeletes;
	
    protected $table = 'user_notifications_config';
    protected $fillable = ['not_following', 'not_follower'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
