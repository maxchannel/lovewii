<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSubscription extends Model
{
	use SoftDeletes;
    protected $fillable = ['renovation','plan','ends_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
	
}
