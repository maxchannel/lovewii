<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignComplete extends Model
{
    use SoftDeletes;
    protected $fillable = ['ip','campaign_id'];
}
