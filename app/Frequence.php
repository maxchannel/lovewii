<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Frequence extends Model
{
    use SoftDeletes;

    protected $table = 'frequences';
}
