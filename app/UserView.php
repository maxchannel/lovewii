<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserView extends Model
{
    use SoftDeletes;
    
    protected $table = 'user_views';
	protected $fillable = ['from_id','user_id','v'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function from()
    {
        return $this->belongsTo('App\User', 'from_id');
    }

}
