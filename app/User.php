<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $fillable = ['name', 'email', 'l', 'password', 'username', 'city', 'type', 'v'];
    protected $hidden = ['password', 'remember_token'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function about()
    {
        return $this->hasOne('App\UserAbout');
    }

    public function verification_pet()
    {
        return $this->hasOne('App\Verification');
    }

    public function habit()
    {
        return $this->hasOne('App\UserHabit');
    }

    public function subscription()
    {
        return $this->hasOne('App\UserSubscription');
    }

    public function lock()
    {
        return $this->hasOne('App\UserLock');
    }

    public function logins()
    {
        return $this->hasMany('App\UserLogin');
    }

    public function tests()
    {
        return $this->hasMany('App\TestUser');
    }

    public function test($id)
    {
        return $this->tests()->where('test_id',$id)->count();
    }

    public function blocks()
    {
        return $this->hasMany('App\UserBlock');
    }

    public function images()
    {
        return $this->hasMany('App\UserImage');
    }

    public function info()
    {
        return $this->hasOne('App\UserInfo');
    }

    public function findByUsername($username)
    {
        return $this->model->where('username', '=', $username)->first();
    }

    public function notifications()
    {
        return $this->hasMany('App\UserNotification');
    }

    public function conversation_sees()
    {
        return $this->hasMany('App\ConversationSee');
    }
    public function conversation_sees_get($id)
    {
        return $this->conversation_sees->where('conversation_id', $id)->first();
    }

    public function views()
    {
        return $this->hasMany('App\UserView');
    }

    public function notifications_config()
    {
        return $this->hasOne('App\UserNotificationConfig');
    }

    public function privacy_config()
    {
        return $this->hasOne('App\UserPrivacyConfig');
    }

    public function del_horoscopes()
    {
        return $this->hasMany('App\UserHoroscope');
    }

    public function userhoroscope()
    {
        return $this->hasOne('App\UserHoroscope');
    }

    public function confirmation()
    {
        return $this->hasOne('App\UserConfirmation');
    }

    public function horoscopes()
    {
       return $this->belongsToMany('App\Horoscope', 'user_horoscopes','user_id', 'horoscope_id')->withPivot('user_id');
    }

    public function horoscope()
    {
        return $this->horoscopes()->first();
    }

    public function getNotifications()
    {
        return $this->notifications()->where('v',0)->count();
    }

    public function getViews()
    {
        return $this->views()->where('v',0)->count();
    }

    public function participants()
    {
        return $this->hasMany('App\Participant');
    }

    public function getParticipants()
    {
        return $this->participants()->where('v',0)->count();
    }

    public function testParticipants()
    {
        return $this->hasMany('App\TestParticipant', 'user_id');
    }

    public function getTestParticipants()
    {
        return $this->testParticipants()->count();
    }

    public function es($type)
    {
        return $this->t === $type;
    }

    public function isAdmin()
    {
        return $this->type === 'admin';
    }

    public function whichComp($id)
    {
        $h = HoroscopeCompatibility::find($id);
        return $h->type->type;
    }

    public function whichCompt($hors_id, $with_id)
    {
        $h = HoroscopeCompatibility::where('horoscope_id',$hors_id)->where('with_id',$with_id)->get()->first();
        if($h) 
        {
            return $h->type->type;
        }
    }

    public function isBlocked($id)
    {
        if($this->blocks()->where('other_id',$id)->count() > 0) 
        {
            abort(404);
        }
    }

    public function isPremium()
    {
        if($this->subscription->ends_at == NULL or $this->subscription->ends_at < date('Y-m-d')) 
        {
            return false;
        }else
        {
            return true;
        }
    }

    public function isNormal()
    {
        if($this->subscription->ends_at == NULL or $this->subscription->ends_at < date('Y-m-d')) 
        {
            return true;
        }else
        {
            return false;
        }
    }
}
