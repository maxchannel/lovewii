<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRank extends Model
{
    use SoftDeletes;
    protected $table = 'user_ranks';
    protected $fillable = ['user_id', 'name', 'rank'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
