<?php

namespace App\Http\Middleware;

use Closure;

class noAbout
{
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->lock()->count() == 0) 
        {
            return \Redirect::route('home');
        }

        return $next($request);
    }
}
