<?php namespace App\Http\Middleware;

use Closure;
use App\Visitor;
use App\VisitorAll;

class StoreUserInfo
{
    public function handle($request, Closure $next)
    {
        $visitor = Visitor::firstOrCreate(['ip' => \Request::ip()]);

        /*
        $v2 = new VisitorAll;
        $v2->ip = \Request::ip();
        $v2->save();
        */

        return $next($request);
    }
}
