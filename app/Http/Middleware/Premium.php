<?php namespace App\Http\Middleware;

use Closure;

class Premium
{
    public function handle($request, Closure $next)
    {
        if(\Auth::check())
        {
            if(\Auth::user()->subscription->ends_at == NULL or \Auth::user()->subscription->ends_at < date('Y-m-d')) 
            {
                return \Redirect::route('gopremium');
            }
        }

        return $next($request);
    }
}
