<?php namespace App\Http\Middleware;

use Closure;

class noCard
{
    public function handle($request, Closure $next)
    {
    	if(\Auth::check())
    	{
            if(\Auth::user()->subscription->ends_at != NULL) 
            {
                return \Redirect::route('home');
            }
        }

        return $next($request);
    }
}
