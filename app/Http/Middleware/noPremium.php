<?php namespace App\Http\Middleware;

use Closure;

class noPremium
{
    public function handle($request, Closure $next)
    {
        if(\Auth::check())
        {
            if(\Auth::user()->subscription->ends_at > date('Y-m-d')) 
            {
                return \Redirect::route('home');
            }
        }

        return $next($request);
    }
}
