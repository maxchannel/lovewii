<?php

namespace App\Http\Middleware;

use Closure;

class About
{
    public function handle($request, Closure $next)
    {
    	if(\Auth::check())
    	{
            if(\Auth::user()->lock()->count() > 0) 
            {
                return \Redirect::route('first_about');
            }
        }

        return $next($request);
    }
}
