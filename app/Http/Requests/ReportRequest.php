<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'report' => 'required',
            'xyz' => 'required|numeric',
            'comments' => 'min:10|max:800'
        ];
    }
}
