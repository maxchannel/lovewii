<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'phone' => 'required|numeric',
        ];
        
        /*
        if(\Auth::guest())
        {
            $rules['email'] = 'required|email|unique:users|max:60';
            $rules['password'] = 'required|min:6|max:25';
        } 
        */

        return $rules;
    }
}
