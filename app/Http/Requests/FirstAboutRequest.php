<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FirstAboutRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'city' => 'required|min:2|max:90',
            'description' => 'required|min:1|max:800',
            'horoscope_id' => 'required|numeric',
            'search_id' => 'required|numeric',
            'para_id' => 'required|numeric',
            'country_id' => 'required|numeric'
        ];
        
        if(\Auth::user()->info->bth == NULL)
        {
            $rules['day'] = 'required|numeric';
            $rules['month'] = 'required|numeric';
            $rules['year'] = 'required|numeric';
        } 

        if(\Auth::user()->username == NULL)
        {
            $rules['username'] = 'required|unique:users|alpha_num|min:3|max:16';
        } 

        if(\Auth::user()->gender_id == NULL)
        {
            $rules['gender_id'] = 'required|numeric';
        } 

        return $rules;
    }
}
