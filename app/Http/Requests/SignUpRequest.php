<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:40',
            'email' => 'required|email|unique:users|max:60',
            'password' => 'required|min:6|max:25',
            'username' => 'required|unique:users|alpha_num|min:3|max:16',
            'captcha' => 'required|captcha',
            'gender_id' => 'required|numeric',
            'day' => 'required|numeric',
            'month' => 'required|numeric',
            'year' => 'required|numeric',
        ];
    }
}
