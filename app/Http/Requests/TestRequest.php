<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [''];    
    
        foreach($this->request->get('item') as $a => $z)
        {
            $rules['item.'.$a] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('item') as $key => $val)
        {
            $messages['item.'.$key] = 'Respuesta '.$key.'" es obligatoria';
        }
        return $messages;
    }
}
