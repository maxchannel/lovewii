<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaypalController extends Controller
{
    public function paypal($slug)
    {   
    	switch($slug) 
    	{
    		case 'anual':
    			$mode = 'anual';
    			break;

    		case 'semester':
    			$mode = 'semester';
    			break;

    		case 'month':
    			$mode = 'month';
    			break;
    		
    		default:
    			abort(404);
    			break;
    	}

        //seguimiento
        $camp = 0;
        if(\Request::input('camp')) 
        {
            $pay = CampaignPayment::firstOrCreate([
                'campaign_id' => \Request::input('camp'),
                'plan' => $slug,          
                'ip' => \Request::ip()
            ]);
            $camp = \Request::input('camp');
        }

        return view('payment.paypal', compact('mode', 'camp'));
    }

    public function paypal_post()
    {
    	
    }

}


