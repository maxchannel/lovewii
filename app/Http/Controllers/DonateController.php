<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;

class DonateController extends Controller
{
    public function index($slug)
    {  
    	switch($slug) 
    	{
    		case 'anual':
    			$mode = 'anual';
    			break;

    		case 'semester':
    			$mode = 'semester';
    			break;

    		case 'month':
    			$mode = 'month';
    			break;
    		
    		default:
    			abort(404);
    			break;
    	}

        return view('payment.donate', compact('mode', 'camp', 'pais'));
    }

    public function payment(PaymentRequest $request)
    {
    	switch($request->input('xyz')) 
       	{
       		case 'xyz1':
       			$precio = 9600;
                $moneda = "USD";
       			break;    

      		case 'xyz2':
       			$precio = 5400;
                $moneda = "USD";
       			break;    

      		case 'xyz3':
       			$precio = 1200;
                $moneda = "USD";
       			break;
       		
       		default:
       			abort(404);
       			break;
       	}
    	\Conekta::setApiKey(ENV('CONEKTA_KEY'));

    	try {
            $charge = \Conekta_Charge::create(array(
              'description'=> 'For1Ever Complete',
              'reference_id'=> 'complete-for1ever',
              'amount'=> $precio,
              'currency'=>$moneda,
              'card'=> $request->input('conektaTokenId'),
              'details'=> array(
                "name"=> $request->input('name'),
                "phone"=> $request->input('phone'),
                "email"=> $request->input('email'),
                'customer'=> array(
                  'corporation_name'=> 'Conekta Inc.',
                  'logged_in'=> true,
                  'successful_purchases'=> 14,
                  'created_at'=> 1379784950,
                  'updated_at'=> 1379784950,
                  'offline_payments'=> 4,
                  'score'=> 9
                ),
                'line_items'=> array(
                  array(
                    'name'=> 'For1Ever Premium',
                    'description'=> 'For1Ever Complete',
                    'unit_price'=> $request->input('price_'),
                    'quantity'=> 1,
                    'sku'=> 'For1Ever',
                    'type'=> 'suscription'
                  )
                ),
                'billing_address'=> array(
                  'street1'=>$request->input('direction'),
                  'street2'=> null,
                  'street3'=> null,
                  'city'=> 'Darlington',
                  'state'=>'NJ',
                  'zip'=> '10192',
                  'country'=> 'Mexico',
                  'phone'=> $request->input('phone'),
                  'email'=> $request->input('email'),
                )
              )
            ));             

            return \Redirect::back()->with('message', 'Tu pago fue procesado con Éxito, Muchas Gracias. Apareceremos como: Pagos Conekta');
        }catch(\Exception $e) {
            return \Redirect::back()->with('e1', $e->message_to_purchaser);
        }
    }
}
