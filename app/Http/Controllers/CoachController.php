<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class CoachController extends Controller
{
    public function coach()
    {   
        return view('user.premium.coach');
    }   
}
