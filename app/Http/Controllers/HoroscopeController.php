<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Horoscope;
use App\HoroscopeDay;
use App\HoroscopeCompatibility;
use App\UserHoroscope;
use Aws\S3\S3Client;
use Carbon\Carbon;

class HoroscopeController extends Controller
{
    public function all()
    {   
        $days = HoroscopeDay::where('fecha','<=',Carbon::today())->orderBy('fecha','DESC')->paginate(5);

        return view('horoscope.all', compact('days'));
    }

    public function open($sign)
    {   
        $horoscope = Horoscope::where('name', $sign)->first();
        $this->notFoundUnless($horoscope);
        $days = HoroscopeDay::where('horoscope_id', $horoscope->id)
            ->where('fecha','<=',Carbon::today())->orderBy('fecha','DESC')->paginate(2);

        return view('horoscope.open', compact('horoscope', 'days'));
    }

    public function individal($sign, $date)
    {   
        $horoscope = Horoscope::where('name', $sign)->first();
        $this->notFoundUnless($horoscope);
        $day = HoroscopeDay::where('horoscope_id', $horoscope->id)->where('fecha',$date)->first();
        $this->notFoundUnless($day);
        //Fecha
        $fecha = Carbon::createFromFormat('Y-m-d',$date);
        $fecha = $fecha->toFormattedDateString();   

        return view('horoscope.individual', compact('horoscope', 'day', 'fecha'));
    }

    public function horoscope()
    {   
        if(\Auth::user()->userhoroscope->horoscope_id != "")
        {
        	$horoscope = Horoscope::find(\Auth::user()->horoscope()->id);
            $this->notFoundUnless($horoscope);
            $days = HoroscopeDay::where('horoscope_id', $horoscope->id)->orderBy('fecha','DESC')->paginate(2);

            return view('horoscope.horoscope', compact('horoscope', 'days'));
        }else
        {
            abort(404);
        }
    }

    public function horoscope_match_prev()
    { 
        return \Redirect::route('horoscope_match', [str_slug(\Input::get('sign'))]);
    }

    public function horoscope_match($sign)
    {   
        //Seleccionando horoscopo
        $horoscope = Horoscope::where('name', $sign)->first();
        $this->notFoundUnless($horoscope);

    	//Compatibilidad con el horoscopo seleccionado
    	$es = HoroscopeCompatibility::
    	    where('horoscope_id', $horoscope->id)
    	    ->where('type_id', 1)
            ->get();
        $en = HoroscopeCompatibility::
            where('horoscope_id', $horoscope->id)
            ->where('type_id', 2)
            ->get();

        $horoscopes = $es->merge($en);
        //return $horoscopes;

    	//Seleccionando usuarios en base a su horoscopo compatible
        $consulta = UserHoroscope::orderBy('created_at', 'DESC');

    	foreach($horoscopes as $i => $hor) 
    	{
    		$consulta->orWhere('horoscope_id', $hor->with_id);
    	}
    	$results = $consulta->paginate(25);

        //return $results;

    	//Av
    	$s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($results as $i => $h)
        {
            $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$h->user->avatar);
        }

        return view('horoscope.compatibility', compact('results', 'avatar', 'horoscope', 'p'));
    }
}
