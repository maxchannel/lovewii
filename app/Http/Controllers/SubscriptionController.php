<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubscriptionRequest;
use App\Subscription;

class SubscriptionController extends Controller
{
    public function subscription_update(SubscriptionRequest $request)
    {
        $subscription = new Subscription;
        $subscription->fill($request->all());
        $subscription->save();

        return \Redirect::back()->with('message', 'Enviado, no olvides confirmar tu email');
    }
}
