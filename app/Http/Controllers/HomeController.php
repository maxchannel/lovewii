<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Aws\S3\S3Client;
use App\User;
use App\Horoscope;

class HomeController extends Controller
{
    public function index($mode="")
    {   
        $horos = Horoscope::orderBy('name', 'ASC')->pluck('name', 'name')->toArray();

        if(\Auth::check())
        {
            $consulta = User::inRandomOrder();
            //Generos que busca el auth
            if(\Auth::user()->info->search_id)
            {
                $consulta->where('gender_id', \Auth::user()->info->search_id);
            }    

            //Pais de donde es el auth
            $consulta->where('country_id', \Auth::user()->country_id);    
            $users = $consulta->paginate(25);    

            if(count($users) == 0)//Sino hay del mismo pais
            {
                $users = User::inRandomOrder()->where('gender_id', \Auth::user()->info->search_id)->paginate(25);
            }    

            $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
            foreach($users as $i => $user)
            {
                $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);
            }    

            return view('welcome', compact('users', 'avatar', 'horos'));    
        }else
        {
            $users = User::inRandomOrder()->take(3)->get();
            $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
            foreach($users as $i => $user)
            {
                $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);
            }
            return view('welcome', compact('users', 'avatar', 'horos'));    
        }
    	
    }

    public function welcome()
    {   
        return view('public.first_welcome');
    }
}
