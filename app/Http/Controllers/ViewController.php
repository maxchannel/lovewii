<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Aws\S3\S3Client;
use App\UserView;

class ViewController extends Controller
{
    public function views()
    {
    	//Cambiando a visto
        if(\Auth::user()->getViews() > 0)
        {
            UserView::where('user_id', \Auth::user()->id)->update(array('v' => 1));
        }
        $views = UserView::where('user_id', \Auth::user()->id)->orderBy('created_at','DESC')->paginate(25);


        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($views as $i => $view)
        {
            $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$view->from->avatar);
        }
        return view('user.premium.views', compact('views', 'avatar'));
    }

    public function destroy_view($id)
    {
        $n = UserView::find($id);
        $n->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }
}
