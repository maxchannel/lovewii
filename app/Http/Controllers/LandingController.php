<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Campaign;

class LandingController extends Controller
{
    public function index()
    {   
        $location = \GeoIP::getLocation(\Request::ip());
        $pais = $location['isoCode'];

        $camp = 0;
        //inicio del seguimiento
        if(\Request::input('src') && \Request::input('ad') && \Request::input('mode')) 
        {
            $camp = Campaign::firstOrCreate([
                'source' => \Request::input('src'),
                'ad' => \Request::input('ad'), 
                'mode' => \Request::input('mode'),           
                'ip' => \Request::ip()
            ]);
        }
        

        return view('payment.pricing', compact('camp','pais'));
    }

    public function horoscopo()
    {   
        return view('landing.horoscopo');
    }

    public function horoscopo2($mode="")
    {   
    	if($mode == "")
    	{
    		return view('landing.horoscopo2');
    	}elseif($mode == "dates")
    	{
    		return view('landing.horoscopo3');
    	}else
    	{
    		return \Redirect::route('horoscopo_promo');
    	}

        
    }


}