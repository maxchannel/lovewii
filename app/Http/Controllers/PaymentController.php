<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PaymentRequest;
use App\UserSubscription;
use App\User;
use App\UserNotificationConfig;
use App\UserPrivacyConfig;
use App\UserHoroscope;
use App\UserInfo;
use App\UserAbout;
use App\UserLock;
use App\UserHabit;
use App\UserConfirmation;
use App\UserNotification;
use App\CampaignPayment;
use App\CampaignComplete;

class PaymentController extends Controller
{
    public function index($slug)
    {  
    	switch($slug) 
    	{
    		case 'anual':
    			$mode = 'anual';
    			break;

    		case 'semester':
    			$mode = 'semester';
    			break;

    		case 'month':
    			$mode = 'month';
    			break;
    		
    		default:
    			abort(404);
    			break;
    	}

        $location = \GeoIP::getLocation(\Request::ip());
        $pais = $location['isoCode'];

        //seguimiento
        $camp = 0;
        if(\Request::input('camp')) 
        {
            $pay = CampaignPayment::firstOrCreate([
                'campaign_id' => \Request::input('camp'),
                'plan' => $slug,          
                'ip' => \Request::ip()
            ]);
            $camp = \Request::input('camp');
        }

        return view('payment.payment', compact('mode', 'camp', 'pais'));
    }

    public function payment(PaymentRequest $request)
    {   
        //Si hay campaña
        $camp = 0;
        if(\Request::input('camp')) 
        {
            $camp = \Request::input('camp');
        }

        $location = \GeoIP::getLocation(\Request::ip());
        $pais = $location['isoCode'];

    	//Precio
        if($pais == "MX")
        {
            $plan_id = "mensualMX";
            switch($request->input('xyz')) 
            {
                case 'xyz1':
                    $precio = 94800;
                    $moneda = "MXN";
                    $plan = "annual";
                    break;    

                case 'xyz2':
                    $precio = 53400;
                    $moneda = "MXN";
                    $plan = "semester";
                    break;    

                case 'xyz3':
                    $precio = 9900;
                    $moneda = "MXN";
                    $plan = "monthly";
                    break;
                
                default:
                    abort(404);
                    break;
            }
        }else
        {
            $plan_id = "mensual";
        	switch($request->input('xyz')) 
        	{
        		case 'xyz1':
        			$precio = 9600;
                    $moneda = "USD";
                    $plan = "annual";
        			break;    

        		case 'xyz2':
        			$precio = 5400;
                    $moneda = "USD";
                    $plan = "semester";
        			break;    

        		case 'xyz3':
        			$precio = 1200;
                    $moneda = "USD";
                    $plan = "monthly";
        			break;
        		
        		default:
        			abort(404);
        			break;
        	}
        }

        \Conekta::setApiKey(ENV('CONEKTA_KEY'));

        if($request->input('xyz') == 'xyz3')//Recurrente mensual
        {
            //Creando el cliente
            try{
                $customer = \Conekta_Customer::create(array(
                    "name"=> \Auth::user()->name,
                    "phone"=> $request->input('phone'),
                    "email"=> \Auth::user()->email,
                    "cards"=>  array($_POST['conektaTokenId']) 
                ));
            }catch (\Exception $e){
                return \Redirect::back()->with('e1', $e->message_to_purchaser);
            }

            //Creando subscription
            $subscription = $customer->createSubscription(array(
                "plan_id"=> $plan_id
            ));
            if($subscription->status == 'active') 
            { 
                //Payment
                $subs = UserSubscription::find(\Auth::user()->subscription->id);    

                if($plan == "annual")//+366
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 366 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 366 days'));
                    }
                    
                }elseif($plan == "semester")//+183
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 183 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 183 days'));
                    }
                }elseif($plan == "monthly")
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 31 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 31 days'));
                    }
                }
                $subs->plan = $plan;
                $subs->conekta_customer = $customer->id;
                $subs->conekta_subscription = $subscription->id;
                $subs->save();
                //Payment        
                
                return \Redirect::route('payment_success', ['camp'=>$camp])->with('message', 'Cobro exitoso, ahora tienes acceso a la Plataforma Completa');
            }elseif ($subscription->status == 'past_due') 
            {        
                return \Redirect::back()->with('e1', 'Error al crear suscripción, comunicate con nosotros.');
            }

        }else
        {
            try {
                $charge = \Conekta_Charge::create(array(
                  'description'=> 'For1Ever Complete',
                  'reference_id'=> 'complete-for1ever',
                  'amount'=> $precio,
                  'currency'=>$moneda,
                  'card'=> $request->input('conektaTokenId'),
                  'details'=> array(
                    "name"=> \Auth::user()->name,
                    "phone"=> $request->input('phone'),
                    "email"=> \Auth::user()->email,
                    'customer'=> array(
                      'corporation_name'=> 'Conekta Inc.',
                      'logged_in'=> true,
                      'successful_purchases'=> 14,
                      'created_at'=> 1379784950,
                      'updated_at'=> 1379784950,
                      'offline_payments'=> 4,
                      'score'=> 9
                    ),
                    'line_items'=> array(
                      array(
                        'name'=> 'For1Ever Premium',
                        'description'=> 'For1Ever Complete',
                        'unit_price'=> $request->input('price_'),
                        'quantity'=> 1,
                        'sku'=> 'For1Ever',
                        'type'=> 'suscription'
                      )
                    ),
                    'billing_address'=> array(
                      'street1'=>'77 Mystery Lane',
                      'street2'=> 'Suite 124',
                      'street3'=> null,
                      'city'=> 'Darlington',
                      'state'=>'NJ',
                      'zip'=> '10192',
                      'country'=> 'Mexico',
                      'phone'=> $request->input('phone'),
                      'email'=> \Auth::user()->email
                    )
                  )
                )); 

                //Payment
                $subs = UserSubscription::find(\Auth::user()->subscription->id);
                $subs->user_id = \Auth::user()->id;

                if($plan == "annual")//+366
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 366 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 366 days'));
                    }
                    
                }elseif($plan == "semester")//+183
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 183 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 183 days'));
                    }
                }elseif($plan == "monthly")
                {
                    if($subs->ends_at == NULL)
                    {
                        $subs->ends_at = date('Y-m-d', strtotime(date('Y-m-d').' + 31 days'));
                    }else
                    {
                        $subs->ends_at = date('Y-m-d', strtotime($subs->ends_at.' + 31 days'));
                    }
                }
                $subs->plan = $plan;
                $subs->save();
                //Payment

                return \Redirect::route('payment_success', ['camp'=>$camp])->with('message', 'Cobro exitoso, ahora tienes acceso a la Plataforma Completa');
            }catch(\Exception $e) {
                return \Redirect::back()->with('e1', $e->message_to_purchaser);
            }
        }


    }

    public function success()
    {   
        //seguimiento
        if(\Request::input('camp')) 
        {
            $complete = CampaignComplete::firstOrCreate([
                'campaign_id' => \Request::input('camp'),          
                'ip' => \Request::ip()
            ]);
        }

        return view('payment.success');
    }

    public function gopremium()
    {   
        return view('payment.gopremium');
    }
}
