<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Aws\S3\S3Client;
use App\User;
use App\UserView;
use DateTime;

class ProfileController extends Controller
{
    public function index($username)
    {
    	$user = User::where('username',$username)->first();
		$this->notFoundUnless($user);

        //View
        if(\Auth::check() && \Auth::user()->id != $user->id)
        {
            if(UserView::where('from_id', \Auth::user()->id)->count() == 0)
            {
                $v = new UserView;
                $v->user_id = $user->id;
                $v->from_id = \Auth::user()->id;
                $v->save();
            }

            //Bloqueados
            $user->isBlocked(\Auth::user()->id);
            \Auth::user()->isBlocked($user->id);
        }

    	$s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        $avatar = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);

        //AWS
        foreach($user->images as $i => $image)
        {
            $temp[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/albums/'.$image->route);
        }
        //Random
        $users = User::where('gender_id',$user->gender_id)->inRandomOrder()->take(4)->get();
        foreach($users as $i => $n)
        {
            $ing[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$n->avatar);
        }
        //AWS

        $from = new DateTime($user->info->bth);
        $to = new DateTime('today');
        $age = $from->diff($to)->y;        

        return view('user.profile.profile', compact('avatar', 'user', 'temp', 'ing', 'users', 'age'));
    }
}
