<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\UserImage;
use App\Country;
use Aws\S3\S3Client;

class ImageController extends Controller
{
    public function image()
    {
        $user = User::find(\Auth::user()->id);

        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($user->images as $i => $image)
        {
            $temp[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/albums/'.$image->route);
        }

        return view('user.large.images', compact('user', 'countries', 'temp'));
    }

    public function image_update($id)
    {
        $user = User::find($id);

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:6000'];
                $validation = \Validator::make(['file'=> $file], $rules);    

                if(!$validation->fails()) 
                {
                    //Limitando el numero de imagenes
                    if($user->images()->count() < 25)
                    {   
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(10).".".$extension;    
                        $path = storage_path().'/uploads/'.$newName;

                        //Insert in db
                        $image = new UserImage;
                        $image->user_id = $user->id;
                        $image->route = $newName;
                        $image->save(); 

                        ///Move file to images/post
                        $file->move(storage_path().'/uploads/', $newName);

                        //Resize
                        \Image::make($path)->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path);

                        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
                        $result = $s3->putObject(array(
                            'Bucket'     => config('filesystems.disks.s3images.bucket'),
                            'SourceFile' => $path,
                            'Key'        => 'images/albums/'.$newName,
                            'ACL'        => 'public-read'
                        ));

                        \File::delete($path);
                    }else
                    {
                        return \Redirect::back()->with('image-message', 'Alcanzaste el limite de 20 imágenes');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Solo se procesarón las imágenes menores a 6MB');
                }
            }
        }else
        {
            return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
        }

        $user->save();
        return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu perfil']);
    }

    public function destroy($id)//Delete
    {
        $img = UserImage::find($id);
        $this->notFoundUnless($img);

        //Delete
        \Storage::disk('s3images')->delete('images/albums/'.$img->route);
        $img->delete();//Delete db
        
        return \Redirect::back()->with('message', 'Eliminada con exito');
    }
}
