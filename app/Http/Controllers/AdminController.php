<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Horoscope;
use App\HoroscopeDay;
use App\Contact;
use App\Verification;
use App\User;
use App\UserReport;
use App\UserImage;
use App\Subscription;
use App\Message;
use App\Http\Requests\AdminHoroscopeRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\ReportRequest;
use App\Http\Requests\VerifyRequest;
use Aws\S3\S3Client;

class AdminController extends Controller
{
    public function horoscope()
    {
        $horoscopes = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('admin.horoscope', compact('horoscopes'));
    }

    public function horoscope_update(AdminHoroscopeRequest $request)
    {
        $user = new HoroscopeDay;
        $user->fill($request->all());
        $user->save();

        return \Redirect::back()->with('message', 'Enviado');
    }

    public function delete_horoscope($sign)
    {
        $horoscope = Horoscope::where('name', $sign)->first();
        $this->notFoundUnless($horoscope);
        $days = HoroscopeDay::where('horoscope_id', $horoscope->id)->paginate(25);

        return view('admin.horoscope_destroy', compact('days'));
    }

    public function destroy_horoscope($id)
    {
        $n = HoroscopeDay::find($id);
        $n->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }


    public function admin_contact()
    {
        $contacts = Contact::orderBy('created_at', 'DESC')->paginate(25);

        return view('admin.contact', compact('contacts'));
    }

    public function contact()
    {
        return view('public.contact');
    }

    public function contact_update(ContactRequest $request)
    {
        $user = new Contact;
        $user->fill($request->all());
        $user->save();

        return \Redirect::back()->with('message', 'Enviado con Éxito');
    }

    public function subscriptions()
    {
        $subscriptions = Subscription::orderBy('created_at', 'DESC')->paginate(30);

        return view('admin.subscription', compact('subscriptions'));
    }

    public function stats()
    {
        $users = User::count();
        $subscriptions = Subscription::count();
        $contacts = Contact::count();
        $reports = UserReport::count();
        $u_images = UserImage::count();
        $messages = Message::count();

        return view('admin.stat', compact('subscriptions', 'users', 'contacts', 'reports', 'u_images', 'messages'));
    }

    public function contact_report(ReportRequest $request)
    {
        $user = User::find($request->input('xyz'));
        $this->notFoundUnless($user);

        $report = new UserReport;
        $report->fill($request->all());
        $report->user_id = $request->input('xyz');
        $report->ip = \Request::ip();
        $report->save();

        return \Redirect::back()->with('message', 'Reporte Enviado');
    }

    public function verify()
    {
        $verifies = Verification::orderBy('created_at', 'DESC')->paginate(25);

        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($verifies as $i => $very)
        {
            $temp[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/verifications/'.$very->route);
        }

        return view('admin.verify',compact('verifies','temp'));
    }

    public function admin_verify_destroy($id)
    {
        $v = Verification::find($id);
        //Verificando
        User::where('id',$v->user_id)->update(['v' => 1]);
        $v->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function verified()
    {
        return view('admin.verifysend');
    }

    public function verify_update()
    {
        $file = \Input::file('file');
        $validation = \Validator::make(['file'=> $file], ['file' => 'required|image|max:7000']);
        
        if(!$validation->fails())
        {
            //Image
            $file = \Input::file('file');
            $extension = $file->getClientOriginalExtension(); 
            $newName = str_random(15).".".$extension;    
            $file->move(storage_path().'/uploads/', $newName);
            $path = storage_path().'/uploads/'.$newName;

            //Resize
            \Image::make($path)->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
           
            $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
            try {
                $s3->putObject([
                    'Bucket'     => config('filesystems.disks.s3images.bucket'),
                    'Key'        => 'images/verifications/'.$newName,
                    'SourceFile' => $path,
                    'ACL'    => 'public-read',
                ]);
            } catch (Aws\Exception\S3Exception $e) {
                echo "There was an error uploading the file.\n";
            }     

            \File::delete($path);
            //Image

            $very = new Verification;
            $very->user_id = \Auth::user()->id;
            $very->route = $newName;
            $very->save();

            return \Redirect::back()->with('message', 'Revisaremos tu solicitud, y en breve nos pondremos en contacto.');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

}
