<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Test;
use App\TestUser;
use App\TestParticipant;
use App\Http\Requests\TestRequest;

class TestController extends Controller
{
    public function index()
    {   
    	$tests = Test::orderBy('created_at', 'DESC')->paginate(10);

        return view('tests.tests', compact('tests'));
    }   

    public function take($id)
    {   
    	$test = Test::find($id);
        $this->notFoundUnless($test);

        return view('tests.take', compact('test'));
    }  

    public function take_update($id, TestRequest $request)
    {   
        $test = Test::find($id);
        $this->notFoundUnless($test);

        $hh = new TestUser;
        $hh->user_id = \Auth::user()->id;
        $hh->test_id = $test ->id;
        $hh->save();

        $values = $request->input('item');

        foreach($test->questions as $i => $q)
        {
            $part = new TestParticipant;
            $part->user_id = \Auth::user()->id;
            $part->test_question_id = $q->id;
            $part->test_answer_id = $values[$i];
            $part->save();
        }

        return \Redirect::back()->with('message', 'Test Hecho');
    }   
}
