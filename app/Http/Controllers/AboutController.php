<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Country;
use App\Gender;
use App\Intention;
use App\Horoscope;
use App\UserAbout;
use App\UserHoroscope;
use App\UserInfo;
use App\UserLock;
use App\UserHabit;
use App\User;
use App\Frequence;
use App\Music;
use App\Conversation;
use App\ConversationSee;
use App\Participant;
use App\Message;
use App\Scholarship;
use App\Http\Requests\FirstAboutRequest;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;

class AboutController extends Controller
{
    public function index()
    {   
    	$countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $horoscopes = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $genders = Gender::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $intentions = Intention::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $frequences = Frequence::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $music = Music::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $schools = Scholarship::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

    	return view('user.large.about', compact('countries', 'horoscopes', 'genders', 'intentions', 'frequences','music','schools'));
    }

    public function avatar_first()
    {   
        $user = User::find(\Auth::user()->id);

        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        $imh = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);

        return view('user.large.avatar', compact('user', 'imh'));
    }

    public function avatar_first_p()
    {
        $file = \Input::file('file');
        $validation = \Validator::make(['file'=> $file], ['file' => 'required|image|max:7000']);
        
        if(!$validation->fails())
        {
            //Update database and delete old image
            $user = User::find(\Auth::user()->id);

            //Image
            $file = \Input::file('file');
            $extension = $file->getClientOriginalExtension(); 
            $newName = str_random(15).".".$extension;    
            $file->move(storage_path().'/uploads/', $newName);
            $path = storage_path().'/uploads/'.$newName;

            //Resize
            \Image::make($path)->resize(1100, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
           
            $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
            try {
                $s3->putObject([
                    'Bucket'     => config('filesystems.disks.s3images.bucket'),
                    'Key'        => 'images/profile/'.$newName,
                    'SourceFile' => $path,
                    'ACL'    => 'public-read',
                ]);
            }catch (Aws\Exception\S3Exception $e) {
                echo "There was an error uploading the file.\n";
            }     

            if($user->avatar != "default.png")
            {
                \Storage::disk('s3images')->delete('images/profile/'.$user->avatar);
            }
            \File::delete($path);
            //Image

            $user->avatar = $newName;
            $user->save();  

            return \Redirect::route('welcome');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

    public function create(FirstAboutRequest $request)
    {   
        $user = User::find(\Auth::user()->id);
        $user->fill($request->all());
        //City
        $user->city = $request->input('city');
        $user->country_id = $request->input('country_id');
        $user->save();

        //Description
    	$abo = UserAbout::find(\Auth::user()->about->id);
        $abo->fill($request->all());
        $abo->description = $request->input('description');
        $abo->save();

        //Description
        $hors = UserHoroscope::find(\Auth::user()->userhoroscope->id);
        $hors->fill($request->all());
        $hors->horoscope_id = $request->input('horoscope_id');
        $hors->save();

        //Habit
        $habit = UserHabit::find(\Auth::user()->habit->id);
        $habit->fill($request->all());
        $habit->save();

        //Info
        $info = UserInfo::find(\Auth::user()->info->id);
        $info->fill($request->all());
        $bth = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
        $info->bth = $bth;
        $info->search_id = $request->input('search_id');
        $info->para_id = $request->input('para_id');
        $info->save();

        //Quintdo lock
        $lock = UserLock::find(\Auth::user()->lock->id);
        $lock->delete();

        //Mensaje ayuda
        $converse = Conversation::create([
        ]);
        if($user->gender_id == 1)
        {
            $genero=2;
        }else
        {
            $genero=1;
        }

        ConversationSee::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$genero,
            
        ]);
        ConversationSee::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$user->id,
        ]);

        Participant::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$genero,
            
        ]);
        Participant::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$user->id,
            
        ]);
        Message::create([
            'body'=>'Hola te doy la Bienvenida a For1Ever, cualquier duda que tengas me puedes escribir en cualquier momento, :)',
            'conversation_id'=>$converse->id,
            'user_id'=>$genero,
        ]);
        //Mensaje ayuda
        
    	return \Redirect::route('avatar_first');
    }

    public function edit()
    {   
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $horoscopes = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $genders = Gender::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $intentions = Intention::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $frequences = Frequence::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $music = Music::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $schools = Scholarship::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('user.large.updateprofile', compact('countries', 'horoscopes', 'genders', 'intentions', 
            'schools','frequences','music'));
    }

    public function update(FirstAboutRequest $request)
    {   
        $user = User::find(\Auth::user()->id);
        //City
        $user->city = $request->input('city');
        $user->country_id = $request->input('country_id');
        $user->save();

        //Description
        $abo = UserAbout::find(\Auth::user()->about->id);
        $abo->description = $request->input('description');
        $abo->save();

        //Description
        $hors = UserHoroscope::find(\Auth::user()->userhoroscope->id);
        $hors->horoscope_id = $request->input('horoscope_id');
        $hors->save();

        //Habit
        $habit = UserHabit::find(\Auth::user()->habit->id);
        $habit->fill($request->all());
        $habit->save();

        //Info
        $info = UserInfo::find(\Auth::user()->info->id);
        $info->fill($request->all());
        $info->search_id = $request->input('search_id');
        $info->para_id = $request->input('para_id');
        $info->save();
        
        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }
}
