<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Horoscope;
use App\Country;
use App\UserHoroscope;
use App\UserInfo;
use App\UserAbout;
use App\UserBlock;
use App\Intention;
use App\UserSubscription;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateNotConfigRequest;
use App\Http\Requests\UpdatePassRequest;
use App\Http\Requests\UpdatePrivacyRequest;
use App\Http\Requests\UpdatePreferenceRequest;
use App\Http\Requests\UpdateAboutRequest;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateHoroscopeRequest;

class SettingsController extends Controller
{
    public function profile()
    {
        $user = User::find(\Auth::user()->id);
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $birth = explode('-', \Auth::user()->info->bth);

        return view('user.settings.profile', compact('user', 'countries','birth'));
    }

    public function profile_update(UpdateUserRequest $request)
    {
        $user = User::find(\Auth::user()->id);
        $user->fill($request->all());
        $user->country_id = $request->input('country_id');
        $user->save();

        //Info
        $info = UserInfo::find(\Auth::user()->info->id);
        $bth = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
        $info->bth = $bth;
        $info->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function avatar()
    {
        $user = User::find(\Auth::user()->id);

        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        $imh = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);

        return view('user.settings.avatar', compact('user', 'imh'));
    }

    public function avatar_update()
    {
        $file = \Input::file('file');
        $validation = \Validator::make(['file'=> $file], ['file' => 'required|image|max:7000']);
        
        if(!$validation->fails())
        {
            //Update database and delete old image
            $user = User::find(\Auth::user()->id);

            //Image
            $file = \Input::file('file');
            $extension = $file->getClientOriginalExtension(); 
            $newName = str_random(15).".".$extension;    
            $file->move(storage_path().'/uploads/', $newName);
            $path = storage_path().'/uploads/'.$newName;

            //Resize
            \Image::make($path)->resize(1100, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
           
            $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
            try {
                $s3->putObject([
                    'Bucket'     => config('filesystems.disks.s3images.bucket'),
                    'Key'        => 'images/profile/'.$newName,
                    'SourceFile' => $path,
                    'ACL'    => 'public-read',
                ]);
            } catch (Aws\Exception\S3Exception $e) {
                echo "There was an error uploading the file.\n";
            }     

            if($user->avatar != "default.png")
            {
                \Storage::disk('s3images')->delete('images/profile/'.$user->avatar);
            }
            \File::delete($path);
            //Image

            $user->avatar = $newName;
            $user->save();  

            return \Redirect::back()->with('message', 'Actualizado');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

    public function horoscope()
    {
        $user = User::find(\Auth::user()->id);
        $horoscopes = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('user.settings.horoscope', compact('user', 'horoscopes'));
    }

    public function horoscope_update(UpdateHoroscopeRequest $request)
    {
        $hor = UserHoroscope::find(\Auth::user()->userhoroscope->id);
        $hor->horoscope_id = $request->input('horoscope_id');
        $hor->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function notifications()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.notifications', compact('user'));
    }

    public function notifications_update(UpdateNotConfigRequest $request)
    {
        if($request->input('not_following') == 'yes')
        {
            $not_following = 1;
        }else
        {
            $not_following = 0;
        }

        if($request->input('not_follower') == 'yes')
        {
            $not_follower = 1;
        }else
        {
            $not_follower = 0;
        }

        User::find(\Auth::user()->id)->notifications_config()->update([
            'not_follower' => $not_follower,
            'not_following' => $not_following
        ]);

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function notifications_email()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.notifications_email', compact('user'));
    }

    public function notifications_email_update(UpdateNotConfigRequest $request)
    {
        if($request->input('email_message') == 'yes')
        {
            $email_message = 1;
        }else
        {
            $email_message = 0;
        }

        if($request->input('email_foll') == 'yes')
        {
            $email_foll = 1;
        }else
        {
            $email_foll = 0;
        }

        if($request->input('email_horos') == 'yes')
        {
            $email_horos = 1;
        }else
        {
            $email_horos = 0;
        }

        User::find(\Auth::user()->id)->notifications_config()->update([
            'email_message' => $email_message,
            'email_foll' => $email_foll,
            'email_horos' => $email_horos
        ]);

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function password()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.password', compact('user'));
    }

    public function password_update(UpdatePassRequest $request)
    {
        $user = User::find(\Auth::user()->id);

        if(\Hash::check($request->input('password'), $user->password)) 
        {
            $user->password = \Hash::make($request->input('newpass_confirmation'));
            $user->save();
            return \Redirect::back()->with('message', 'Perfil Actualizado');
        }else
        {
            return \Redirect::back()->withErrors(['Contraseña Incorrecta']);
        }
    }

    public function privacy()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.privacy', compact('user'));
    }

    public function privacy_update(UpdatePrivacyRequest $request)
    {
        if($request->input('recieve_message') == 'yes')
        {
            $recieve_message = 1;
        }else
        {
            $recieve_message = 0;
        }

        if($request->input('last_login') == 'yes')
        {
            $last_login = 1;
        }else
        {
            $last_login = 0;
        }

        User::find(\Auth::user()->id)->privacy_config()->update([
            'recieve_message' => $recieve_message,
            'last_login' => $last_login
        ]);

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function privacy_unblock($id)
    {
        $block = UserBlock::find($id);
        $block->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function intention()
    {
        $user = User::find(\Auth::user()->id);
        $intentions = Intention::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('user.settings.intention', compact('user', 'intentions'));
    }

    public function intention_update(UpdatePreferenceRequest $request)
    {
        $i = UserInfo::find(\Auth::user()->info->id);
        $i->search_id = $request->input('search_id');
        $i->para_id = $request->input('para_id');
        $i->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function about()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.about', compact('user'));
    }

    public function about_update(UpdateAboutRequest $request)
    {
        $i = UserAbout::find(\Auth::user()->about->id);
        $i->description = $request->input('description');
        $i->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

    public function payment()
    {
        $user = User::find(\Auth::user()->id);

        return view('user.settings.payment', compact('user'));
    }

    public function payment_renovation()
    {
        \Conekta::setApiKey(ENV('CONEKTA_KEY'));

        if(\Auth::user()->subscription->renovation == 0)
        {
            UserSubscription::find(\Auth::user()->subscription->id)->update(['renovation' => 1]);
            $customer = \Conekta_Customer::find(\Auth::user()->subscription->conekta_customer);
            $subscription = $customer->subscription->resume();
        }else
        {
            UserSubscription::find(\Auth::user()->subscription->id)->update(['renovation' => 0]);
            $customer = \Conekta_Customer::find(\Auth::user()->subscription->conekta_customer);
            $subscription = $customer->subscription->pause();
        }

        return \Redirect::back();
    }


    public function lang()
    {
        return view('user.settings.lang');
    }

}
