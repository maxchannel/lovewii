<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Aws\S3\S3Client;
use App\Country;
use App\Horoscope;
use App\Frequence;
use App\Intention;
use App\Music;
use App\Search;
use App\SearchUser;
use DateTime;

class SearchController extends Controller
{
	public function search()
    {
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $horos = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $frequences = Frequence::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $intentions = Intention::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $music = Music::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        $q = \Request::input('q');
        //Guardando la busqueda
        if($q)
        {
            $s = new Search;
            $s->q = $q;
            $s->save();
            if(\Auth::check())
            {
                $user = new SearchUser;
                $user->user_id = \Auth::user()->id;
                $user->search_id = $s->id;
                $user->save();
            }
        }
        //Guardando la busq

        //Si hay horoscopos
        $horoscopes = Horoscope::where('name','like', '%'.$q.'%')->get();

        $consulta = User::orderBy('created_at', 'DESC');
        if($q)
        {
        	$consulta->where('name','like', '%'.$q.'%')->orWhere('username','like', '%'.$q.'%');
        }

        if(\Request::input('country_id'))
        {
            $consulta->where('country_id',\Request::input('country_id'));
        }
        if(\Request::input('gender_id'))
        {
            $consulta->where('gender_id',\Request::input('gender_id'));
        }
        if(\Request::input('city'))
        {
            $consulta->where('city',\Request::input('city'));
        }

        //Relations
        if(\Request::input('horos'))
        {
            $consulta->whereHas('userhoroscope', function($q)
            {
                $q->where('horoscope_id', \Request::input('horos'));            

            });
        }
        if(\Request::input('smo'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('smo', \Request::input('smo'));          

            });
        }
        if(\Request::input('drin'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('drin', \Request::input('drin'));          

            });
        }
        if(\Request::input('gy'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('gy', \Request::input('gy'));          

            });
        }
        if(\Request::input('wo'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('wo', \Request::input('wo'));          

            });
        }
        if(\Request::input('re'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('re', \Request::input('re'));          

            });
        }
        if(\Request::input('mo'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('mo', \Request::input('mo'));          

            });
        }
        if(\Request::input('con'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('con', \Request::input('con'));          

            });
        }
        if(\Request::input('search_id'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('search_id', \Request::input('search_id'));          

            });
        }
        if(\Request::input('para_id'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('para_id', \Request::input('para_id'));          

            });
        }
        if(\Request::input('home'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('home', \Request::input('home'));          

            });
        }
        if(\Request::input('kds'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('kds', \Request::input('kds'));          

            });
        }
        if(\Request::input('hvkds'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('hvkds', \Request::input('hvkds'));          

            });
        }
        if(\Request::input('hvkds'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('hvkds', \Request::input('hvkds'));          

            });
        }
        if(\Request::input('hvkds'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('hvkds', \Request::input('hvkds'));          

            });
        }
        if(\Request::input('height'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('height','>=',\Request::input('height'));          

            });
        }
        if(\Request::input('height_to'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('height','<=',\Request::input('height_to'));          

            });
        }

        $date = date_create(date("Y-m-d"));
        date_sub($date, date_interval_create_from_date_string(\Request::input('age').' years'));
        $date2 = date_create(date("Y-m-d"));
        date_sub($date2, date_interval_create_from_date_string(\Request::input('age_to').' years'));
        $year1 = date_format($date, 'Y-m-d');
        $year2 = date_format($date2, 'Y-m-d');
        //echo $year1.' '.$year2;

        if(\Request::input('age'))
        {
            $consulta->whereHas('info', function($q) use($year2)
            {
                $q->where('bth','>=',$year2);          

            });
        }
        if(\Request::input('age_to'))
        {
            $consulta->whereHas('info', function($q) use($year1)
            {
                $q->where('bth','<=',$year1);          

            });
        }
        if(\Request::input('timido'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('timido', \Request::input('timido'));          

            });
        }
        if(\Request::input('muse'))
        {
            $consulta->whereHas('info', function($q)
            {
                $q->where('muse', \Request::input('muse'));          

            });
        }


        if(\Request::input('knd'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('knd', \Request::input('knd'));          

            });
        }
        if(\Request::input('her'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('her', \Request::input('her'));          

            });
        }
        if(\Request::input('pts'))
        {
            $consulta->whereHas('habit', function($q)
            {
                $q->where('pts', \Request::input('pts'));          

            });
        }



        $results = $consulta->paginate(25);
        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($results as $i => $user)
        {
            $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$user->avatar);
        }
        return view('search.search', compact('q', 'results', 'avatar', 'countries', 'horos', 'frequences', 'intentions',
            'music', 'horoscopes'));
    }

    public function advance_prev()
    { 
        if(\Request::input('o') == 1) 
        {
            $gender_id=1;
            $search_id=2;
        }elseif(\Request::input('o') == 2) 
        {
            $gender_id=1;
            $search_id=1;
        }elseif(\Request::input('o') == 3) 
        {
            $gender_id=2;
            $search_id=1;
        }elseif(\Request::input('o') == 4) 
        {
            $gender_id=2;
            $search_id=2;
        }


        return \Redirect::route('search', ['gender_id'=>$gender_id, 'search_id'=>$search_id]);
    }

    public function advance()
    {
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $horos = Horoscope::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $frequences = Frequence::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $intentions = Intention::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $music = Music::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('search.advance', compact('countries', 'horos', 'frequences', 'intentions','music'));
    }
}
