<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\UserNotificationConfig;
use App\UserPrivacyConfig;
use App\UserHoroscope;
use App\UserInfo;
use App\UserAbout;
use App\UserLock;
use App\UserLogin;
use App\UserHabit;
use App\UserSubscription;
use App\UserNotification;
use App\UserConfirmation;
use App\Country;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\SignInRequest;

class SignController extends Controller
{
    public function signin()
    {   
        return view('user.sign.signin');
    }

    public function signin_post(SignInRequest $request)
    {   
        $credentials =['email'=>$request->input('email'), 'password'=>$request->input('password')];

        if(\Auth::attempt($credentials,$request->input('remember')))
        {
            $login = new UserLogin;
            $login->user_id = \Auth::user()->id;
            $login->visitor = \Request::ip();
            $login->save();

            return \Redirect::route('home');
        }else
        {
            return \Redirect::back()->withInput()->with('login_error', 1);
        }
    }

    public function signup()
    {   
        $countries = Country::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();

        return view('user.sign.signup', compact('countries'));
    }

    public function signup_post(SignUpRequest $request)
    {
        $user = new User;
        $user->name = $request->input('name');
        $user->avatar = 'default.png';
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->gender_id = $request->input('gender_id');
        $user->password = \Hash::make($request->input('password'));
        $user->save();

        $lock = new UserLock;
        $lock->user_id = $user->id;
        $lock->save();

        //Edad
        $u_info = new UserInfo;
        $u_info->user_id = $user->id;
        if($request->input('gender_id') == 1)
        {
            $u_info->search_id = 2;
        }else
        {
            $u_info->search_id = 1;
        }
        $bth = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
        $u_info->bth = $bth;
        $u_info->para_id = 2;
        $u_info->save();

        $about = new UserAbout;
        $about->user_id = $user->id;
        $about->save();

        $about = new UserHabit;
        $about->user_id = $user->id;
        $about->save();

        $horos = new UserHoroscope;
        $horos->user_id = $user->id;
        $horos->save();

        UserNotification::create([
            'm'=> 'Bienvenido',
            'user_id'=> $user->id,
            'v'=>0
        ]); 

        $not_config = new UserNotificationConfig;
        $not_config->user_id = $user->id;
        $not_config->save();

        $user_priva = new UserPrivacyConfig;
        $user_priva->user_id = $user->id;
        $user_priva->save();

        $subs = new UserSubscription;
        $subs->user_id = $user->id;
        $subs->save();

        $login = new UserLogin;
        $login->user_id = $user->id;
        $login->visitor = \Request::ip();
        $login->save();

        $confirmation = new UserConfirmation;
        $confirmation->user_id = $user->id;
        $confirmation->key = str_random(4);
        $confirmation->save();

        //Email
        /*
        $subject = 'Código de confirmación';
        $to = $user->email;
        $content = 'Tu código de confirmación es: '.$confirmation->key;
        $params = [
            'subject'=>$subject,
            'content'=>$content,
        ];
        \Mail::send('partials.email.marketing', $params, function($message) use ($subject,$to)
        {
            $message->to($to)->subject($subject);
            $message->from('welcome@for1ever.com', 'For1Ever.com');

        });
        */
        //Email

        \Auth::login($user);
        return \Redirect::route('first_about')->with('message', 'Registro exitoso, ahora puedes completar un Perfil');
    }

    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('home');
    }


}
