<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class PublicController extends Controller
{
    public function terms()
    {   
        return view('public.terms');
    }

    public function privacy()
    {   
        return view('public.privacy');
    }
}
