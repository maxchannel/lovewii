<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Aws\S3\S3Client;
use App\User;
use App\Conversation;
use App\ConversationSee;
use App\Participant;
use App\Message;
use App\UserBlock;
use App\Http\Requests\NewMessageRequest;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class MessagesController extends Controller
{
    public function storeMessage($id, NewMessageRequest $request)
    {
        $message = Message::create([
            'conversation_id' => $id,
            'user_id' => \Auth::user()->id,
            'body' => $request->input('message'),
        ]);

        $conversation = Conversation::find($id);
            Participant::where('conversation_id',$id)->where('user_id',$conversation->recipients()->last()->user->id)
                ->update(['v' => 0]);

        return \Redirect::route('message_me', [$id]);
    }

    public function conversation()
    {
        $user = \Auth::user();

        $consulta = Conversation::orderBy('created_at', 'DESC');
        //Cuando no esta eliminada
        $consulta->whereHas('sees', function($q)
        {
            $q->where('v', 1);     
            $q->where('user_id', \Auth::user()->id);
        });
        $conversations = $consulta->paginate(25);

        //Av
        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        foreach($conversations as $i => $h)
        {
            $avatar[$i] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$h->recipients()->last()->user->avatar);
        }
        return view('messages.conversations', compact('conversations', 'avatar'));
    }

    public function message($id)
    {
        $user = \Auth::user();
        $conversation  = Conversation::find($id);
        $this->notFoundUnless($conversation);

        //Es mi conversasion
        if(!$conversation->myConverse())
        {
            return abort(404);
        }
        //Bloqueados
        $conversation->recipients()->last()->user->isBlocked(\Auth::user()->id);
        \Auth::user()->isBlocked($conversation->recipients()->last()->user->id);

        //Cambiando a visto
        if(\Auth::user()->getParticipants() > 0)
        {
            Participant::where('conversation_id', $id)->where('user_id', \Auth::user()->id)->update(array('v' => 1));
        }
        //Seleccionando mensajes
        
        $consul  = Message::where('conversation_id',$id)->orderBy('created_at','ASC');
        //Si ya se a eliminado alguna ves
        $desde = \Auth::user()->conversation_sees_get($conversation->id)->updated_at;
        if($desde  > \Auth::user()->conversation_sees_get($conversation->id)->created_at)
        {
            $consul->where('created_at', '>=', $desde);
        }
        $messages = $consul->paginate(45);

        //Av
        $s3 = new S3Client(['version' => 'latest','region'  => 'us-west-2', 'credentials'=>config('filesystems.disks.s3images')]);
        $avatar[$conversation->recipients()->last()->user->username] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.$conversation->recipients()->last()->user->avatar);
        $avatar[\Auth::user()->username] = $s3->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/profile/'.\Auth::user()->avatar);
        
        return view('messages.message', compact('conversation', 'avatar', 'messages'));
    }

    public function rewre()
    {
        $user = Auth::user();
        $conversations = Conversation::forUser()->orderBy('updated_at', 'desc')->get();

        return View::make('conversations.create', compact('conversations'));
    }

    public function message_new(NewMessageRequest $request)
    {
        if($request->input('to_id') == \Auth::user()->id)
        {
            return \Redirect::route('home');
        }

        //Si ya hablan
        $user = \Auth::user();
        $conversations = Conversation::forUser()->get();//Mis conversaciones
        $con_id=0;
        foreach($conversations as $con) 
        {
            if(Participant::where('conversation_id',$con->id)->where('user_id', $request->input('to_id'))->count() > 0)
            {
                $con_id = $con->id;
            }
        }

        //Crear o redirigir a existente
        if($con_id > 0)
        {
            $conversation = Conversation::find($con_id);
            //Volviendo a poder ver
            if(\Auth::user()->conversation_sees_get($conversation->id)->v == 0)
            {
                $see = ConversationSee::find(\Auth::user()->conversation_sees_get($conversation->id)->id);
                $see->v = 1;
                $see->save();
            }
            //Nuevo mensaje a la conversacion ya existente
            $message = Message::create([
                'conversation_id' => $con_id,
                'user_id' => \Auth::user()->id,
                'body' => $request->input('message'),
            ]);
            //Mensaje sin ver
            Participant::where('conversation_id',$con_id)->where('user_id',$conversation->recipients()->last()->user->id)
                ->update(['v' => 0]);


            return \Redirect::route('message_me', [$con_id]);
        }else
        {
            $conversation = Conversation::create([]);
            $s1 = ConversationSee::create([
                'conversation_id'=>$conversation->id,
                'user_id'=>\Auth::user()->id
            ]);
            $s2 = ConversationSee::create([
                'conversation_id'=>$conversation->id,
                'user_id' => $request->input('to_id'),
            ]);
            $message = Message::create([
                'conversation_id' => $conversation->id,
                'user_id' => \Auth::user()->id,
                'body' => $request->input('message'),
            ]);
            $sender = Participant::create([
                'conversation_id' => $conversation->id,
                'user_id' => \Auth::user()->id,
                'v'=>0
            ]);
            $destiny = Participant::create([
                'conversation_id' => $conversation->id,
                'user_id' => $request->input('to_id'),
                'v'=>0
            ]);

            return \Redirect::route('message_me', [$conversation->id]);
        }
    }


    public function destroy_converse($conversation_id)
    {
        $see = ConversationSee::find(\Auth::user()->conversation_sees_get($conversation_id)->id);
        $see->v = 0;
        $see->save();

        return \Redirect::route('messages')->with('message', 'Conversación Eliminada');
    }

    public function block_user($conversation_id, $other_id)
    {
        //Eliminando conversation
        $see = ConversationSee::find(\Auth::user()->conversation_sees_get($conversation_id)->id);
        $see->v = 0;
        $see->save();

        //Sino esta bloqueado
        $block = new UserBlock;
        $block->user_id = \Auth::user()->id;
        $block->other_id = $other_id;
        $block->save();

        return \Redirect::route('messages')->with('message', 'Usuario Bloqueado');
    }

}
