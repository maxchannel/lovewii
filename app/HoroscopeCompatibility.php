<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class HoroscopeCompatibility extends Model
{
    public function horoscope()
    {
        return $this->belongsTo('App\Horoscope', 'horoscope_id');
    }

    public function destiny()
    {
        return $this->belongsTo('App\Horoscope', 'with_id');
    }

    public function type()
    {
        return $this->belongsTo('App\HoroscopeCompatibilityType', 'type_id');
    }
}
