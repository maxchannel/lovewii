<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HoroscopeDay extends Model
{
	use SoftDeletes;

	protected $fillable = ['horoscope_id','fecha','content'];

    public function horoscope()
    {
        return $this->belongsTo('App\Horoscope');
    }
}
