<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horoscope extends Model
{
	public function days()
    {
        return $this->hasMany('App\HoroscopeDay');
    }

    public function real_days()
    {
        return $this->days()->where('fecha','<=',date('Y-m-d'))->orderBy('created_at','DESC');
    }

}
