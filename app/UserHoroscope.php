<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHoroscope extends Model
{
	use SoftDeletes;
	
	protected $fillable = ['horoscope_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function horoscope()
    {
        return $this->belongsTo('App\Horoscope');
    }
}
