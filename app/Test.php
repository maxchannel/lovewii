<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
	use SoftDeletes;
	protected $fillable = ['name', 'description'];

	public function questions()
	{
		return $this->hasMany('App\TestQuestion');
	}
}
