<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestQuestion extends Model
{
    protected $fillable = ['name', 'test_id'];

    public function test()
	{
		return $this->belongsTo('App\Test');
	} 
}
