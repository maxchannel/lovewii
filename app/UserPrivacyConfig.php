<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrivacyConfig extends Model
{
	use SoftDeletes;
	
    protected $table = 'user_privacy_config';
    protected $fillable = ['recieve_message', 'last_login'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
