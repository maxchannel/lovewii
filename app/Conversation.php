<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model 
{
	use SoftDeletes;
	
	protected $table = 'conversations';
	protected $fillable = ['subject'];

	public function messages()
	{
		return $this->hasMany('App\Message');
	}

	public function messages_to_me()//Mensajes mios activos
	{
		return $this->messages()->where('user_id', \Auth::user()->id)->count();
	}

	public function messages_to_re()//Mensajes another activos
	{
		return $this->messages()->where('user_id', $this->recipients()->last()->user->id)->count();
	}

	public function total_messages()//Mensajes total activos
	{
		return $this->messages_to_re()+$this->messages_to_me();
	}

	public function participants()
	{
		return $this->hasMany('App\Participant');
	}

	public function getParticipants()
    {
        return $this->participants()->where('v',0)->where('user_id',\Auth::user()->id)->count();
    }

	public function recipients()
	{
		return $this->participants()->where('user_id', '!=', \Auth::user()->id)->get();
	}

	public function recipients_count()
	{
		return $this->participants()->where('user_id', '!=', \Auth::user()->id)->count();
	}

	public function sees()
	{
		return $this->hasMany('App\ConversationSee');
	}

	public function scopeForUser($query, $user = null)
	{
		$user = $user ?: \Auth::user()->id;
		return $query->join('participants', 'conversations.id', '=', 'participants.conversation_id')
		->where('participants.user_id', $user)
		->select('conversations.*');
	}

	public function myConverse($user = null)
	{
		$user = $user ?: \Auth::user()->id;
		return $this->participants()->where('user_id',$user)->count();
	}

	public function scopeWithNewMessages($query, $user = null)
	{
		$user = $user ?: \Auth::user()->id;
		return $query->join('participants', 'conversations.id', '=', 'participants.conversation_id')
		->where('participants.user_id', $user)
		->where('conversations.updated_at', '>', \DB::raw('participants.last_read'))
		->select('conversations.*');
	}

	public function participantsString($user = null)
	{
		$user = $user ?: \Auth::user()->id;
		$participantNames = \DB::table('users')
		->join('participants', 'users.id', '=', 'participants.user_id')
		->where('users.id', '!=', $user)
		->where('participants.conversation_id', $this->id)
		->select(\DB::raw("concat(users.first_name, ' ', users.last_name) as name"))
		->lists('users.name');
		return implode(', ', $participantNames);
	}

	public function addParticipants(array $participants)
	{
		$userModel = Config::get('messenger::user_model');
		$userModel = new $userModel;
		$participant_ids = [];
		if (is_array($participants))
		{
			if (is_numeric($participants[0]))
			{
				$participant_ids = $participants;
			}
			else
			{
				$participant_ids = $userModel->whereIn('email', $participants)->lists('id');
			}
		}
		else
		{
			if (is_numeric($participants))
			{
				$participant_ids = [$participants];
			}
			else
			{
				$participant_ids = $userModel->where('email', $participants)->lists('id');
			}
		}
		if(count($participant_ids))
		{
			foreach ($participant_ids as $user_id)
			{
				Participant::create([
					'user_id' => $user_id,
					'conversation_id' => $this->id,
				]);
			}
		}
	}


}