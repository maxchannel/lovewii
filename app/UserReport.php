<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserReport extends Model
{
    use SoftDeletes;
    protected $fillable = ['report', 'comments', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
