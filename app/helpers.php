<?php

function getAge($bth)
{
    $from = new DateTime($bth);
    $to = new DateTime('today');

    return $from->diff($to)->y;   
}