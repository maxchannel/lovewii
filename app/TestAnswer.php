<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestAnswer extends Model
{
    protected $fillable = ['name'];

    public function participant()
	{
		return $this->belongsTo('App\TestParticipant');
	} 
}
