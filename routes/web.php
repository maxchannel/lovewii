<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index'])->middleware('about');
Route::group(['prefix' => 'es'], function () 
{
    //Landings
    Route::group(['prefix' => 'p'], function () {
    	Route::get('horoscopo/{mode?}', ['as' => 'horoscopo_promo', 'uses' => 'LandingController@horoscopo2']);
    });    

    Route::group(['middleware' => ['auth', 'premium', 'about']], function () {
    	Route::get('coach', ['as' => 'coach', 'uses' => 'CoachController@coach']);
    	Route::get('search-advance', ['as' => 'search_advance', 'uses' => 'SearchController@advance']);
    	Route::get('views', ['as' => 'views', 'uses' => 'ViewController@views']);
    	Route::get('verify', ['as' => 'verify', 'uses' => 'AdminController@verified']); 
    	Route::post('verify', ['as' => 'verify_update', 'uses' => 'AdminController@verify_update']); 
    });    

    Route::get('pricing', ['as' => 'pricing', 'uses' => 'LandingController@index']);
	Route::get('terms', ['as' => 'terms', 'uses' => 'PublicController@terms']);
	Route::get('privacy', ['as' => 'privacy', 'uses' => 'PublicController@privacy']);
	Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
	Route::get('horoscopo-day', ['as' => 'horoscopo_day', 'uses' => 'LandingController@horoscopo']);
	Route::post('horoscope-match-prev', ['as' => 'horoscope_match_prev', 'uses' => 'HoroscopeController@horoscope_match_prev']);
	Route::get('horoscope-match/{sign}', ['as' => 'horoscope_match', 'uses' => 'HoroscopeController@horoscope_match']);//Compatibility 
    Route::get('contact', ['as' => 'contact', 'uses' => 'AdminController@contact']); 
	Route::post('contact', ['as' => 'contact_update', 'uses' => 'AdminController@contact_update']); 
    Route::get('subscriptions', ['as' => 'admin_subscriptions', 'uses' => 'AdminController@subscriptions']); 
    Route::get('stats', ['as' => 'admin_stats', 'uses' => 'AdminController@stats']); 
	Route::post('subscription', ['as' => 'subscription_update', 'uses' => 'SubscriptionController@subscription_update']); 
	//Buscar
    Route::get('search', ['as' => 'search', 'uses' => 'SearchController@search']);
    Route::post('search-advance-prev', ['as' => 'advance_prev', 'uses' => 'SearchController@advance_prev']);   
    //Donar
    Route::get('donate/{slug}', ['as' => 'donate', 'uses' => 'DonateController@index']);
    Route::post('donate', ['as' => 'donate_post', 'uses' => 'DonateController@payment']);

    Route::group(['prefix' => 'payment', 'middleware' => ['auth', 'noPremium']], function () {
    	Route::get('secure/{slug}', ['as' => 'payment', 'uses' => 'PaymentController@index']);
    	Route::post('secure', ['as' => 'payment_post', 'uses' => 'PaymentController@payment']);
    });    

    Route::group(['prefix' => 'payment'], function () {
    	Route::get('success', ['as' => 'payment_success', 'uses' => 'PaymentController@success']);
    	Route::get('gopremium', ['as' => 'gopremium', 'uses' => 'PaymentController@gopremium']);
    });    

    Route::group(['middleware' => ['guest']], function () {
    	Route::get('signin', ['as' => 'signin', 'uses' => 'SignController@signin']);
    	Route::post('signin', ['as' => 'signin_post', 'uses' => 'SignController@signin_post']);
    	Route::get('signup', ['as' => 'signup', 'uses' => 'SignController@signup']);
    	Route::post('signup', ['as' => 'signup_post', 'uses' => 'SignController@signup_post']);
        Route::get('horoscopes', ['as' => 'horoscope_all', 'uses' => 'HoroscopeController@all']);
    	Route::get('horoscope/sign/{sign}', ['as' => 'horoscope_open', 'uses' => 'HoroscopeController@open']);//Horos Open
    	Route::get('horoscope/{sign}/{date}', ['as' => 'horoscope_individual', 'uses' => 'HoroscopeController@individal']);
    });    

    Route::group(['prefix' => 'settings', 'middleware' => ['auth', 'about']], function () {
    	Route::get('about-all', ['as' => 'settings_about_xl', 'uses' => 'AboutController@edit']);
    	Route::put('about-all', ['as' => 'settings_about_xl_update', 'uses' => 'AboutController@update']);
    	Route::get('profile', ['as' => 'settings_profile', 'uses' => 'SettingsController@profile']);
    	Route::put('profile', ['as' => 'settings_profile_update', 'uses' => 'SettingsController@profile_update']);
    	Route::get('password', ['as' => 'settings_password', 'uses' => 'SettingsController@password']);
    	Route::put('password', ['as' => 'settings_password_update', 'uses' => 'SettingsController@password_update']);
    	Route::get('avatar', ['as' => 'settings_avatar', 'uses' => 'SettingsController@avatar']);
    	Route::put('avatar', ['as' => 'settings_avatar_update', 'uses' => 'SettingsController@avatar_update']);
    	Route::get('horoscope', ['as' => 'settings_horoscope', 'uses' => 'SettingsController@horoscope']);
    	Route::put('horoscope', ['as' => 'settings_horoscope_update', 'uses' => 'SettingsController@horoscope_update']);
    	Route::get('notifications', ['as' => 'settings_notifications', 'uses' => 'SettingsController@notifications']);
    	Route::put('notifications', ['as' => 'settings_notifications_update', 'uses' => 'SettingsController@notifications_update']);
    	Route::get('notifications_email', ['as' => 'settings_notifications_email', 'uses' => 'SettingsController@notifications_email']);
    	Route::put('notifications_email', ['as' => 'settings_notifications_email_update', 'uses' => 'SettingsController@notifications_email_update']);
    	Route::get('privacy', ['as' => 'settings_privacy', 'uses' => 'SettingsController@privacy']);
    	Route::put('privacy', ['as' => 'settings_privacy_update', 'uses' => 'SettingsController@privacy_update']);
    	Route::delete('privacy/unblock/{id}', ['as' => 'privacy_unblock', 'uses' => 'SettingsController@privacy_unblock']);
    	Route::get('intention', ['as' => 'settings_intention', 'uses' => 'SettingsController@intention']);
    	Route::put('intention', ['as' => 'settings_intention_update', 'uses' => 'SettingsController@intention_update']);
    	Route::get('about', ['as' => 'settings_about', 'uses' => 'SettingsController@about']);
    	Route::put('about', ['as' => 'settings_about_update', 'uses' => 'SettingsController@about_update']);
    	Route::get('payment', ['as' => 'settings_payment', 'uses' => 'SettingsController@payment']);
    	Route::delete('payment/renovation', ['as' => 'payment_renovation', 'uses' => 'SettingsController@payment_renovation']); 
    	Route::get('image', ['as' => 'settings_image', 'uses' => 'ImageController@image']);
    	Route::put('image/{id}', ['as' => 'settings_image_update', 'uses' => 'ImageController@image_update']);
    	Route::delete('image/destroy/{id}', ['as' => 'settings_image_destroy', 'uses' => 'ImageController@destroy']);    

    	Route::get('lang', ['as' => 'settings_lang', 'uses' => 'SettingsController@lang']);
    });    

    Route::group(['middleware' => ['auth', 'about']], function () {
    	Route::get('notifications', ['as' => 'notifications', 'uses' => 'NotificationController@notifications']);     
    	Route::delete('notifications/destroy/{id}', ['as' => 'destroy_not', 'uses' => 'NotificationController@destroy_not']);
    	Route::get('tests', ['as' => 'tests', 'uses' => 'TestController@index']);   
    	Route::get('test/{id}', ['as' => 'take', 'uses' => 'TestController@take']);
    	Route::post('test/{id}', ['as' => 'take_update', 'uses' => 'TestController@take_update']);
    	
    	Route::delete('views/destroy/{id}', ['as' => 'destroy_view', 'uses' => 'ViewController@destroy_view']); 
    	Route::get('welcome', ['as' => 'welcome', 'uses' => 'HomeController@welcome']); 
    	Route::get('horoscope', ['as' => 'horoscope', 'uses' => 'HoroscopeController@horoscope']);//Horos Auth  	
    	//Messages
    	Route::get('messages', ['as' => 'messages', 'uses' => 'MessagesController@conversation']);    	
    	Route::get('message/{id}', ['as' => 'message_me', 'uses' => 'MessagesController@message']); 
    	Route::post('message/{id}', ['as' => 'message_me_update', 'uses' => 'MessagesController@storeMessage']); 
    	Route::post('message-new', ['as' => 'message_new', 'uses' => 'MessagesController@message_new']);  
    	Route::delete('message/destroy/{conversation_id}', ['as' => 'destroy_converse', 'uses' => 'MessagesController@destroy_converse']); 
    	Route::delete('block/user/{conversation_id}/{other_id}', ['as' => 'block_user', 'uses' => 'MessagesController@block_user']);     

    	Route::get('avatar_first', ['as' => 'avatar_first', 'uses' => 'AboutController@avatar_first']); 
    	Route::post('avatar_first', ['as' => 'avatar_first_p', 'uses' => 'AboutController@avatar_first_p']); 
    	//Route::get('paypal/{slug}', ['as' => 'paypal', 'uses' => 'PaypalController@paypal']);//Paypal
    	//Route::post('paypal', ['as' => 'paypal_post', 'uses' => 'PaypalController@paypal_post']);//Paypal
    });    

    Route::group(['prefix' => 'i', 'middleware' => ['auth']], function () {
    	Route::get('logout', ['as' => 'logout', 'uses' => 'SignController@logout']);
    	Route::post('report', ['as' => 'contact_report', 'uses' => 'AdminController@contact_report']); 
    });    

    Route::group(['prefix' => 's', 'middleware' => ['auth', 'noAbout']], function () {   	
    	Route::get('about', ['as' => 'first_about', 'uses' => 'AboutController@index']); 
    	Route::post('about', ['as' => 'first_about_create', 'uses' => 'AboutController@create']); 
    });    

    Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'is_admin']], function () {   	
    	Route::get('home', ['as' => 'admin_horoscope', 'uses' => 'AdminController@horoscope']); 
    	Route::post('home', ['as' => 'admin_horoscope_update', 'uses' => 'AdminController@horoscope_update']); 
        Route::get('destroy/horoscope/{sign}', ['as' => 'admin_horoscope_delete', 'uses' => 'AdminController@delete_horoscope']); 
        Route::delete('destroy/horoscope/{id}', ['as' => 'admin_horoscope_delete_update', 'uses' => 'AdminController@destroy_horoscope']); 
    	Route::get('contact', ['as' => 'admin_contact', 'uses' => 'AdminController@admin_contact']); 
    	Route::get('verify', ['as' => 'admin_verify', 'uses' => 'AdminController@verify']); 
    	Route::delete('verify/destroy/{id}', ['as' => 'admin_verify_destroy', 'uses' => 'AdminController@admin_verify_destroy']);
    });


    //Al final el user sino falla
    Route::get('{username}', ['as' => 'profile', 'uses' => 'ProfileController@index'])->middleware('about');    
});
