@extends('layouts.app')

@section('title', 'Mi Suscripción')
@section('meta-description', 'Mi Suscripción')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Membresía</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    @if(\Auth::user()->subscription->ends_at == NULL or \Auth::user()->subscription->ends_at < date('Y-m-d')) 
                        Actualmente tu perfil es normal, para ser un usuario completo puedes:<br>
                        <a href="{{ route('pricing') }}" class="btn btn-ts">Ver Planes</a><br><br>
                    @else
                        @if(\Auth::user()->subscription->renovation == 0)
                        <div class="alert alert-danger alert-dismissable">
                            Dejarás de ser un Usuario Completo al finalizar tu actual período de facturación.
                        </div>
                        @endif

                        -Ya eres Usuario Completo, tu proxima factura será: 
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{\Auth::user()->subscription->ends_at}}").format("D MMMM YYYY"));
                        </script>
                        <br>
                        @if(\Auth::user()->subscription->renovation == 0 and \Auth::user()->subscription->plan == "monthly")
                            {!! Form::open(['route'=>['payment_renovation'], 'method'=>'DELETE']) !!}
                            {!! Form::submit('Reactivar Renovación Automática', ['class' => 'delete-button btn btn-primary btn-sm']) !!}
                            {!! Form::close() !!}
                        @elseif(\Auth::user()->subscription->renovation == 1 and \Auth::user()->subscription->plan == "monthly")
                            {!! Form::open(['route'=>['payment_renovation'], 'method'=>'DELETE']) !!}
                            {!! Form::submit('Cancelar Renovación Automática', ['class' => 'delete-button btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}

                            <br>
                            <p class="text-muted text-center">Para tu comodidad tu membresía se renovará automáticamente.</p>
                        @endif
                    @endif
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $(".delete-button").click(function(){
        if (!confirm("Confirma para cancelar la Renovación Automática?")){
          return false;
        }
    });
</script>
@endsection