@extends('layouts.app')

@section('title', 'Editar mis Preferencias')
@section('meta-description', 'Editar mis Preferencias')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Preferencias</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_intention_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Buscando</label>
                            <div class="col-md-6">
                                {!! Form::select('search_id',
                                [
                                    ''=>'Seleccionar',
                                    '1'=>'Hombre',
                                    '2'=>'Mujer'
                                ],$user->info->search_id,['class'=>'form-control', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Para</label>
                            <div class="col-md-6">
                                {!! Form::select('para_id',[''=>'Seleccionar']+$intentions,$user->info->para_id,['class'=>'form-control', 'required']) !!}    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection