@extends('layouts.app')

@section('title', 'Editar Notificaciones por Email')
@section('meta-description', 'Editar Notificaciones por Email')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Notificaciones por Email</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_notifications_email_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                Enviarme un correo electrónico cuando
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->notifications_config->email_message == 1)
                                    {!! Form::checkbox('email_message', 'yes', true) !!} Me envíen un Mensaje   
                                @else
                                    {!! Form::checkbox('email_message', 'yes', false) !!} Me envíen un Mensaje   
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->notifications_config->email_foll == 1)
                                    {!! Form::checkbox('email_foll', 'yes', true) !!} Alguien nuevo me sigue  
                                @else
                                    {!! Form::checkbox('email_foll', 'yes', false) !!} Alguien nuevo me sigue  
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->notifications_config->email_horos == 1)
                                    {!! Form::checkbox('email_horos', 'yes', true) !!} Exista nuevo Hóroscopo 
                                @else
                                    {!! Form::checkbox('email_horos', 'yes', false) !!} Exista nuevo Hóroscopo 
                                @endif
                            </div>
                        </div>
                        
                        
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection