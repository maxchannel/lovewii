@extends('layouts.app')

@section('title', 'Editar Notificaciones')
@section('meta-description', 'Editar Notificaciones')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Notificaciones</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_notifications_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                Recibir notificaciones de
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->notifications_config->not_following == 1)
                                    {!! Form::checkbox('not_following', 'yes', true) !!} Peronas que sigo  
                                @else
                                    {!! Form::checkbox('not_following', 'yes', false) !!} Peronas que sigo  
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->notifications_config->not_follower == 1)
                                    {!! Form::checkbox('not_follower', 'yes', true) !!} Peronas que me siguen  
                                @else
                                    {!! Form::checkbox('not_follower', 'yes', false) !!} Peronas que me siguen  
                                @endif
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection