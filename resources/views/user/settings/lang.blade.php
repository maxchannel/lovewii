@extends('layouts.app')

@section('title', 'Cambiar Idioma')
@section('meta-description', 'Cambiar Idioma')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Idioma ({{ Config::get('languages')[App::getLocale()] }})</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    <div class="list-group">
                        @foreach (Config::get('languages') as $lang => $language)
                            @if ($lang != App::getLocale())
                                <a href="{{ route('lang.switch', $lang) }}" class="list-group-item">{{$language}} <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                            @endif
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection