@extends('layouts.app')

@section('title', 'Editar Privacidad')
@section('meta-description', 'Editar Privacidad')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Privacidad</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_privacy_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                @if($user->privacy_config->recieve_message == 1)
                                    {!! Form::checkbox('recieve_message', 'yes', true) !!} Permitir que otros me manden mensajes privados
                                @else
                                    {!! Form::checkbox('recieve_message', 'yes', false) !!} Permitir que otros me manden mensajes privados 
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">                                
                                @if($user->privacy_config->last_login == 1)
                                    {!! Form::checkbox('last_login', 'yes', true) !!} Última Conexión
                                @else
                                    {!! Form::checkbox('last_login', 'yes', false) !!} Última Conexión 
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}

                        <hr>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <h4>Bloqueados</h4>
                                <table class="table table-hover">
                                    @foreach($user->blocks as $block)
                                    <tr data-id="{{ $block->id }}">
                                        <td>{!! $block->other->name !!}</td>
                                        <td> 
                                            <a href="#" class="btn-delete">Desbloquear</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <br>
                    
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
{!! Form::open(['route'=>['privacy_unblock', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();
        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();
        row.fadeOut();
        $.post(url, data, function(result){
            alert(result.message);
        });
    });
});
</script>
@endsection