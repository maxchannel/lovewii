@extends('layouts.app')

@section('title', 'Editar Perfil Básico')
@section('meta-description', 'Editar Perfil Básico')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Cuenta</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        @include('partials.btns.success')
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_profile_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Nombre']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                {!! Form::email('email',null,['class'=>'form-control', 'maxlength'=>'60', 'placeholder'=>'Email']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">País</label>
                            <div class="col-md-6">
                                {!! Form::select('country_id',[''=>'Seleccionar']+$countries,$user->country_id,['class'=>'form-control', 'required'=>'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Ciudad</label>
                            <div class="col-md-6">
                                {!! Form::text('city',null,['class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Ciudad']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nacimiento</label>
                            <div class="col-md-2">
                                {!! Form::select('day',config('options.days'),$birth[2],['class'=>'form-control', 'required']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('month',config('options.months'),$birth[1],['class'=>'form-control', 'required']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('year',config('options.years'),$birth[0],['class'=>'form-control', 'required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection