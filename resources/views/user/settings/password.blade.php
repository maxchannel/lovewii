@extends('layouts.app')

@section('title', 'Editar Password')
@section('meta-description', 'Editar Password')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_settings')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Contraseña</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_password_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Contraseña Actual</label>
                            <div class="col-md-6">
                                {!! Form::password('password',['class'=>'form-control']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nueva</label>
                            <div class="col-md-6">
                                {!! Form::password('newpass',['class'=>'form-control']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirmar</label>
                            <div class="col-md-6">
                                {!! Form::password('newpass_confirmation',['class'=>'form-control']) !!}    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection