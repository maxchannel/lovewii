@extends('layouts.app')

@section('title', 'Registrate')
@section('meta-description', 'Registrate en For1Ever')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    SignUp
                </div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        @include('partials.btns.success')
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::open(['route'=>'signup_post', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                {!! Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Username</label>
                            <div class="col-md-6">
                                {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Username', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::password('password',['class'=>'form-control', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Sexo</label>
                            <div class="col-md-6">
                                {!! Form::select('gender_id',
                                [
                                    ''=>'Seleccionar',
                                    '1'=>'Hombre',
                                    '2'=>'Mujer'
                                ],null,['class'=>'form-control', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nacimiento</label>
                            <div class="col-md-2">
                                {!! Form::select('day',config('options.days'),null,['class'=>'form-control', 'required']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('month',config('options.months'),null,['class'=>'form-control', 'required']) !!}
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('year',config('options.years'),null,['class'=>'form-control', 'required']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                {!! captcha_img() !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Captcha</label>
                            <div class="col-md-6">
                                {!! Form::password('captcha',null,['class'=>'form-control','placeholder'=>'Captcha']) !!}    
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Registrarme
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <p class="text-muted">
                                Al dar click en enviar aceptas los 
                                <a href="{{ route('terms') }}" target="_blank">Terminos y Condiciones</a> y
                                <a href="{{ route('privacy') }}" target="_blank">Política de Privacidad</a>
                            </p><br>
                            <p class="text-muted">Eres miembro? <a href="{{ route('signin') }}">Iniciar Sesión</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
@endsection