@extends('layouts.app')

@section('title', 'Iniciar Sesión')
@section('meta-description', 'Iniciar Sesión en For1Ever')


@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                    SignIn
                </div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        @include('partials.btns.danger')
                    @endif
                    @include('partials.errorMessages')
                    @if(Session::has('login_error'))
                        </br><div class="alert alert-danger" role="alert"><p>Email o Password incorrectos</p></div>
                    @endif

                    {!! Form::open(['route'=>'signin_post', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                {!! Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::password('password',['class'=>'form-control', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('remember') !!} Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>   
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Login
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <p class="text-muted">No Eres miembro? <a href="{{ route('signup') }}">Crear Cuenta</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>
@endsection