@extends('layouts.app')

@section('title', $user->name.' Perfil, Gustos, Intenciones, Acerca de mi y Fotos')
@section('meta-description', $user->city.' - '.$user->about->description)

@section('script_head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 font-14">
            @if(Auth::check())
                @if(Auth::user()->isNormal())
                    @include('partials.ad')<br>
                @endif
            @else
                @include('partials.ad')<br>
            @endif

            @if(Auth::check() and Auth::user()->id==$user->id)
                @include('partials.menu_profile')
                <br>
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif
            @include('partials.errorMessages')

            <div class="panel panel-default">    
                <div class="panel-body">    
                    <div class="row">
                        <div class="col-md-4">
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog">
                                   <div class="modal-content">
                                     <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">{{ $user->name }}</h4>
                                     </div>
                                     <div class="modal-body">
                                        <!-- images -->
                                        @if(count($user->images))
                                        <div class="bs-example" data-example-id="simple-carousel">
                                              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                                    <ol class="carousel-indicators">
                                                        @foreach($user->images as $key => $image)
                                                            @if($key == 0)
                                                                <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                                                            @else
                                                                <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                                                            @endif
                                                        @endforeach  
                                                    </ol>                        

                                                    <div class="carousel-inner" role="listbox">
                                                        @foreach($user->images as $key => $image)
                                                            @if($key == 0)
                                                                <div class="item active">
                                                                    <img src="{{ $temp[$key] }}" class="tales img-album" alt="{{ $user->name }}">
                                                                </div>
                                                            @else
                                                                <div class="item">
                                                                    <img src="{{ $temp[$key] }}" class="tales img-album" alt="{{ $user->name }}">
                                                                </div> 
                                                            @endif
                                                        @endforeach  
                                                    </div>                        

                                                    @if(count($user->images) > 1)
                                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    @endif
                                              </div>
                                        </div>
                                        @endif
                                        <!-- images -->
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                     </div>
                                   </div>
                                 </div>
                            </div>
                            <!-- Modal -->
                            <!-- avatar -->
                            <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog modal-lg">
                                   <div class="modal-content">
                                     <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">{{ $user->name }}</h4>
                                     </div>
                                     <div class="modal-body">
                                        <img src="{{ $avatar }}" class="img-responsive" />
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                     </div>
                                   </div>
                                 </div>
                            </div>
                            <!-- avatar -->
                            <a href="" data-toggle="modal" data-target="#myModal5"><img src="{{ $avatar }}" class="img-responsive aoi-profile" /></a>
                            
                        </div>
                        <div class="col-md-8">
                            <h1>{{ $user->name }}</h1>
                            <p>
                                @if($user->city != "")
                                Origen:
                                {{$user->city}}
                                @endif
                                @if($user->country_id != "")
                                , {{$user->country->name}}
                                @endif
                            </p>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog">
                                   <div class="modal-content">
                                     <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">Mensaje a {{ $user->name }}</h4>
                                     </div>
                                     <div class="modal-body">
                                        @include('partials.errorMessages')
                                        {!! Form::open(['route'=>['message_new'], 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                                        {!! Form::textarea('message',null,['class'=>'form-control', 'placeholder'=>'Mensaje a '.$user->name, 'rows'=>'4','required']) !!}    
                                        <input type="hidden" name="to_id" value="{{ $user->id }}">
                                        <br>
                                        <input type="submit" class="btn btn-tm" value="Enviar" />
                                        {!! Form::close() !!}
                                     </div>
                                     <div class="modal-footer">
                                         <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                                     </div>
                                   </div>
                                 </div>
                            </div>
                            <!-- Modal -->
                            @if(Auth::check())
                                @if(Auth::user()->id != $user->id)
                                <a href="" data-toggle="modal" class="btn btn-ts" data-target="#myModal2">Enviar Mensaje</a>
                                @endif
                            @else
                            <a href="" data-toggle="modal" class="btn btn-ts" data-target="#myModal2">Enviar Mensaje</a>
                            @endif

                            @if(count($user->images))
                            <a href="" data-toggle="modal" class="btn btn-ora" data-target="#myModal">Fotos</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <div class="row">
                                <div class="col-md-4">
                                    <p><span class="fb">Edad:</span> {{ $age }}</p>
                                </div>
                                @if($user->about->prof != "")
                                <div class="col-md-4">
                                    <p><span class="fb">Profesión:</span> {{ $user->about->prof }}</p>
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <p><span class="fb">Tatuajes:</span>
                                        @if($user->info->tato == 4)
                                        +{{ $user->info->tato }}
                                        @else
                                        {{ $user->info->tato }}
                                        @endif
                                    </p>
                                </div>
                                @if($user->about->prof != "")
                                <div class="col-md-4">
                                    <p><span class="fb">Altura:</span> {{ $user->info->height }} cm</p>
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <p>
                                        @if($user->info->timido != "")
                                            <span class="fb">Tímido o Atrevido:</span>
                                            @if($user->info->timido == 1)
                                            +Atrevido
                                            @elseif($user->info->timido == 2)
                                            +Tímido
                                            @endif
                                        @endif
                                    </p>
                                </div>
                                @if($user->info->music != "")
                                <div class="col-md-4">
                                    <p><span class="fb">Música:</span> {{ $user->info->music->name }}</p>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                @if($user->habit->drin != "")
                <div class="col-md-12">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <h4>Hábitos</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <p><span class="fb">Fuma:</span>  {{ $user->habit->smoke->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Toma:</span>  {{ $user->habit->drink->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Se ejercita:</span>  {{ $user->habit->gym->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Trabaja:</span> {{ $user->habit->work->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Usa Drogas:</span>  {{ $user->habit->drugs->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Lee:</span>  {{ $user->habit->read->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Va al Cine:</span>  {{ $user->habit->movies->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Va a Conciertos:</span>  {{ $user->habit->concerts->name }}</p>
                                </div>
                                @if($user->info->school != "")
                                <div class="col-md-4">
                                    <p><span class="fb">Estudios:</span>  {{ $user->info->school->name }}</p>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user->about->description != "")
                <div class="col-md-12">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <h4>Acerca de Mí</h4>
                            <p class="horoscope-font">
                            {{ $user->about->description }}
                            </p>
                        </div>
                    </div>
                </div>
                @endif

                @if($user->info->height != "")
                <div class="col-md-12">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <h4>Intención</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <p><span class="fb">Buscando:</span>
                                        @if($user->info->buscando->name == "female")
                                        Mujer
                                        @else
                                        Hombre
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Para:</span> {{ $user->info->para->name }}</p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Es Hogareño:</span>
                                        @if($user->info->home == 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Quiere Hijos?:</span> 
                                        @if($user->info->kds == 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <p><span class="fb">Tiene Hijos?:</span> 
                                        @if($user->info->hvkds == 1)
                                        Si
                                        @else
                                        No
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(Auth::check())
                        @if(Auth::user()->isNormal())
                            @include('partials.ad')<br>
                        @endif
                    @else
                        @include('partials.ad')<br>
                    @endif
                </div>
                @endif

                @if($user->userhoroscope->horoscope_id != "")
                <div class="col-md-12">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <h4>{{ $user->horoscope()->name }}</h4>
                            <div class="col-md-4">
                                <span class="fb">Elemento:</span> {{ $user->horoscope()->elem }}
                            </div>
                            <div class="col-md-4">
                                <span class="fb">Planeta:</span> {{ $user->horoscope()->planet }}
                            </div>
                            <div class="col-md-4">
                                <span class="fb">Símbolo:</span> {{ $user->horoscope()->sym }}
                            </div>
                            @if($user->horoscope()->stone != "x")
                            <div class="col-md-4">
                                <span class="fb">Piedra:</span> {{ $user->horoscope()->stone }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endif

                <div class="col-md-12 hidden-xs">
                    <div class="panel panel-default">    
                        <div class="panel-body">    
                            <h4>Perfiles Similares</h4>
                            <div class="row">
                                @foreach($users as $r => $j)
                                <div class="col-md-3">
                                    <a href="{{route('profile',$j->username)}}"><img src="{{ $ing[$r] }}" class="img-responsive aoi-profile" /></a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-center"><a href="{{route('contact', ['mode'=>'report-user','id'=>$user->id])}}">Reportar Usuario</a></p>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection