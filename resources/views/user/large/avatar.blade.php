@extends('layouts.app')

@section('title', 'Editar Avatar')
@section('meta-description', 'Editar Avatar')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection 

@section('content')
<div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center plan-name">
                    Subir Avatar
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <h4>
                                Sube tu primer Imágen y aumenta 80% las visitas y mensajes a tu perfil
                            </h4><br>
                            @include('partials.errorMessages')
                            {!! Form::model($user, ['route'=>'settings_avatar_update', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Imágen Actual</label>
                                    <div class="col-md-6">
                                        <!-- Modal -->
                                         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                     <img src="{{ $imh }}" class="img-responsive" />
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  </div>
                                                </div>
                                              </div>
                                         </div>
                                         <!-- Modal -->
                                         <a href="" data-toggle="modal" data-target="#myModal"><img src="{{ $imh }}" class="img-responsive"  /></a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-6">
                                       {!! Form::file('file', ['accept' => 'image/*']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        @include('partials.btn_update')
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            <br>
                            <a href="{{ route('welcome') }}" class="text-center">Siguiente</a>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
@endsection