@extends('layouts.app')

@section('title', 'Editar Perfil')
@section('meta-description', 'Editar Perfil')

@section('script_head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
            
            @include('partials.menu_profile')
            <br>
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif
            @include('partials.errorMessages')

            <h4>Editar Perfil</h4>
            {!! Form::open(['route'=>'settings_about_xl_update', 'method'=>'PUT', 'role'=>'form', 'class'=>'form-horizontal']) !!}
            {!! Form::label('country_id', 'País') !!}
            {!! Form::select('country_id',[''=>'Seleccionar']+$countries,\Auth::user()->country_id,['class'=>'form-control', 'required']) !!}
            {!! Form::label('city', 'Ciudad') !!}
            {!! Form::text('city',\Auth::user()->city,['class'=>'form-control', 'placeholder'=>'Ciudad', 'required']) !!}
            <br>
            {!! Form::label('description', 'Descripción') !!}
            {!! Form::textarea('description',\Auth::user()->about->description,['class'=>'form-control', 'placeholder'=>'Será la primera cosa que la persona lea al buscarte, habla sobre ti, tus pasatiempos, a que te dedicas, etc...', 'required']) !!}    
            <br>
            {!! Form::label('horoscope_id', 'Horóscopo (Usaremos tu signo para presentarte compatibles)') !!}
            {!! Form::select('horoscope_id',[''=>'Seleccionar']+$horoscopes,\Auth::user()->userhoroscope->id,['class'=>'form-control', 'required']) !!}
                            <br>

            <!-- Info -->
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('search_id', 'Busco') !!}
                    {!! Form::select('search_id',[
                        ''=>'Seleccionar',
                        '1'=>'Hombre',
                        '2'=>'Mujer'
                    ],\Auth::user()->info->search_id,['class'=>'form-control', 'required'=>'required']) !!}
                    <br>
                </div>
                <div class="col-md-6">
                    {!! Form::label('para_id', 'Para') !!}
                    {!! Form::select('para_id',[''=>'Seleccionar']+$intentions,\Auth::user()->info->para_id,['class'=>'form-control', 'required'=>'required']) !!}
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('s', 'Hogareño?') !!}
                    {!! Form::select('s',[
                        ''=>'Seleccionar',
                        '0'=>'Si',
                        '1'=>'No'
                    ],\Auth::user()->info->home,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('kds', '¿Quieres Hijos?') !!}
                    {!! Form::select('kds',[
                        ''=>'Seleccionar',
                        '0'=>'No',
                        '1'=>'Si',
                    ],\Auth::user()->info->kds,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('hvkds', '¿Tienes?') !!}
                    {!! Form::select('hvkds',[
                        ''=>'Seleccionar',
                        '0'=>'No',
                        '1'=>'Si',
                    ],\Auth::user()->info->hvkds,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('prof', 'Profesión') !!}
                    {!! Form::text('prof',\Auth::user()->about->prof,['class'=>'form-control', 'placeholder'=>'Profesión', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('tato', '¿Tienes Tatuajes?') !!}
                    {!! Form::select('tato',[
                        ''=>'Seleccionar',
                        '0'=>'0',
                        '1'=>'1',
                        '2'=>'2',
                        '3'=>'3',
                        '4'=>'4+'
                    ],\Auth::user()->info->tato,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('height', 'Altura') !!}
                    {!! Form::select('height',config('options.heights'),\Auth::user()->info->height,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {!! Form::label('muse', 'Música #1 en tu vida') !!}
                    {!! Form::select('muse',[''=>'Seleccionar']+$music,\Auth::user()->info->muse,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-6">
                    {!! Form::label('timido', '¿Timido?') !!}
                    {!! Form::select('timido',[
                        ''=>'Seleccionar',
                        '1'=>'Tímido',
                        '2'=>'Atrevido',
                    ],\Auth::user()->info->timido,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <!-- Info -->

            <h3>Hábitos</h3>
            <!-- Habits -->
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('smo', '¿Fumas?') !!}
                    {!! Form::select('smo',[''=>'Seleccionar']+$frequences,\Auth::user()->habit->smo,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('drin', '¿Tomas?') !!}
                    {!! Form::select('drin',[''=>'Seleccionar']+$frequences,\Auth::user()->habit->drin,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('gy', '¿Haces Ejercicio?') !!}
                    {!! Form::select('gy',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->gy,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('sch', 'Escolaridad') !!}
                    {!! Form::select('sch',[''=>'Seleccionar']+$schools,\Auth::user()->info->sch,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('wo', '¿Actualmente Trabajas?') !!}
                    {!! Form::select('wo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->wo,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('drug', '¿Usas Drogas?') !!}
                    {!! Form::select('drug',[''=>'Seleccionar']+$frequences,\Auth::user()->habit->drug,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('re', 'Lees Habitualmente') !!}
                    {!! Form::select('re',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->re,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('mo', 'Te gusta ir al Cine') !!}
                    {!! Form::select('mo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->mo,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('con', '¿Vas mucho a Conciertos?') !!}
                    {!! Form::select('con',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->con,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('knd', 'Eres Amable y Cuidados@?') !!}
                    {!! Form::select('knd',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->knd,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('her', 'Sabes escuchar?') !!}
                    {!! Form::select('her',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->her,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
                <div class="col-md-4">
                    {!! Form::label('pts', 'Mascotas?') !!}
                    {!! Form::select('pts',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Auth::user()->habit->pts,['class'=>'form-control', 'required']) !!}
                    <br>
                </div>
            </div>
            <!-- Habits -->

            <br><br><br>
            <button type="submit" class="btn btn-success">
                Completar
            </button>
            {!! Form::close() !!}

        </v class="col-md-2">
        </div>
    </div>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection