@extends('layouts.app')

@section('title', 'Subir Imágenes')
@section('meta-description', 'Subir Imágenes')

@section('script_head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
            
            @if(Auth::check() and Auth::user()->id==$user->id)
                @include('partials.menu_profile')
                <br>
            @endif
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif
            @include('partials.errorMessages')
            <h4>Subir Imágenes</h4>
            <!-- Upload -->
            {!! Form::open(['route'=>['settings_image_update', $user->id], 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
            <p class="text-muted">Puedes subir multiples imágenes</p>
            @if(Session::has('image-message'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('image-message') }}
                </div>
            @endif
            {!! Form::file('file[]',['multiple' => 'multiple', 'id' => 'multiple-files', 'accept' => 'image/*']) !!}
            <br>
            <button type="submit" class="btn btn-primary">Enviar</button>
            {!! Form::close() !!}
            <!-- Upload -->

            <br><br><br>
            @if($user->images->count() > 0)
                @foreach($user->images as $i => $image)
                    <div class="col-sm-4">
                        <!-- Modal -->
                        <div class="modal fade" id="myModal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog modal-lg">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel">{{$user->name}}</h4>
                                 </div>
                                 <div class="modal-body">
                                    <img src="{{ $temp[$i] }}" class="img-responsive" />
                                 </div>
                                 <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                               </div>
                             </div>
                        </div>
                        <!-- Modal -->
                        <a href="" data-toggle="modal" data-target="#myModal{{$image->id}}"><img src="{{ $temp[$i] }}" class="order-image-min" /></a>
                        <!-- Delete -->
                        {!! Form::open(['route'=>['settings_image_destroy', $image->id], 'method'=>'DELETE']) !!}
                        {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm btn-block', 'style' => 'margin: 10px 0']) !!}
                        {!! Form::close() !!}
                        <!-- Delete -->
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning alert-dismissable">
                    Aún no tienes imágenes en tu Album.
                </div>
            @endif


        </div>
        <div class="col-md-2">
        </div>
    </div>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Eliminar definitivamente?")){
          return false;
        }
    });
</script>
@endsection