@extends('layouts.app')

@section('title', 'Completar Perfil')
@section('meta-description', 'Completar Perfil')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection 

@section('content')
<div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center plan-name">
                    Completa tu Perfil para mejorar tu Compatibiliad
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <h4>
                                Incluye información relevante en cuanto a tus gustos, pasatiempos, musica, que buscas en las otras personas, en una pareja, etc...<br>
                                Más nunca incluyas información personal, email, teléfonos, direcciones, etc...
                            </h4><br>
                            @include('partials.errorMessages')

                            {!! Form::open(['route'=>'first_about_create', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                            @if(Auth::user()->username == NULL)
                            {!! Form::label('username', 'Username') !!}
                            {!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Username', 'required']) !!}
                            @endif
                            @if(Auth::user()->gender_id == NULL)
                            {!! Form::label('gender_id', 'Sexo') !!}
                            {!! Form::select('gender_id',
                            [
                                ''=>'Seleccionar',
                                '1'=>'Hombre',
                                '2'=>'Mujer'
                            ],null,['class'=>'form-control', 'required']) !!} 
                            @endif
                            @if(Auth::user()->info->bth == NULL)
                            {!! Form::label('bth', 'Nacimiento') !!}
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::select('day',config('options.days'),null,['class'=>'form-control', 'required']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::select('month',config('options.months'),null,['class'=>'form-control', 'required']) !!}
                                </div>
                                <div class="col-md-4">
                                    {!! Form::select('year',config('options.years'),null,['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                            @endif

                            {!! Form::label('country_id', 'País') !!}
                            {!! Form::select('country_id',[''=>'Seleccionar']+$countries,null,['class'=>'form-control', 'required']) !!}
                            {!! Form::label('city', 'Ciudad') !!}
                            {!! Form::text('city',null,['class'=>'form-control', 'placeholder'=>'Ciudad', 'required']) !!}
                            <br>
                            {!! Form::label('description', 'Descripción') !!}
                            {!! Form::textarea('description',null,['class'=>'form-control', 'placeholder'=>'Será la primera cosa que la persona lea al buscarte, habla sobre ti, tus pasatiempos, a que te dedicas, etc...', 'required']) !!}    
                            <br>
                            {!! Form::label('horoscope_id', 'Horóscopo (Usaremos tu signo para presentarte compatibles)') !!}
                            {!! Form::select('horoscope_id',[''=>'Seleccionar']+$horoscopes,null,['class'=>'form-control', 'required']) !!}
                            <br>

                            <!-- Info -->
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Form::label('search_id', 'En busca de') !!}
                                    {!! Form::select('search_id',[
                                        ''=>'Seleccionar',
                                        '1'=>'Hombre',
                                        '2'=>'Mujer'
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::label('para_id', 'Me interesa') !!}
                                    {!! Form::select('para_id',[''=>'Seleccionar']+$intentions,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('home', '¿Eres Hogareño?') !!}
                                    {!! Form::select('home',[
                                        ''=>'Seleccionar',
                                        '0'=>'Si',
                                        '1'=>'No'
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('kds', '¿Quieres Hijos?') !!}
                                    {!! Form::select('kds',[
                                        ''=>'Seleccionar',
                                        '0'=>'No',
                                        '1'=>'Si',
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('hvkds', '¿Tienes Hijos?') !!}
                                    {!! Form::select('hvkds',[
                                        ''=>'Seleccionar',
                                        '0'=>'No',
                                        '1'=>'Si',
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('prof', 'Profesión Actualmente') !!}
                                    {!! Form::text('prof',null,['class'=>'form-control', 'placeholder'=>'Profesión', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('tato', '¿Tienes Tatuajes?') !!}
                                    {!! Form::select('tato',[
                                        ''=>'Seleccionar',
                                        '0'=>'0',
                                        '1'=>'1',
                                        '2'=>'2',
                                        '3'=>'3',
                                        '4'=>'4+'
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('height', 'Cuanto Mides') !!}
                                    {!! Form::select('height',config('options.heights'),null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Form::label('muse', 'Música #1 en tu vida') !!}
                                    {!! Form::select('muse',[''=>'Seleccionar']+$music,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::label('timido', '¿Timido?') !!}
                                    {!! Form::select('timido',[
                                        ''=>'Seleccionar',
                                        '1'=>'Tímido',
                                        '2'=>'Atrevido',
                                    ],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <!-- Info -->

                            <h3>Hábitos</h3>
                            <!-- Habits -->
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('smo', '¿Fumas?') !!}
                                    {!! Form::select('smo',[''=>'Seleccionar']+$frequences,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('drin', '¿Tomas?') !!}
                                    {!! Form::select('drin',[''=>'Seleccionar']+$frequences,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('gy', '¿Haces Ejercicio?') !!}
                                    {!! Form::select('gy',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('sch', 'Tu nivel de Escolaridad') !!}
                                    {!! Form::select('sch',[''=>'Seleccionar']+$schools,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('wo', '¿Actualmente Trabajas?') !!}
                                    {!! Form::select('wo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('drug', '¿Usas Drogas?') !!}
                                    {!! Form::select('drug',[''=>'Seleccionar']+$frequences,null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('re', 'Lees Habitualmente') !!}
                                    {!! Form::select('re',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('mo', 'Te gusta ir al Cine') !!}
                                    {!! Form::select('mo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('con', '¿Vas mucho a Conciertos?') !!}
                                    {!! Form::select('con',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::label('knd', 'Eres Amable y Cuidados@?') !!}
                                    {!! Form::select('knd',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('her', 'Sabes escuchar?') !!}
                                    {!! Form::select('her',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                                <div class="col-md-4">
                                    {!! Form::label('pts', '¿Quieres o tienes Mascotas?') !!}
                                    {!! Form::select('pts',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control', 'required']) !!}
                                    <br>
                                </div>
                            </div>
                            <!-- Habits -->

                            <br><br><br>
                            <button type="submit" class="btn btn-success">
                                Completar
                            </button>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
@endsection