@extends('layouts.app')

@section('title', 'Quíen vio mi Perfil')
@section('meta-description', 'Quíen vio mi Perfil')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Views</h1>
            </div>

            <table class="table table-hover">
                <tr>
                    <th>Fecha</th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($views as $i => $view)
                <tr data-id="{{ $view->id }}">
                    <td>
                    </td>
                    <td>
                        <a href="{{ route('profile', [$view->from->username]) }}"><img src="{{ $avatar[$i] }}" class="aoi pull-left"/></a>
                        <a href="{{ route('profile', [$view->from->username]) }}">{{ $view->from->name }}</a><br>
                        <p>
                            visitó tu perfil 
                            <script>
                            moment.locale("es");
                            document.writeln(moment.utc("{{ $view->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                            </script>
                        </p>
                    </td>
                    <td> 
                        <a href="#" class="btn-delete">Eliminar</a>
                    </td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $views->render()) !!} 
        </div>

        <div class="col-md-2">
        </div>
    </div>
</div>
{!! Form::open(['route'=>['destroy_view', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection