@extends('layouts.app')

@section('title', 'Coach')
@section('meta-description', 'Coach')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            <!--
            <a href="{{ route('messages') }}" class="btn btn-info btn-ts">Busqueda Avanzada</a>
            -->
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Coach</h1>
                <p>Entrena tu perfil y aumenta el número de parejas potenciales, que te respondan mensajes y que te sigan.</p>
            </div>
            <br>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Avatar</div>
                        <div class="panel-body text-center">
                            @if(\Auth::user()->avatar == "default.png")
                            <p>
                                <span class="glyphicon glyphicon-remove-sign red-ok" aria-hidden="true"></span>
                                No haz Personalizado tu Avatar: <a href="{{ route('settings_avatar')}}">Cambiar Avatar</a>
                            </p>
                            @else
                            <p>
                                <span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span>
                                Ya Personalizaste tu Avatar
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Descripción</div>
                        <div class="panel-body text-center">
                            <p>
                                @if(strlen(\Auth::user()->about->description) < 190)
                                    <span class="glyphicon glyphicon-remove-sign red-ok" aria-hidden="true"></span>
                                    Tienes menos de 190 caracteres en tu descripción, 
                                    esta comprobado que quien escribe más sobre si mismo tienes más 
                                    probabilidad de encontrar a alguien:
                                    <a href="{{ route('settings_about')}}">Escribir mi Descripción</a>
                                @else
                                    <span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span>
                                    Tienes al menos de 190 caracteres en tu descripción, esta correcta.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Test de Compatibiliad</div>
                        <div class="panel-body text-center">
                            <p>
                                @if(\Auth::user()->getTestParticipants() > 0)
                                    <span class="glyphicon glyphicon-remove-sign green-ok" aria-hidden="true"></span>
                                    Ya tomaste al menos 1 Test.
                                @else
                                    <span class="glyphicon glyphicon-remove-sign red-ok" aria-hidden="true"></span>
                                    No haz tomado algún Test, tomar minimo 1 te sugerira mejores potenciales matchs:
                                    <a href="{{ route('tests')}}">Tomar algún Test</a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Album de Fotos</div>
                        <div class="panel-body text-center">
                            <p>
                                @if(count(\Auth::user()->images) < 4)
                                <p>
                                    <span class="glyphicon glyphicon-remove-sign red-ok" aria-hidden="true"></span>
                                    Quien tiene al menos 4 fotos en su album incrementa sus match: 
                                    <a href="{{ route('settings_image')}}">Subir imágenes a Album</a>
                                </p>
                                @else
                                <p>
                                    <span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span>
                                    Tienes al menos 4 fotos en su album, ya tienes mejores matchs.
                                </p>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Usuario Completo</div>
                        <div class="panel-body text-center">
                            @if(\Auth::user()->subscription->ends_at == NULL or \Auth::user()->subscription->ends_at < date('Y-m-d')) 
                            <p>Actualmente cuentas con una suscripción normal, ser usuario completo te permitira:</p>
                            <p></p>
                            <a href="{{ route('settings_about')}}">Ser Usuario Completo</a>
                            @else
                                <span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span>
                                Ya eres un Usuario Completo, cuentas con un plus genial.
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default panel-coach">
                        <div class="panel-heading text-center">Perfil Verificado</div>
                        <div class="panel-body text-center">
                            <p>
                                -Actualmente tu perfil no esta verificado, por ser Usuario Completo puedes verificarlo: 
                                <a href="{{ route('verify')}}">Verificar</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-2">
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection