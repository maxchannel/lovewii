@extends('layouts.app')

@section('title', 'Procesar mi Pago')
@section('meta-description', 'Procesar mi Pago')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.5.0/js/conekta.js"></script>
<script type="text/javascript">
    Conekta.setPublicKey('{{ ENV("CONEKTA_KEY_PUBLIC") }}');
</script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">        
            <div class="panel panel-default">
                <div class="panel-heading text-center">Elegiste</div>
                <div class="panel-body">
                @if($mode == 'anual')
                <div class="price-list text-center">
                    <p class="lan-price">Plan Anual</p>
                    <p>96 USD</p>
                </div>
                @elseif($mode == 'semester')
                <div class="price-list text-center">
                    <p class="lan-price">Plan Semestral</p>
                    <p>54 USD</p>
                </div>
                @elseif($mode == 'month')
                <div class="price-list text-center">
                    <p class="lan-price">Plan Mensual</p>
                    <p>12 USD por mes</p>
                </div>
                @endif
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading text-center">Obtendrás</div>
                <div class="panel-body">
                    <div class="price-list">
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Quien ve tu Perfil</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Parejas en base a tu Horóscopo</p>    
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Coach</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Hasta Arriba en el Buscador</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Horóscopo</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Con quien tiene más Química</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Busqueda Avanzada</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Sin Publicidad</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Pago Seguro</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')
                    <br>

                    {!! Form::open(['route'=>['donate_post'], 'id'=>'card-form','method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                        <p class="text-center"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Metodo de Pago </p>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <span class="card-errors orange"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control','data-conekta'=>'card[name]', 'maxlength'=>'50', 'placeholder'=>'Nombre como aparece en la tarjeta', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                {!! Form::email('email',null,['class'=>'form-control', 'maxlength'=>'50', 'placeholder'=>'Email de Contacto', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Número de tarjeta de crédito</label>
                            <div class="col-md-6">
                                {!! Form::text(null,null,['class'=>'form-control','data-conekta'=>'card[number]', 'maxlength'=>'16', 'placeholder'=>'Número de tarjeta de crédito', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">CVC (3 Dígitos al Reverso)</label>
                            <div class="col-md-6">
                                {!! Form::text(null,null,['class'=>'form-control','data-conekta'=>'card[cvc]', 'maxlength'=>'3', 'placeholder'=>'Número de seguridad', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Vencimiento</label>
                            <div class="col-md-2">
                                {!! Form::select(null,config('options.months'),null,['class'=>'form-control','data-conekta'=>'card[exp_month]', 'required']) !!}    
                            </div>
                            <div class="col-md-4">
                                {!! Form::select(null,config('options.years_card'),null,['class'=>'form-control','data-conekta'=>'card[exp_year]', 'required']) !!}     
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Teléfono de Contacto</label>
                            <div class="col-md-6">
                                {!! Form::text('phone',null,['class'=>'form-control', 'maxlength'=>'20', 'placeholder'=>'Número de Teléfono', 'required']) !!}    
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Dirección de Contacto</label>
                            <div class="col-md-6">
                                {!! Form::text('direction',null,['class'=>'form-control', 'placeholder'=>'Dirección de contacto', 'required']) !!}    
                            </div>
                        </div>

                        @if($mode == 'anual')
                            {!! Form::hidden('xyz', 'xyz1') !!}
                        @elseif($mode == 'semester')
                            {!! Form::hidden('xyz', 'xyz2') !!}
                        @elseif($mode == 'month')
                            {!! Form::hidden('xyz', 'xyz3') !!}
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Avanzar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
            <p class="text-muted text-center">En tu estado de cuenta apareceremos como: Pagos Conekta</p>
            <p class="text-muted text-center">Tu IP será guardada en esta Transacción.</p>
            <p class="text-muted text-center">Si necesita ayuda de click <a href="{{route('contact')}}" target="_blank">aquí</a></p>
        </div>
        <div class="col-md-1">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="{{ asset('assets/js/conekt.js') }}"></script>
@endsection 