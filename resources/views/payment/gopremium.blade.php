@extends('layouts.app')

@section('title', 'Conviertete a Usuario Completo')
@section('meta-description', 'Conviertete a Usuario Completo')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Conviertete a Usuario Completo</div>
                <div class="panel-body text-center">
                    <h2>¿Qué obtienes?</h2>
                    <a href="" class="btn btn-lg btn-ora">Tener un Coach</a><br><br>
                    <a href="" class="btn btn-lg btn-gr">Ver Quien Visita tu Perfil</a><br><br>
                    <a href="" class="btn btn-lg btn-cielo">Leer tu Hóroscopo y Compatibles</a><br><br>
                    <a href="" class="btn btn-lg btn-re">Busqueda Avanzada</a><br><br>
                    <a href="" class="btn btn-lg btn-ts">Subir hasta 25 Imágenes</a><br><br>
                    <h3>Navegar sin anuncios...</h3>
                    <h3>y más...</h3>
                    <br>
                    <a href="{{ route('pricing') }}" class="btn btn-lg btn-ts">Ver Planes</a><br><br>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection