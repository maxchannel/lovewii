@extends('layouts.app')

@section('title', 'Precios')
@section('meta-description', 'Precios de los diferentes planes')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection 

@section('content')
@if(\Request::input('i') == "")
<div class="container-fluid">
    <section class="row text-center pageHero">
        <h1><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> Usuario Completo</h1>
        <br>
        <p class="lan-tip">
            Contrata y haz que tu Cuenta este completa con 12 funciones más
        <p>
    </section>

    <section class="row plan-separation">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center plan-name">
                            Plan Mensual
                        </div>
                        <div class="panel-body text-center">
                        @if(\Request::input('don') == "")
                            @if($pais == "MX")
                            <p class="lan-price">99 MXN</p>
                            <p>Cobro mes a mes</p>
                            @else
                            <p class="lan-price">12 USD</p>
                            <p>Cobro mes a mes</p>
                            @endif
                        @elseif(\Request::input('don') == "true")
                            <p class="lan-price">12 USD</p>
                        @endif
                            <hr>
                            <ul class="list-unstyled plan-list">
                                <li>Quien ve tu Perfil</li>
                                <li>Parejas en base a tu Horóscopo</li>
                                <li>Coach</li>
                                <li>Hasta Arriba en el Buscador</li>
                                <li>Tests</li>
                                <li>Sin Publicidad</li>
                                <li>Horóscopo</li>
                                <li>Con quien tiene más Química</li>
                                <li>Busqueda Avanzada</li>
                            </ul>
                            <hr>
                        @if(\Request::input('don') == "")
                            <a href="{{ route('payment', ['month', 'camp'=>$camp]) }}" class="btn green btn-lg">Tomar este Plan</a>
                        @elseif(\Request::input('don') == "true")
                            <a href="{{ route('donate', ['month']) }}" class="btn green btn-lg">Donar</a>
                        @endif
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center plan-name">
                            Plan Anual
                        </div>
                        <div class="panel-body text-center">
                        @if(\Request::input('don') == "")
                            @if($pais == "MX")
                            <p class="lan-price">79 MXN</p>
                            <p>por mes, pagando semestral: 948 MXN</p>
                            @else
                            <p class="lan-price">8 USD</p>
                            <p>por mes, pagando anual: 96 USD</p>
                            @endif
                        @elseif(\Request::input('don') == "true")
                            <p class="lan-price">96 USD</p>
                        @endif
                            
                            <hr>
                            <div class="plan-unique">
                                <p class="plan-unique-f">Solo en Este Plan:</p>
                                <ul class="list-unstyled plan-list">
                                    <li>Horóscopos por email</li>
                                    <li>Revisión de Imágenes de usuarios</li>
                                    <li>Prioridad en Soporte</li>
                                </ul>
                            </div>
                            <ul class="list-unstyled plan-list">
                                <li>Quien ve tu Perfil</li>
                                <li>Parejas en base a tu Horóscopo</li>
                                <li>Coach</li>
                                <li>Hasta Arriba en el Buscador</li>
                                <li>Tests</li>
                                <li>Sin Publicidad</li>
                                <li>Horóscopo</li>
                                <li>Con quien tienes más Química</li>
                                <li>Busqueda Avanzada</li>
                            </ul>
                            <hr>
                        @if(\Request::input('don') == "")
                            <a href="{{ route('payment', ['annual', 'camp'=>$camp]) }}" class="btn orange btn-lg">Tomar este Plan</a>
                        @elseif(\Request::input('don') == "true")
                            <a href="{{ route('donate', ['anual']) }}" class="btn orange btn-lg">Donar</a>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center plan-name">
                            Plan Semestral
                        </div>
                        <div class="panel-body text-center">
                        @if(\Request::input('don') == "")
                            @if($pais == "MX")
                            <p class="lan-price">89 MXN</p>
                            <p>por mes, pagando semestral: 534 MXN</p>
                            @else
                            <p class="lan-price">9 USD</p>
                            <p>por mes, pagando semestral: 54 USD</p>
                            @endif
                        @elseif(\Request::input('don') == "true")
                            <p class="lan-price">54 USD</p>
                        @endif
                            
                            <hr>
                            <ul class="list-unstyled plan-list">
                                <li>Quien ve tu Perfil</li>
                                <li>Parejas en base a tu Horóscopo</li>
                                <li>Coach</li>
                                <li>Hasta Arriba en el Buscador</li>
                                <li>Tests</li>
                                <li>Sin Publicidad</li>
                                <li>Horóscopo</li>
                                <li>Con quien tiene más Química</li>
                                <li>Busqueda Avanzada</li>
                            </ul>
                            <hr>
                        @if(\Request::input('don') == "")
                            <a href="{{ route('payment', ['semester', 'camp'=>$camp]) }}" class="btn blue btn-lg">Tomar este Plan</a>
                        @elseif(\Request::input('don') == "true")
                            <a href="{{ route('donate', ['semester']) }}" class="btn blue btn-lg">Donar</a>
                        @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <section class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <p class="lan-tipA"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></p>
                            <p class="lan-tip">Quien ve tu Perfil</p>
                            <p class="lan-tipB">Podrás saber quien ha visitado tu Perfil</p>
                        </div>
                        <div class="col-md-12">
                            <p class="lan-tipA"><span class="glyphicon glyphicon-tags" aria-hidden="true"></span></p>
                            <p class="lan-tip">Sin Anuncios</p>
                            <p class="lan-tipB">No se añadira publicidad</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <p class="lan-tipA"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></p>
                            <p class="lan-tip">Personas Compatibles con tu Signo</p>
                            <p class="lan-tipB">Tienes la opción de ver compatibles en base a tu signo</p>
                        </div>
                        <div class="col-md-12">
                            <p class="lan-tipA"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></p>
                            <p class="lan-tip">Coach</p>
                            <p class="lan-tipB">Atención Personalizada para que tu perfil mejore hasta un 80% la posibilidad de encontrar pareja</p>    
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <br><br><br>
    <div class="text-center">
        <h2>Crea tu propia historia Hoy</h2>
        <a href="{{ route('signup') }}" class="btn btn-primary btn-lg">Registrarme</a>
    </div>

    <div class="balance"></div>
    @include('partials.footer')
</div>

@elseif(\Request::input('i') == "comp")
<div class="container">
    <section class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <section class="row text-center intro">
                <h2>Planes y Precios</h2>
                <h3 class="lan-tip">
                    Deja de preguntar a todos que signo son y comparar si son Compatibles, aquí conoces tu compatibilidad y 
                    hasta de los Presentamos.
                <h3>
            </section>
        </div>
        <div class="col-md-2">
        </div>
    </section>

    <section class="row">
        <div class="col-md-3">
            <div class="panel text-center box verde">
                <div class="panel-body">
                    <h4>Usuario Normal</h4>
                    <h1>$ 0</h1>
                    <p>por mes</p>
                </div>
            </div>
            <br>
            <a href="{{ route('signup') }}" class="link text-center">Ir</a>
            <hr>
            <ul class="list-unstyled charac">
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Menores parejas potenciales</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Menor cantidad de fotos en Album</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Busqueda Normal</li>
            </ul>
        </div>
        <div class="col-md-3">
            <div class="panel text-center box morado">
                <div class="panel-body">
                    <h4>Plan Mensual</h4>
                    <h1>$ 12</h1>
                    <p>por mes</p>
                </div>
            </div>
            <br>
            <a href="{{ route('payment', ['month']) }}" class="link text-center">Más información</a>
            <hr>
            <ul class="list-unstyled charac">
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Parejas en base a tu Signo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Quien ve tu Perfil</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Coach</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Hasta Arriba en el Buscador</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tests</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Sin Publicidad</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Horóscopo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Con quien tienes más Química</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Busqueda Avanzada</li>
            </ul>
        </div>
        <div class="col-md-3">
            <div class="panel text-center box azul">
                <div class="panel-body">
                    <h4>Plan Semestral</h4>
                    <h1>$ 9</h1>
                    <p>por mes, pagando semestral: 54 USD</p>
                </div>
            </div>
            <br>
            <a href="{{ route('payment', ['semester']) }}" class="link text-center">Más información</a>
            <hr>
            <ul class="list-unstyled charac">
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Parejas en base a tu Signo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Quien ve tu Perfil</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Coach</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Hasta Arriba en el Buscador</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tests</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Sin Publicidad</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Horóscopo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Con quien tienes más Química</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Busqueda Avanzada</li>
            </ul>
        </div>
        <div class="col-md-3">
            <div class="panel text-center box rojo">
                <div class="panel-body">
                    <h4>Plan Anual</h4>
                    <h1>$ 8</h1>
                    <p>por mes, pagando anual: 96 USD</p>
                </div>
            </div>
            <br>
            <a href="{{ route('payment', ['anual']) }}" class="link text-center">Más información</a>
            <hr>
            <ul class="list-unstyled charac">
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Parejas en base a tu Signo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Horóscopos por email</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Revisión de Imágenes de usuarios</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Prioridad en Soporte</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Quien ve tu Perfil</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Coach</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Hasta Arriba en el Buscador</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Tests</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Sin Publicidad</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Horóscopo</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Con quien tienes más Química</li>
                <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Busqueda Avanzada</li>
            </ul>
        </div>
    </section>
    <br><br>
    <section class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-body">
                    El proceso de selección de una persona para estar, nosotros te presentamos esta nueva opción donde te decimos
                    con que signo eres compatible y además te presentamos el perfil de la persona
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </section>


    <div class="balance"></div>
</div>
@endif
    
@endsection