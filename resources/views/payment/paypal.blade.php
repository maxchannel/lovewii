@extends('layouts.app')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">        
            <div class="panel panel-default">
                <div class="panel-heading text-center">Elegiste</div>
                <div class="panel-body">
                    @if($mode == 'anual')
                    <div class="price-list text-center">
                        <p class="lan-price">Plan Anual</p>
                        <p>96 USD (Ahorras $48 USD)</p>
                    </div>
                    @elseif($mode == 'semester')
                    <div class="price-list text-center">
                        <p class="lan-price">Plan Semestral</p>
                        <p>54 USD (Ahorras $18 USD)</p>
                    </div>
                    @elseif($mode == 'month')
                    <div class="price-list text-center">
                        <p class="lan-price">Plan Mensual</p>
                        <p>12 USD por mes</p>
                    </div>
                    @endif
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading text-center">Obtendrás</div>
                <div class="panel-body">
                    <div class="price-list">
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Quien ve tu Perfil</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Parejas en base a tu Horóscopo</p>    
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Coach</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Hasta Arriba en el Buscador</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Horóscopo</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Con quien tiene más Química</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Busqueda Avanzada</p>
                        <p><span class="glyphicon glyphicon-ok-sign green-ok" aria-hidden="true"></span> Sin Publicidad</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Pago Seguro</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')
                    <br>
                    <ul class="nav nav-tabs nav-justified">
                        <li role="presentation"><a href="{{ route('payment', [$mode, 'camp'=>$camp]) }}">Tarjeta de Crédito</a></li>
                        <li role="presentation"><a href="{{ route('paypal', [$mode, 'camp'=>$camp]) }}">Paypal</a></li>
                    </ul>
                    <br>

                    
                        <span class="card-errors"></span>

                        <p class="text-center"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> PayPal </p>

                        @if($mode == 'anual')
                            {!! Form::hidden('xyz', 'xyz1') !!}
                        @elseif($mode == 'semester')
                            {!! Form::hidden('xyz', 'xyz2') !!}
                        @elseif($mode == 'month')
                            {!! Form::hidden('xyz', 'xyz3') !!}
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                <input type="hidden" name="cmd" value="_s-xclick">
                                <input type="hidden" name="hosted_button_id" value="VL2MVS5BVXG4E">
                                <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
                                <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
                                </form>
                            </div>
                        </div>

                </div>
            </div>
            <p class="text-muted text-center">En tu estado de cuenta apareceremos como: For1Ever complete</p>
            <p class="text-muted text-center">Tu IP será guardada en esta Transacción.</p>
            <p class="text-muted text-center">Si necesita ayuda de click <a href="{{route('contact')}}" target="_blank">aquí</a></p>
        </div>
        <div class="col-md-1">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.5.0/js/conekta.js"></script>
<script src="{{ asset('assets/js/conekt.js') }}"></script>
<script type="text/javascript">
    Conekta.setPublishableKey('key_CsHGWrz1qqrV3rbdeCZ9q4Q');
</script>
@endsection 