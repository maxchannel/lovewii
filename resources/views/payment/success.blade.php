@extends('layouts.app')

@section('title', 'Felicidades')
@section('meta-description', 'Felicidades')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Felicidades</div>
                <div class="panel-body text-center">
                    Ya eres Usuario Completo!

                    <h2>Ahora Puedes:</h2>
                    <a href="{{ route('coach') }}" class="btn btn-lg btn-ora">Tener un Coach</a><br><br>
                    <a href="{{route('views') }}" class="btn btn-lg btn-gr">Ver Quien Visita tu Perfil</a><br><br>
                    <a href="{{ route('horoscope') }}" class="btn btn-lg btn-cielo">Leer tu Hóroscopo y Compatibles</a><br><br>
                    <a href="{{ route('search_advance') }}" class="btn btn-lg btn-re">Busqueda Avanzada</a><br><br>
                    <a href="{{ route('settings_image') }}" class="btn btn-lg btn-ts">Subir hasta 25 Imágenes</a><br><br>
                    <h3>Navegar sin anuncios...</h3>
                    <h3>y más...</h3>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection