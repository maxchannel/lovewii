<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    @if(View::hasSection('title'))
    <title>@yield('title')</title>
    @else
    <title>For1Ever</title>
    @endif
    @if(View::hasSection('meta-description'))
    <meta name="description" content="@yield('meta-description')">
    @else
    <meta name="description" content="Encuentra la pareja ideal en el menor tiempo posible, haz busquedas avanzadas en por ciudades y por signo.">
    @endif
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- Styles -->
    <link href="{!! asset('assets/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('assets/css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('assets/css/footer.css') !!}" rel="stylesheet">
    <link href="{!! asset('assets/css/nav.css') !!}" rel="stylesheet">
    @yield('script_head')
</head>
<body id="app-layout">
    @include('partials.analytics')
    @yield('script_body')

    <nav class="navbar navbar-default navbar-static-top nav-wii">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand nav-wii" href="{{ route('home') }}">
                    <span class="glyphicon glyphicon-heart" aria-hidden="true"></span> For1Ever
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if(Auth::guest())
                        <li><a href="{{ route('tests') }}">Tests</a></li>
                        <li><a href="{{ route('views') }}">Mis Visitas</a></li>
                        <li><a href="{{ route('horoscope_all') }}">@lang('general.horoscopo')</a></li>
                    @else
                        <li><a href="{{ route('messages') }}"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> @lang('general.messages')
                            @if(Auth::user()->getParticipants() > 0)
                                <span class="badge badge-color">{{ Auth::user()->getParticipants() }}</span>
                            @endif
                        </a></li>
                        <li><a href="{{ route('horoscope') }}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> @lang('general.horoscopo')</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> @lang('general.plus')
                                @if(Auth::user()->getViews() > 0)
                                     <span class="badge badge-color">{{ Auth::user()->getViews() }}</span>
                                @endif
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @if(Auth::user()->userhoroscope->horoscope_id != "")
                                    <li><a href="{{ route('horoscope_match', [str_slug(Auth::user()->horoscope()->name)]) }}">ver {{ Auth::user()->horoscope()->name }}</a></li>
                                @endif
                                <li><a href="{{ route('views') }}"> 
                                    Mis Visitas
                                    @if(Auth::user()->getViews() > 0)
                                        <span class="badge badge-color">{{ Auth::user()->getViews() }}</span>
                                    @endif
                                </a></li>
                                @if(Auth::user()->city != "")
                                    <li><a href="{{ route('search', ['city'=>Auth::user()->city]) }}"> de {{Auth::user()->city}}</a></li>
                                @endif
                                <li><a href="{{ route('coach') }}"> @lang('general.coach')</a></li>
                                <li><a href="{{ route('tests') }}"> Tests</a></li>
                            </ul>
                        </li>
                        <li>    
                            <a href="{{ route('notifications') }}"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> 
                                @lang('general.nots') 
                                @if(Auth::user()->getNotifications() > 0)
                                    <span class="badge badge-color">{{ Auth::user()->getNotifications() }}</span>
                                @endif
                            </a>
                        </li>
                    @endif
                    {!! Form::open(['route'=>'search', 'method'=>'GET', 'role'=>'form', 'class'=>'navbar-form navbar-left']) !!}
                        <div class="form-group">
                            {!! Form::text('q',null,['class'=>'form-control', 'placeholder'=>'Personas, ciudades...']) !!}
                        </div>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    {!! Form::close() !!}
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if(Auth::guest())
                        <li><a href="{{ route('signin') }}"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> @lang('general.signin')</a></li>
                        <li><a href="{{ route('signup') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> @lang('general.signup')</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('settings_profile') }}"><span class="glyphicon glyphicon-cog"></span> @lang('general.settings')</a></li>
                                <li><a href="{{ route('profile', [Auth::user()->username]) }}"><span class="glyphicon glyphicon-user"></span> @lang('general.profile')</a></li>
                                <li><a href="{{ route('contact') }}"><span class="glyphicon glyphicon-wrench"></span>  @lang('general.support')</a></li>
                                <li><a href="{{ route('logout') }}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> @lang('general.logout')</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    @yield('script_footer')
</body>
</html>