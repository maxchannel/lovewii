                <ul class="nav nav-tabs nav-justified">
                    <li role="presentation"><a href="{{ route('profile', [Auth::user()->username]) }}">Ver Perfil</a></li>
                    <li role="presentation"><a href="{{ route('settings_about_xl') }}">Editar Perfil</a></li>
                    <li role="presentation"><a href="{{ route('settings_image') }}">Subir Imágenes</a></li>
                </ul>