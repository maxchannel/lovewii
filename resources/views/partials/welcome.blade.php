<div class="container" >
    <div class="row balance2">
        <div class="col-xs-12 col-md-1"></div>
        <div class="col-xs-12 col-md-7 text-center">
            @if(\Request::input('m') == 'ln')
                <h1>Encuentra la Pareja que Buscas:</h1>
                <h4>Mujeres que les guste el Rock en Medellín y sean Aries</h4>
                <h4>Hombres que sean Hogareños en Hermosillo</h4>
                <h4>Hombres que no fumen o tomen en mi Ciudad</h4>
                <h4>y más...</h4>
                <br>
                <a href="{{route('signup')}}" class="btn btn-success btn-lg btn-sg">Regístrate y Lógralo</a>
            @elseif(\Request::input('m') == 'hors')
                <h1>¿Con quien es compatible Sagitario?</h1>
                <h4>Soy Tauro... ver mi compatibilidad</h4>

                {!! Form::open(['route'=>'horoscope_match_prev', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                {!! Form::select('sign',[''=>'Mi signo es...']+$horos,null,['class'=>'form-control', 'required']) !!}
                <br>
                <button class="btn btn-success btn-lg btn-sg">Buscar</button>
                {!! Form::close() !!}
            @else
                <h1>Encuentra la Pareja que Buscas:</h1><br>
                <h4>Soy</h4>
                {!! Form::open(['route'=>'advance_prev', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                {!! Form::select('o',[
                    ''=>'Seleccionar',
                    '1'=>'Hombre buscando Mujer',
                    '2'=>'Hombre buscando Hombre',
                    '3'=>'Mujer buscando Hombre',
                    '4'=>'Mujer buscando Mujer',
                ],null,['class'=>'form-control', 'required']) !!}
                <br>
                <button class="btn btn-success btn-lg btn-sg">Buscar</button>
                {!! Form::close() !!}
            @endif
        </div>
        <div class="col-xs-12 col-md-4">
            <ul class="list-inline">
                <table class="table table-hover">
                    @foreach($users as $i => $user)
                    <tr>
                        <td> 
                            <a href="{{ route('profile', [$user->username]) }}"><img src="{{ $avatar[$i] }}" class="aob"/></a>
                            <a href="{{ route('profile', [$user->username]) }}">{{ $user->name }}</a><br>
                            {{ substr($user->about->description,0,140) }}...
                        </td>
                    </tr>
                    @endforeach
                </table>
            </ul>
        </div>

    </div>

    @include('partials.footer')
</div>