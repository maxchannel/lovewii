    <footer class="balance3 foot">
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <ul class="list-inline">
                    <li>For1Ever</li>
                    <li>Nuestros goles son tus goles, queremos que encuentres una persona con mucha química, en el menor 
                        tiempo posible.</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>Explore</p>
                <ul class="list-unstyled">
                    <li><a href="{{route('horoscope_all')}}">Compatibles con tu Horóscopo</a></li>
                    <li><a href="{{ route('search', ['muse'=>3]) }}">Encontrar por Gustos Musicales</a></li>
                    <li><a href="{{ route('search', ['horos'=>1]) }}">Encontrar por Hóroscopo</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>Ayuda</p>
                <ul class="list-unstyled">
                    <li><a href="{{route('contact')}}">Contacto</a></li>
                    <li><a href="{{route('privacy')}}">Privacidad</a></li>
                    <li><a href="{{route('terms')}}">Terminos de Uso</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-3">
                <p>Social</p>
                <ul class="list-inline">
                    <li><a href="https://twitter.com/for1everapp" target="_blank"><i class="fa fa-twitter-square social-buttons__icon" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.facebook.com/for1everapp" target="_blank"><i class="fa fa-facebook-square social-buttons__icon" aria-hidden="true"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCZ5dhZjq6ge5v74wPb1Wmtw" target="_blank"><i class="fa fa-youtube-square social-buttons__icon" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </footer>