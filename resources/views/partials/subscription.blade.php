        @if(Session::has('message'))
            @include('partials.btns.success')
        @endif
        @include('partials.errorMessages')
        {!! Form::open(['route'=>'subscription_update', 'method'=>'POST', 'role'=>'form', 'style'=>'border:1px solid #ccc;padding:13px;text-align:center;']) !!}
            <div class="form-group">
                <p class="text-info">Recibirás tu hóroscopo por email</p>
                {!! Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!} 
            </div>
            <input type="hidden" value="ellectv" name="uri"/><input type="hidden" name="loc" value="es_ES"/>
            <input type="submit" value="Suscribirme" class="btn btn-success" />
            <p class="text-danger">Recuerda Confirmar tu Email</p>
        {!! Form::close() !!}