            <div class="list-group">
                <a href="{{ route('admin_horoscope') }}" class="list-group-item">Horoscope<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('admin_horoscope_delete', ['aries']) }}" class="list-group-item">Horoscope (Delete)<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('admin_stats') }}" class="list-group-item">Stats<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('admin_subscriptions') }}" class="list-group-item">Subscripción<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('admin_contact') }}" class="list-group-item">Contacto<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('admin_verify') }}" class="list-group-item">Verificar<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
            </div>