            <div class="list-group">
                <a href="{{ route('settings_about_xl') }}" class="list-group-item"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Perfil Completo<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_profile') }}" class="list-group-item">Cuenta<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_about') }}" class="list-group-item">Acerca de mí<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_intention') }}" class="list-group-item">Preferencia<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_avatar') }}" class="list-group-item">Avatar<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_password') }}" class="list-group-item">Contraseña<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_horoscope') }}" class="list-group-item">Hóroscopo<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <!-- <a href="{{ route('settings_lang') }}" class="list-group-item">Idioma<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a> -->
                <a href="{{ route('settings_payment') }}" class="list-group-item">Suscripción y Pagos<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_notifications') }}" class="list-group-item">Notificaciones<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_notifications_email') }}" class="list-group-item">Notificaciones por Email<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                <a href="{{ route('settings_privacy') }}" class="list-group-item">Seguridad y Privacidad<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
            </div>