@extends('layouts.app')

@if(\Request::input('page') > 1)
    @section('title', 'Resultados para: '.$q.' en la Página '.\Request::input('page'))
    @section('meta-description', 'Resultados para: '.$q.' en la Página '.\Request::input('page'))
@else
    @section('title', 'Resultados para: '.$q)
    @section('meta-description', 'Resultados para: '.$q)
@endif

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::check())
                @if(Auth::user()->isNormal())
                    @include('partials.ad')<br>
                @endif
            @else
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Busqueda</h1>
                <a href="{{route('search_advance')}}">Avanzada</a>
                @if($q)
                <p>Termino: {{ $q }}</p>
                @endif
                <div class="row">
                    {!! Form::open(['route'=>'search', 'method'=>'GET', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                    <div class="col-md-3">
                        {!! Form::label('country_id', 'País') !!}
                        {!! Form::select('country_id',[''=>'Seleccionar']+$countries,\Request::input('country_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('city', 'Ciudad') !!}
                        {!! Form::text('city',\Request::input('city'),['class'=>'form-control', 'placeholder'=>'Nombre Completo']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('age', 'Edad') !!}
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::number('age',\Request::input('age'),['class'=>'form-control', 'placeholder'=>'Desde']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::number('age_to',\Request::input('age_to'),['class'=>'form-control', 'placeholder'=>'Hasta']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('horos', 'Horóscopo') !!}
                        {!! Form::select('horos',[''=>'Seleccionar']+$horos,\Request::input('horos'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('gender_id', 'Sexo') !!}
                        {!! Form::select('gender_id',[''=>'Seleccionar','1'=>'Hombre','2'=>'Mujer'],\Request::input('gender_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('search_id', 'Que Busque') !!}
                        {!! Form::select('search_id',[''=>'Seleccionar','1'=>'Hombre','2'=>'Mujer'],\Request::input('search_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('para_id', 'Para') !!}
                        {!! Form::select('para_id',[''=>'Seleccionar']+$intentions,\Request::input('para_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('muse', 'Música #1 en su vida') !!}
                        {!! Form::select('muse',[''=>'Seleccionar']+$music,\Request::input('muse'),['class'=>'form-control']) !!}
                        <br>
                    </div>
                </div>
                <br>
                <button class="btn btn-success">Buscar</button>
                {!! Form::close() !!}
            </div>
            <br>

            @if($q and count($horoscopes) > 0)
            <table class="table table-hover">
                @foreach($horoscopes as $i => $horoscope)
                <tr>
                    <td> 
                        <a href="{{ route('profile', [$horoscope->name]) }}">{{ $horoscope->name }}</a><br>
                        <a href="{{ route('horoscope_match', [str_slug($horoscope->name)]) }}" class="btn btn-primary">Ver Compatibles</a>
                    </td>
                </tr>
                @endforeach
            </table>
            @endif

            @if(count($results) > 0)
            <table class="table table-hover">
                @foreach($results as $i => $user)
                <tr>
                    <td> 
                        <a href="{{ route('profile', [$user->username]) }}"><img src="{{ $avatar[$i] }}" class="aob"/></a>
                        <a href="{{ route('profile', [$user->username]) }}">{{ $user->name }}</a><br>
                        @if($user->city != "")
                        {{$user->city}}
                        @endif
                        @if($user->country_id != "")
                        , {{$user->country->name}}
                        @endif
                        @if($user->userhoroscope->horoscope_id != "")
                        - {{ $user->userhoroscope->horoscope->name }}
                        @endif
                        @if($user->info->bth != "")
                        - {{getAge($user->info->bth)}}
                        @endif
                        <br>
                        {{ substr($user->about->description,0,140) }}...
                    </td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $results->render()) !!} 
            @endif

            @if(count($results) == 0 and count($horoscopes) == 0)
                <h3>Sin resultados</h3> | <a href="{{route('search_advance')}}">Refinar la busqueda</a>
            @endif

        </div>

        <div class="col-md-2">
        </div>
    </section>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection