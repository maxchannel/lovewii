@extends('layouts.app')

@section('title', 'Busqueda Avanzada')
@section('meta-description', 'Busqueda Avanzada')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Busqueda Completa</h1>

                <div class="row">
                    {!! Form::open(['route'=>'search', 'method'=>'GET', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                    <div class="col-md-3">
                        {!! Form::label('country_id', 'País') !!}
                        {!! Form::select('country_id',[''=>'Seleccionar']+$countries,\Request::input('country_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('gender_id', 'Sexo') !!}
                        {!! Form::select('gender_id',[''=>'Seleccionar','1'=>'Hombre','2'=>'Mujer'],\Request::input('gender_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('search_id', 'Que Busque') !!}
                        {!! Form::select('search_id',[''=>'Seleccionar','1'=>'Hombre','2'=>'Mujer'],\Request::input('search_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('para_id', 'Para') !!}
                        {!! Form::select('para_id',[''=>'Seleccionar']+$intentions,\Request::input('para_id'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('city', 'Ciudad') !!}
                        {!! Form::text('city',\Request::input('city'),['class'=>'form-control', 'placeholder'=>'Ciudad']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('horos', 'Horóscopo') !!}
                        {!! Form::select('horos',[''=>'Seleccionar']+$horos,\Request::input('horos'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('smo', 'Que Fume') !!}
                        {!! Form::select('smo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('smo'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('muse', 'Música #1 en su vida') !!}
                        {!! Form::select('muse',[''=>'Seleccionar']+$music,\Request::input('muse'),['class'=>'form-control']) !!}
                        <br>
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('age', 'Edad') !!}
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::number('age',\Request::input('age'),['class'=>'form-control', 'placeholder'=>'Desde']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::number('age_to',\Request::input('age_to'),['class'=>'form-control', 'placeholder'=>'Hasta']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('height', 'Altura') !!}
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::select('height',[''=>'Desde']+config('options.heights'),\Request::input('height'),['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::select('height_to',[''=>'Desde']+config('options.heights'),\Request::input('height_to'),['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        {!! Form::label('drin', 'Que Tome') !!}
                        {!! Form::select('drin',[''=>'Seleccionar']+$frequences,\Request::input('drin'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('gy', 'Que Haga Ejercicio') !!}
                        {!! Form::select('gy',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('gy'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('wo', 'Que Tenga Trabajo') !!}
                        {!! Form::select('wo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('wo'),['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('re', 'Que Lea') !!}
                        {!! Form::select('re',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('re'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('mo', 'Que vaya al Cine') !!}
                        {!! Form::select('mo',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('mo'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('con', 'Que vaya a Conciertos') !!}
                        {!! Form::select('con',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('con'),['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('home', 'Que sea Hogareño') !!}
                        {!! Form::select('home',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('home'),['class'=>'form-control']) !!}
                    </div>

                    <div class="col-md-3">
                        {!! Form::label('kds', 'Que quiera Hijos') !!}
                        {!! Form::select('kds',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('kds'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('hvkds', 'Que tenga Hijos') !!}
                        {!! Form::select('hvkds',[''=>'Seleccionar','1'=>'Si','2'=>'No'],\Request::input('hvkds'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('tato', 'Que tenga Tatuajes') !!}
                        {!! Form::select('tato',[
                            ''=>'Seleccionar',
                            '0'=>'0',
                            '1'=>'1',
                            '2'=>'2',
                            '3'=>'3',
                            '4'=>'4+'
                        ],\Request::input('tato'),['class'=>'form-control']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('timido', '¿Timido?') !!}
                        {!! Form::select('timido',[
                            ''=>'Seleccionar',
                            '1'=>'Tímido',
                            '2'=>'Atrevido',
                        ],\Request::input('timido'),['class'=>'form-control']) !!}
                    </div>



                    <div class="col-md-3">
                        {!! Form::label('knd', 'Amable y Cuidados@?') !!}
                        {!! Form::select('knd',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control']) !!}
                        <br>
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('her', 'Que sepa escuchar?') !!}
                        {!! Form::select('her',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control']) !!}
                        <br>
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('pts', 'Mascotas?') !!}
                        {!! Form::select('pts',[''=>'Seleccionar','1'=>'Si','2'=>'No'],null,['class'=>'form-control']) !!}
                        <br>
                    </div>
                </div>
                <br>
                <button class="btn btn-tm">Buscar</button>
                {!! Form::close() !!}
            </div>
            <br>
        </div>

        <div class="col-md-2">
        </div>
    </section>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection