@extends('layouts.app')

@section('title', 'Términos y Condiciones')
@section('meta-description', 'Términos y Condiciones')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <h1>Términos y condiciones</h1>
                <hr>
                <h3>Definiciones</h3>
                <p>"Usted" se refiere a cualquier usuario que use este sitio web, aplicación movil o cualquier otro servicio asociado a este.</p>
                <p>"Servicio (s)" se refiere al uso que usted le de a nuestro sitio web.</p>
                <p>"Nosotros" hace referencia a la administración y representación legal de For1Ever</p>

                <h3>Acceso</h3>
                <p>
                    Usted debe tener una edad miníma de 18 años para poder usuario nuestro sitio web, acepta entonces que por el hecho de ya usarlo
                    tiene la edad minima requerida, además de no contar con la edad miníma Nos reservamos la desactivación de la cuenta.
                </p>
                <h3>Consentimiento</h3>
                <p>
                    El presente Contrato constituye Su consentimiento, y para permanecer dentro del Servicio acepta que cumplira con él.
                </p>

                <h3>Normas de la comunidad</h3>
                <p>
                    a. Los usuarios serán respetuosos y se mantendrán en un tono cordial con los demás, cualquier amenaza, presión, ostigamiento,
                    se considerará como ofensa y puede resultar en el pedido de expulsión del servicio.<br>
                    b. Deberá abstenerce de crear perfiles para uso comercial o de publicidad, cualquier perfil de dicha indole será desactivado,
                    y en caso de ser Usuario Completo no se reembolzará el monto de los días restantes activos.<br>
                    c. En ningún perfil ni en nigún caso se permitirá el uso de imágenes pornográficas de ningún tipo ni en el avatar,
                    ni en el album del usuario, de lo contrario será se desactivará su cuenta de forma inmediata.<br>
                    d. No deberá proporcionar información financiera, de domicilios, telefonos o cualquier otra información personal ni en
                    su perfil, album, avatar, mensajes, status o cualquier medio a traves de nuestro servicio.<br>
                    e. Se abstendrá de divulgar cualquier texto, imágen, video o cualquier otro elemento de los perfiles de los otros usuario fuera
                    de nuestro servicio.
                </p>
                <h3>Interacción con Otros Usuarios</h3>
                <p>
                    Usted es el único responzable de como interactua con los demás, acepta que Nosotros no investigamos antecedentes penales de los usuarios
                    ni niguna otra clase de historial de los mismos.<br>
                    En nigún caso For1Ever se hará cargo de los daños que con su comportamiento pueda provocar a otros usuarios del Servicio.
                </p>
                <h3>Formulación de Reclamo por Derechos de Autor</h3>
                <p>
                    Le pedimos que para dicho reclamo nos proporcione: link(s) que cree que infringe derechos de autor, 
                    firma física o electronica de la persona autorizada para hacer este reclamo, descripción de la obra que ha sido infringida,
                    Su dirección, Su número de teléfono y Su dirección de correo electrónico,
                    Su pena de condena por falso testimonio, de que la información contenida en Su comunicación es exacta, y de que Usted es el titular de los derechos de autor o que está autorizado para actuar en nombre de éste.
                </p>



                <h3>Facturación y Pagos</h3>
                <p>
                    Para poder acceder a nuestra función de Usuario Completo debe proporcionarnos su tarjeta de credito valida,
                    el pago debe de realizarce y confirmarse posteriormente para poder continuar gozando de esta función.
                    El manejo de su información financiera esta sujeta a nuestra <a href="{{ route('privacy') }}">Política de Privacidad</a>
                    Lo cual asegura el correcto manejo de los mismo.<br>

                </p>
                <h3>Renovación Automática</h3>
                <p>
                    Acepta que por default la renovación automática esta activada y que esta se hará por el primer periodo que usted
                    decidio tomar, sea este mensual, semestral o anual, En caso de que sea promoción la renovación se hará mensual.<br>
                    Para desactivar esta función puede hacerlo desde su menu de configuración en cualquier momento. 
                </p>
                <h3>Suspensión por Impago</h3>
                <p>
                    En el caso de que su suscripción no pueda ser cobrada por cualquier motivo, bajaremos su usuario a uno normal, mientras esta situación se aclara.
                </p>

                <h3>Devoluciones y Reembolsos</h3>
                <p>For1Ever solo tiene 1 caso en el que hace reembolsos a usuarios:</p>
                <p>
                    -Cuando un hijo es y solo si es menor de edad, y usa la tarjeta de crédito de los padres sin su concentimiento.
                    El reclamo debe de hacerse en el siguiente link <a href="{{ route('contact') }}">Contacto</a>, Explicando la situación con fechas, montos
                    y detalles de la transacción. No se hará un reembolso total, sino el equivalente a los días de suscripción que queden
                    activos en la cuenta asociada al cargo. El usuario podrá seguir activo en la plataforma como usuario normal siempre y cuando respete las 
                    normas de la comunidad.
                </p>

                <p>Vigente desde el 24 de octubre de 2016</p>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div> <!-- /container -->
@endsection