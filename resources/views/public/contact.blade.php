@extends('layouts.app')

@section('title', 'Contacto')
@section('meta-description', 'Contacto')

@section('content')
<div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center plan-name">
                    Contacto
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            @if(Session::has('message'))
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @include('partials.errorMessages')

                            @if(\Request::input('mode') == "report-user" and \Request::input('id'))
                                <p>¿Motivo del reporte?*</p>
                                {!! Form::open(['route'=>['contact_report', \Request::input('id')], 'method'=>'POST', 'role'=>'form']) !!}
                                {!! Form::select('report',[
                                ''=>'',
                                '0'=>'Perfil Falso',
                                '1'=>'Contenido Violento',
                                '2'=>'Fraude',
                                '3'=>'Duplicado',
                                '4'=>'Perfil Ilegal',
                                '5'=>'Contenido inapropiado'] ,null,['class'=>'form-control']) !!}
                                {!! Form::label('comments', 'Comentarios(Opcional)') !!}
                                {!! Form::textarea('comments',null,['class'=>'form-control', 'placeholder'=>'Escribe información detallada']) !!}
                                {!! Form::hidden('xyz', \Request::input('id')) !!}
                                <br>
                                <button type="submit" class="btn btn-primary">
                                    Reportar
                                </button>
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['route'=>'contact_update', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                                {!! Form::email('email',null,['class'=>'form-control', 'placeholder'=>'Email', 'required']) !!}
                                <br>
                                {!! Form::select('sub',
                                    [
                                        ''=>'Seleccionar Asunto',
                                        '1'=>'Usabilidad',
                                        '2'=>'Pagos',
                                        '3'=>'Feedback',
                                        '4'=>'Reportar Bug'
                                    ],null,['class'=>'form-control']) !!}    
                                <br>
                                {!! Form::textarea('content',null,['class'=>'form-control', 'placeholder'=>'Mensaje de Contacto', 'required']) !!}    
                                <br>
                                <button type="submit" class="btn btn-success">
                                    Enviar
                                </button>
                                {!! Form::close() !!}
                            @endif
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
@endsection