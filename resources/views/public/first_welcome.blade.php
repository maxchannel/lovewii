@extends('layouts.app')

@section('title', 'Bienvenido')
@section('meta-description', 'Bienvenido')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection 

@section('content')
<div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center plan-name">
                    Bienvenido
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 text-center">
                            <h2>¿Qué quieres hacer Primero?</h2>
                            <a href="{{ route('horoscope') }}" class="btn btn-lg btn-ora">Ver Mi Hóroscopo</a><br><br>
                            <a href="{{route('horoscope_match', [strtolower(\Auth::user()->horoscope()->name)]) }}" class="btn btn-lg btn-gr">Perfiles Compatibles con mi Hóroscopo</a><br><br>
                            <a href="{{ route('settings_image') }}" class="btn btn-lg btn-cielo">Subir Imágenes a mi Album</a><br><br>
                            <a href="{{ route('tests') }}" class="btn btn-lg btn-re">Tomar un Test</a><br><br>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
@endsection