@extends('layouts.app')

@section('title', 'Eliminar Hóroscopo')
@section('meta-description', 'Eliminar Hóroscopo')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Días</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Fecha</th>
                            <th>Content</th>
                            <th></th>
                        </tr>
                        @foreach($days as $day)
                        <tr data-id="{{ $day->id }}">
                            <td>{{ $day->fecha }}</td>
                            <td>{!! $day->content !!}</td>
                            <td>
                                <a href="#" class="btn-delete">Eliminar</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {!! str_replace('/?', '?', $days->render()) !!} 
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>

{!! Form::open(['route'=>['admin_horoscope_delete_update', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();
        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();
        row.fadeOut();
        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection