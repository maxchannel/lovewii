@extends('layouts.app')

@section('title', 'Verificar')
@section('meta-description', 'Verificar')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Verificar</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Fecha</th>
                            <th>Imágen</th>
                            <th>Usuario</th>
                            <th></th>
                        </tr>
                        @foreach($verifies as $key => $verify)
                        <tr data-id="{{ $verify->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $verify->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>
                                <!-- Modal -->
                                <div class="modal fade" id="myModal{{$verify->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                     <div class="modal-dialog modal-lg">
                                       <div class="modal-content">
                                         <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                           <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                         </div>
                                         <div class="modal-body">
                                            <img src="{{ $temp[$key] }}" class="img-responsive" />
                                         </div>
                                         <div class="modal-footer">
                                             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         </div>
                                       </div>
                                     </div>
                                </div>
                                <!-- Modal -->
                                <a href="" data-toggle="modal" data-target="#myModal{{$verify->id}}"><img src="{{ $temp[$key] }}" class="order-image-min" /></a>
                            </td>
                            <td>
                                <a href="{{ route('profile', [$verify->user->username]) }}" target="_blank">{{$verify->user->name}}</a>
                            </td>
                            <td>
                                <a href="#" class="btn-delete">Verificar</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {!! str_replace('/?', '?', $verifies->render()) !!} 
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::open(['route'=>['admin_verify_destroy', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection