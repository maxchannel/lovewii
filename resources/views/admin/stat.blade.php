@extends('layouts.app')

@section('title', 'Estadísticas Completas')
@section('meta-description', 'Estadísticas Completas')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Estadísticas Completas</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Usuarios</th>
                            <th>Mensajes</th>
                            <th>Subscripciones</th>
                            <th>Contactos</th>
                            <th>Usuarios Rep.</th>
                            <th>Imágenes Alb.</th>
                        </tr>
                        <tr>
                            <td>{!! $users !!}</td>
                            <td>{!! $messages !!}</td>
                            <td>{!! $subscriptions !!}</td>
                            <td>{!! $contacts !!}</td>
                            <td>{!! $reports !!}</td>
                            <td>{!! $reports !!}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection