@extends('layouts.app')

@section('title', 'Escribir Hóroscopo')
@section('meta-description', 'Escribir Hóroscopo')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Horoscope</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::open(['route'=>'admin_horoscope_update', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Signo</label>
                            <div class="col-md-6">
                                {!! Form::select('horoscope_id',[''=>'Seleccionar']+$horoscopes,null,['class'=>'form-control', 'required']) !!} 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Día</label>
                            <div class="col-md-6">
                                <input type="date" name="fecha" min="2016-01-02" value="{{ date('Y-m-d') }}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Content</label>
                            <div class="col-md-6">
                                {!! Form::textarea('content',null,['class'=>'form-control', 'placeholder'=>'Content','required']) !!}   
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection