@extends('layouts.app')

@section('title', 'Verificar')
@section('meta-description', 'Verificar')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Solicitar Perfil Verificado</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    @if(count(Auth::user()->verification_pet) > 0)
                        <h3>Ya haz solicitado la verificación de tu perfil, en breve nos pondremos en contacto.</h3>
                    @else 
                    <h4>Para poder verificar tu perfil, sube una foto tuya con tu nombre de usuario o email escrito en un papel.</h4>
                    {!! Form::open(['route'=>'verify_update', 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                               {!! Form::file('file', ['accept' => 'image/*']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('partials.btn_update')
                            </div>
                        </div>
                    {!! Form::close() !!}
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection