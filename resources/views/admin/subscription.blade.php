@extends('layouts.app')

@section('title', 'Subscriptores por Email')
@section('meta-description', 'Subscriptores por Email')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Email Subscriptions</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Fecha</th>
                            <th>Verificado</th>
                            <th>Email</th>
                        </tr>
                        @foreach($subscriptions as $sub)
                        <tr data-id="{{ $sub->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $sub->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>{!! $sub->v !!}</td>
                            <td>{!! $sub->email !!}</td>
                        </tr>
                        @endforeach
                    </table>
                    {!! str_replace('/?', '?', $subscriptions->render()) !!} 
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection