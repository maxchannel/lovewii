@extends('layouts.app')

@section('title', 'Contactos Enviados')
@section('meta-description', 'Contactos Enviados')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-3">
            @include('partials.menu_admin')
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Contactos</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Fecha</th>
                            <th>Content</th>
                            <th>Email</th>
                        </tr>
                        @foreach($contacts as $c)
                        <tr data-id="{{ $c->id }}">
                            <td>
                                <script>
                                moment.locale("es");
                                document.writeln(moment.utc("{{ $c->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                </script>
                            </td>
                            <td>{!! $c->content !!}</td>
                            <td>{!! $c->email !!}</td>
                        </tr>
                        @endforeach
                    </table>
                    {!! str_replace('/?', '?', $contacts->render()) !!} 
                </div>
            </div>
        </div>
        <div class="col-md-1 ">
        </div>
    </div>
</div>
@endsection