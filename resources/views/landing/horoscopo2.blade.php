@extends('layouts.landing')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection

@section('content')
<div class="container-fluid lan-horos-2 Absolute-Center">
    <div class="row">
    	<div class="green_border"></div>
        <div class="col-md-6 col-md-offset-3">
        	<section class="row text-center">
        		<h2><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> For1Ever</h2>
        		<h1>Cúal es tu Horóscopo?</h1><br>
                {!! Form::select('exp_month',config('signos'),null,['class'=>'form-control','data-conekta'=>'card[exp_month]']) !!}
                <br>
                <a href="" class="button">Econtrar Compatibilidad</a>
        	</section>
        </div>
    </div>
</div>
@endsection