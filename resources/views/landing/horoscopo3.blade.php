@extends('layouts.landing')

@section('script_head')
{!! Html::style('assets/css/landing.css') !!}
@endsection

@section('content')
<div class="container-fluid lan-horos-2 Absolute-Center">
    <div class="row">
    	<div class="green_border"></div>
        <div class="col-md-6 col-md-offset-3">
        	<section class="row text-center">
        		<h2><span class="glyphicon glyphicon-heart" aria-hidden="true"></span> For1Ever</h2>
        		<h1>Tu Horóscopo Hoy:</h1><br>
                <p>
                    Si tienes pareja y esta te pone obstáculos en tus metas, sueños y decisiones no vale la pena seguir ahí. 
                    Tu pasado se hará presente y quizás te duela o te afecte, quita de tu camino todo aquello que te haga daño 
                    o afecte pon los pies en la tierra y...
                </p>
                {!! Form::text('signo',null,['class'=>'form-control', 'maxlength'=>'40', 'placeholder'=>'Email']) !!}
                <br>
                <a href="" class="button">Horoscopo Completo</a>
        	</section>
        </div>
    </div>
</div>
@endsection