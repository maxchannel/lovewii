@extends('layouts.app')

@section('script_head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
@endsection

@section('content')
@if(Auth::check())
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
            <!--
            <a href="" class="btn btn-info btn-ts">Mis Compatibles</a><br><br>
            <a href="" class="btn btn-info btn-ts">Random</a><br><br>
            <a href="" class="btn btn-info btn-ts">Calificar Fotos</a><br><br>
            -->
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Feed</h1>
                <p>Personas sugeridos a tu perfil</p>
                <br>
            </div>

            <table class="table table-hover">
                @foreach($users as $i => $user)
                <tr>
                    <td> 
                        <a href="{{ route('profile', [$user->username]) }}"><img src="{{ $avatar[$i] }}" class="aob"/></a>
                        <a href="{{ route('profile', [$user->username]) }}">{{ $user->name }}</a><br>
                        @if($user->city != "")
                        {{$user->city}}
                        @endif
                        @if($user->country_id != "")
                        , {{$user->country->name}}
                        @endif
                        @if($user->userhoroscope->horoscope_id != "")
                        - {{ $user->userhoroscope->horoscope->name }}
                        @endif
                        @if($user->info->bth != "")
                        - {{getAge($user->info->bth)}}
                        @endif
                        <br>
                        {{ substr($user->about->description,0,140) }}...
                    </td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $users->render()) !!} 
            <br>
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-cog"></span> Configuración</div>
                <div class="panel-body text-center">
                    <a href="{{ route('settings_notifications') }}">Preferencias de Feed</a><br>
                </div>
            </div>
        </div>
    </section>

    <div class="balance"></div>
</div>
@else
    @include('partials.welcome')
@endif


@endsection