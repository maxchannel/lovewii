@extends('layouts.app')

@section('title', 'Messages')
@section('meta-description', 'Messages')

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Messages</h1>
                <p>Una forma útil de conectar con los demás.</p>
                <br>
            </div>
            <br>
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @if($conversations->isEmpty())
                <p>Aún no tienes Conversaciones</p>
            @else
                <table class="table table-hover">
                    @foreach($conversations as $i => $conversation)
                        @if($conversation->total_messages() > 0)
                        <tr>
                            <td> 
                                <a href="{{ route('message_me', [$conversation->id]) }}"><img src="{{$avatar[$i]}}" class="aoi pull-left"/></a>
                                <a href="{{ route('message_me', [$conversation->id]) }}">
                                    @if($conversation->recipients_count() > 0)
                                        {{ $conversation->recipients()->last()->user->name }}
                                        @if($conversation->getParticipants() > 0)
                                            <span class="badge badge-color">{{ $conversation->getParticipants() }}</span>
                                        @endif
                                    @endif
                                </a><br>
                                {{ $conversation->messages->last()->body }}
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </table>

                {!! str_replace('/?', '?', $conversations->render()) !!} 
            @endif
            <br>
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-cog"></span> Configuración</div>
                <div class="panel-body text-center">
                    <a href="{{ route('settings_privacy') }}">Preferencias de Messages</a><br>
                </div>
            </div>
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection