@extends('layouts.app')

@section('title', 'Mensaje Individual')
@section('meta-description', 'Mensaje Individual')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            <a href="{{ route('messages') }}" class="btn btn-info btn-ts">Volver a Messages</a>
            <br><br>
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Messages</h1>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-lg">
                       <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title" id="myModalLabel">{{$conversation->recipients()->last()->user->name}}</h4>
                         </div>
                         <div class="modal-body">
                            @if($conversation->recipients_count() > 0)
                            <img src="{{$avatar[$conversation->recipients()->last()->user->username]}}" class="img-responsive" />
                            @endif
                         </div>
                         <div class="modal-footer">
                             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                         </div>
                       </div>
                     </div>
                </div>
                <!-- Modal -->
                @if($conversation->recipients_count() > 0)
                <a href="" data-toggle="modal" data-target="#myModal"><img src="{{$avatar[$conversation->recipients()->last()->user->username]}}" class="aoh" /></a>
                @endif
                <br>
                @if($conversation->recipients_count() > 0)
                <a href="{{ route('profile', [$conversation->recipients()->last()->user->username]) }}">
                    {{ $conversation->recipients()->last()->user->name }}
                </a><br>
                @endif
            </div>
            <br>

            @include('partials.errorMessages')
            <table class="table table-hover">
                @foreach($messages as $i => $message)
                <tr>
                    <td>
                        <img src="{{$avatar[$message->user->username]}}" class="aoi pull-left"  />
                        <a href="{{route('profile',[$message->user->username])}}">{{ $message->user->name }}</a> 
                        - <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $message->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                        <br>
                        {{ $message->body }}
                    </td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $messages->render()) !!} 

            {!! Form::open(['route'=>['message_me_update', $conversation->id], 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
            {!! Form::textarea('message',null,['class'=>'form-control', 'placeholder'=>'Mensaje a Karla', 'rows'=>'4','required']) !!}    
            <br>
            <button type="submit" class="btn btn-tm" data-dismiss="modal">Enviar</button>
            {!! Form::close() !!}
        </div>
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-cog"></span> Configuración</div>
                <div class="panel-body text-center">
                    {!! Form::open(['route'=>['destroy_converse', $conversation->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                    <br>
                    {!! Form::open(['route'=>['block_user', $conversation->id, $conversation->recipients()->last()->user->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Bloquear', ['class' => 'block-button btn btn-primary btn-sm']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $(".delete-button").click(function(){
        if (!confirm("Confirmas que quieres Eliminar?")){
          return false;
        }
    });

    $(".block-button").click(function(){
        if (!confirm("Confirmas que quiere Bloquear?")){
          return false;
        }
    });
</script>
@endsection