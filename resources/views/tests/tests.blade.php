@extends('layouts.app')

@section('title', 'Tests')
@section('meta-description', 'Tests')

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Tests</h1>
                <p>Tomar un test de tu interes aumenta la probabilidad de encontrar a alguien</p>
            </div>
            <br>

            <div class="row">
                @foreach($tests as $test)
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading text-center">{{$test->name}}</div>
                        <div class="panel-body text-center">
                            {{$test->description}}
                            <br>
                            @if(\Auth::user()->test($test->id) == 0)
                                <a href="{{route('take',[$test->id])}}" class="btn btn-primary">Tomar</a>
                            @else
                               <h4>Felicidades ya Tomaste este Test</h4>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
        <div class="col-md-2">
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection