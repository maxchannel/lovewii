@extends('layouts.app')

@section('title', 'Tomar Test')
@section('meta-description', 'Tomar Test')

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            <a href="{{ route('tests') }}" class="btn btn-info btn-ts">Todos los Test</a>
            <br><br>
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Tomar Test {{ $test->name }}</h1>
                <p>{{ $test->description }}</p>
                <p class="text-muted">
                    Escoge "Mucho" cuando, en su mayor parte, estés de acuerdo con la proposición, 
                    "Algo" si no la consideras acertada o requieres de más información, 
                    y "Nada" si definitivamente estás en desacuerdo. 
                </p>
            </div>

            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif
            @include('partials.errorMessages')

            @if(\Auth::user()->test($test->id) == 0)
            <table class="table table-hover">
                <tr>
                    <th></th>
                    <th></th>
                    <th>Nada</th>
                    <th>Algo</th>
                    <th>Mucho</th>
                </tr>
                {!! Form::open(['route'=>['take_update', $test->id], 'method'=>'POST', 'role'=>'form', 'class'=>'form-horizontal']) !!}
                    @foreach($test->questions as $i => $q)
                        <tr>
                            <td>{{$i}}- {{ $q->name }}</td>
                            <td>
                                <input type="radio" name="item[{{$i}}]" value="" checked>
                            </td>
                            <td>
                                {!! Form::radio('item['.$i.']', '1', (Input::old('item['.$i.']') == '1'), ['class'=>'radio']) !!}
                            </td>
                            <td>
                                {!! Form::radio('item['.$i.']', '2', (Input::old('item['.$i.']') == '2'), ['class'=>'radio']) !!}
                            </td>
                            <td>
                                {!! Form::radio('item['.$i.']', '3', (Input::old('item['.$i.']') == '3'), ['class'=>'radio']) !!}
                            </td>
                        </tr>
                    @endforeach    
                </table>    

                <button type="submit" class="btn btn-success">
                    Enviar
                </button>
                {!! Form::close() !!}
            @else
                <h3 class="text-center">Ya tomaste este Test.</h3>
            @endif
        </div>
        <div class="col-md-2">
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection