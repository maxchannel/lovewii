@extends('layouts.app')

@section('title', 'Hóroscopos de Hoy')
@section('meta-description', 'Hóroscopo del día por signo zodiacal junto con compatibilidad en el Amor por Signo y perfiles de esas personas compatibles en tu Ciudad.')

@section('script_head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
  return t;
}(document, "script", "twitter-wjs"));</script>
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @include('partials.subscription')
            <br><br>
            @if(Auth::check())
                @if(Auth::user()->isNormal())
                    @include('partials.ad')<br>
                @endif
            @else
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>Hóroscopos de Hoy</h1>
                <h2 class="horoscope-font">
                    Aquí además de encontrar tu Hóroscopo encontrarás perfiles de personas compatibles con tu signo, solo da
                    click en ver compatibles
                </h2>
            </div>
            <br>

            <div class="row">
                @foreach($days as $day)
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <a href="{{ route('horoscope_individual', [str_slug($day->horoscope->name), $day->fecha]) }}">
                            Hóroscopo de {{ $day->horoscope->name }} del día
                            <?php $dt = Carbon::createFromFormat('Y-m-d',$day->fecha); ?>
                            {{ $dt->toFormattedDateString() }}
                            </a>
                        </div>
                        <div class="panel-body text-center">
                            <p class="horoscope-font">
                                {{ $day->content }}
                            </p>
                            <a href="{{ route('horoscope_match', [str_slug($day->horoscope->name)]) }}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                Ver Compatibles
                            </a>
                            <br><br>
                            <div class="btn-social">
                                <div class="fb-like" data-href="{{ route('horoscope_individual', [str_slug($day->horoscope->name), $day->fecha]) }}" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                <a href="{{ route('horoscope_individual', [str_slug($day->horoscope->name), $day->fecha]) }}">
                                    <img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" />
                                </a>
                                <div class="btn-pin">
                                <a class="twitter-share-button"
                                  href="{{ route('horoscope_individual', [str_slug($day->horoscope->name), $day->fecha]) }}">
                                Tweet</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {!! str_replace('/?', '?', $days->render()) !!} 
            </div>

            <br><br>
            @if(Auth::check())
                @if(Auth::user()->isNormal())
                    @include('partials.ad')<br>
                @endif
            @else
                @include('partials.ad')<br>
            @endif
        </div>

        <div class="col-md-2">
            @if(Auth::guest())
            <div class="alert alert-info text-center" role="alert">
                Selecciona Tu Signo
            </div>
            <div class="list-group text">
                <a href="{{ route('horoscope_open', ['aries']) }}" class="list-group-item text-center">Aries</a>
                <a href="{{ route('horoscope_open', ['tauro']) }}" class="list-group-item text-center">Tauro</a>
                <a href="{{ route('horoscope_open', ['geminis']) }}" class="list-group-item text-center">Géminis</a>
                <a href="{{ route('horoscope_open', ['cancer']) }}" class="list-group-item text-center">Cáncer</a>
                <a href="{{ route('horoscope_open', ['leo']) }}" class="list-group-item text-center">Leo</a>
                <a href="{{ route('horoscope_open', ['virgo']) }}" class="list-group-item text-center">Virgo</a>
                <a href="{{ route('horoscope_open', ['libra']) }}" class="list-group-item text-center">Libra</a>
                <a href="{{ route('horoscope_open', ['scorpio']) }}" class="list-group-item text-center">Escorpión</a>
                <a href="{{ route('horoscope_open', ['sagitario']) }}" class="list-group-item text-center">Sagitario</a>
                <a href="{{ route('horoscope_open', ['capricornio']) }}" class="list-group-item text-center">Capricornio</a>
                <a href="{{ route('horoscope_open', ['acuario']) }}" class="list-group-item text-center">Acuario</a>
                <a href="{{ route('horoscope_open', ['piscis']) }}" class="list-group-item text-center">Piscis</a>
            </div>
            @endif
        </div>
    </section>

    <div class="balance"></div>
    @include('partials.footer')
</div>
@endsection