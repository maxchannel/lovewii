@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            <a href="{{ route('horoscope_match', [str_slug($horoscope->name)]) }}" class="btn btn-info btn-ts">Compatibles con {{ \Auth::user()->horoscope()->name }}</a>
            <br><br>
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>
        <div class="col-md-8">
            <div class="row text-center">
                <h1>@lang('general.horoscopo')</h1>
                <p> para {{ \Auth::user()->horoscope()->name }} (los Lunes y Miércoles)</p>
            </div>

            <div class="row">
                @foreach($days as $day)
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <script>
                            moment.locale("es");
                            document.writeln(moment.utc("{{ $day->fecha }}").format("dddd, MMMM D"));
                            </script>
                        </div>
                        <div class="panel-body text-center">
                            <p class="horoscope-font">
                                {{ $day->content }}
                            </p>
                            <a href="{{ route('horoscope_match', [str_slug($horoscope->name)]) }}" class="btn btn-warning">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                Ver Compatibles
                            </a>
                            <br><br>
                            <div class="btn-social">
                                <div class="fb-like" data-href="{{ route('horoscope_open', [str_slug($horoscope->name)]) }}" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                <a href="{{ route('horoscope_open', [str_slug($horoscope->name)]) }}">
                                    <img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" />
                                </a>
                                <div class="btn-pin">
                                <a class="twitter-share-button"
                                  href="{{ route('horoscope_open', [str_slug($horoscope->name)]) }}">
                                Tweet</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {!! str_replace('/?', '?', $days->render()) !!} 
            </div>
            <br>
            @if(Auth::user()->isNormal())
                @include('partials.ad')<br>
            @endif
        </div>

        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-cog"></span> Configuración</div>
                <div class="panel-body text-center">
                    <a href="{{ route('settings_horoscope') }}">Configuración de Horoscope</a><br>
                </div>
            </div>
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection