@extends('layouts.app')

@section('title', $horoscope->name.' personas Compatibles')
@section('meta-description', 'Personas en mi Ciudad Compatibles con '.$horoscope->name)

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <section class="row">
        <div class="col-md-2">
            @if(Auth::guest())
                <a href="{{ route('horoscope_open', [str_slug($horoscope->name)]) }}" class="btn btn-info btn-ts">Hóroscopo de {{ $horoscope->name }}</a>
            @else
                <a href="{{ route('horoscope') }}" class="btn btn-info btn-ts">Hóroscopo de {{ $horoscope->name }}</a>
            @endif
            <br><br>
            @if(Auth::check())
                @if(Auth::user()->isNormal())
                    @include('partials.ad')<br>
                @endif
            @else
                @include('partials.ad')<br>
            @endif
        </div>

        <div class="col-md-8">
            <div class="row text-center">
                <h1>Compatibles</h1>
                <p> para {{ $horoscope->name }}</p>
            </div>
            <br>
                @if(count($results) > 0)
                    @if(\Request::input('page') == 1 or \Request::input('page')=="")
                        <table class="table table-hover">
                            @foreach($results as $i => $h)
                            <tr>
                                <td> 
                                    <a href="{{ route('profile', [$h->user->username]) }}"><img src="{{ $avatar[$i] }}" class="aob"/></a>
                                    <a href="{{ route('profile', [$h->user->username]) }}">{{ $h->user->name }} - {{ $h->horoscope->name }}</a><br>
                                    @if($h->user->whichCompt($horoscope->id, $h->horoscope->id) == "x")
                                        <span class="font-red">@lang('general.'.$h->user->whichCompt($horoscope->id, $h->horoscope->id))</span><br>
                                    @elseif($h->user->whichCompt($horoscope->id, $h->horoscope->id) == "l")
                                        <span class="font-orange">@lang('general.'.$h->user->whichCompt($horoscope->id, $h->horoscope->id))</span><br>
                                    @endif
                                    
                                    @if($h->user->city != "")
                                    {{$h->user->city}}
                                    @endif
                                    @if($h->user->country_id != "")
                                    , {{$h->user->country->name}}
                                    @endif
                                    @if($h->user->info->bth != "")
                                    - {{getAge($h->user->info->bth)}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        {!! str_replace('/?', '?', $results->render()) !!} 
                    @elseif(\Request::input('page') >= 2 and Auth::check() and Auth::user()->isPremium())
                        <table class="table table-hover">
                            @foreach($results as $i => $h)
                            <tr>
                                <td> 
                                    <a href="{{ route('profile', [$h->user->username]) }}"><img src="{{ $avatar[$i] }}" class="aob"/></a>
                                    <a href="{{ route('profile', [$h->user->username]) }}">{{ $h->user->name }} - {{ $h->horoscope->name }}</a><br>
                                    @if($h->user->whichCompt($horoscope->id, $h->horoscope->id) == "x")
                                        <span class="font-red">@lang('general.'.$h->user->whichCompt($horoscope->id, $h->horoscope->id))</span><br>
                                    @elseif($h->user->whichCompt($horoscope->id, $h->horoscope->id) == "l")
                                        <span class="font-orange">@lang('general.'.$h->user->whichCompt($horoscope->id, $h->horoscope->id))</span><br>
                                    @endif

                                    @if($h->user->city != "")
                                    {{$h->user->city}}
                                    @endif
                                    @if($h->user->country_id != "")
                                    , {{$h->user->country->name}}
                                    @endif
                                    @if($h->user->info->bth != "")
                                    - {{getAge($h->user->info->bth)}}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        {!! str_replace('/?', '?', $results->render()) !!} 
                    @else
                        @include('partials.premium_discover')
                    @endif

                @else
                    <h3>Sin resultados</h3> | <a href="{{route('search')}}">Hacer un busqueda</a>
                @endif

        </div>

        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><span class="glyphicon glyphicon-cog"></span> Configuración</div>
                <div class="panel-body text-center">
                    <a href="{{ route('settings_horoscope') }}">Configuración de Horoscope</a><br>
                </div>
            </div>
        </div>
    </section>

    <div class="balance"></div>
</div>
@endsection