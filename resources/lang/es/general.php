<?php
return [
    'horoscopo' => 'Horóscopo',
    'messages' => 'Mensajes',
    'coach' => 'Coach',
    'nots' => 'Notificaciones',
    'pricing' => 'Precios',
    'plus' => 'Más',
    'signin' => 'Login',
    'signup' => 'Registro',
    'settings' => 'Configuración',
    'support' => 'Ayuda',
    'logout' => 'Salir',
    'profile' => 'Perfil',
    'btn_update' => 'Actualizar',
    'x' => 'Favorable: podría ser una excelente pareja',
    'l' => 'No tan Favorable: podría funcionar, pero con un poco más de trabajo',
    'o' => 'Crítico: hacer funcionar esta relación es muy difícil'


];
