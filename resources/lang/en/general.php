<?php
return [
    'horoscopo' => 'Horoscope',
    'messages' => 'Messages',
    'coach' => 'Coach',
    'nots' => 'Notifications',
    'pricing' => 'Pricing',
    'plus' => 'More',
    'signin' => 'SignIn',
    'signup' => 'SignUp',
    'settings' => 'Settings',
    'support' => 'Help',
    'logout' => 'Logout',
    'btn_update' => 'Update'

];
