<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    public function up()
    {
        Schema::create('campaigns', function ($table) {
            $table->increments('id');
            $table->string('source');
            $table->string('ad');
            $table->string('mode');
            $table->string('ip');

            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('campaigns');
    }
}
