<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHabitsTable extends Migration
{
    public function up()
    {
        Schema::create('user_habits', function($table) 
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('smo')->unsigned()->nullable();
            $table->integer('drin')->unsigned()->nullable();
            $table->integer('gy')->unsigned()->nullable();
            $table->integer('wo')->unsigned()->nullable();
            $table->integer('drug')->unsigned()->nullable();
            $table->integer('re')->unsigned()->nullable();//Leer
            $table->integer('mo')->unsigned()->nullable();//Movies
            $table->integer('con')->unsigned()->nullable();//Concerts

            $table->integer('pts')->unsigned()->nullable();//pets
            $table->integer('knd')->unsigned()->nullable();//kind
            $table->integer('her')->unsigned()->nullable();//listen
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('smo')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('drin')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('gy')->references('id')->on('frequences')->onDelete('set null');
    
            $table->foreign('wo')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('drug')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('re')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('mo')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('con')->references('id')->on('frequences')->onDelete('set null');

            $table->foreign('pts')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('knd')->references('id')->on('frequences')->onDelete('set null');
            $table->foreign('her')->references('id')->on('frequences')->onDelete('set null');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_habits');
    }
}
