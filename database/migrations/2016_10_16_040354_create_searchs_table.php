<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchsTable extends Migration
{
    public function up()
    {
        Schema::create('searchs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('q');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('searchs');
    }
}
