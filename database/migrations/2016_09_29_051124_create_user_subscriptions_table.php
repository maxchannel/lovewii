<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create('user_subscriptions', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->date('trial_ends_at')->nullable();
            $table->date('ends_at')->nullable();
            $table->boolean('renovation')->default(1);
            $table->string('plan')->nullable();
            $table->string('conekta_subscription')->nullable();
            $table->string('conekta_customer')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('user_subscriptions');
    }
}
