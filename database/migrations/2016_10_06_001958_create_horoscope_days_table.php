<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoroscopeDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horoscope_days', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->integer('horoscope_id')->unsigned();
            $table->date('fecha')->default('2016-09-01');

            $table->foreign('horoscope_id')->references('id')->on('horoscopes')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('horoscope_days');
    }
}
