<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('test_questions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('test_id')->unsigned();

            $table->foreign('test_id')->references('id')->on('tests');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('test_questions');
    }
}
