<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorAllsTable extends Migration
{
    public function up()
    {
        Schema::create('visitor_alls', function($table) 
        {
            $table->increments('id');
            $table->string('ip')->nullable();
            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('visitor_alls');
    }
}
