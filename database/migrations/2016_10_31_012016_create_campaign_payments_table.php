<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignPaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('campaign_payments', function ($table) {
            $table->increments('id');
            $table->string('plan');
            $table->integer('campaign_id')->unsigned();
            $table->string('ip');

            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('campaign_payments');
    }
}
