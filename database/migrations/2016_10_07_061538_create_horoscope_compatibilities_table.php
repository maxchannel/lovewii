<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoroscopeCompatibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horoscope_compatibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('horoscope_id')->unsigned();
            $table->integer('with_id')->unsigned();
            $table->integer('type_id')->unsigned();

            $table->foreign('horoscope_id')->references('id')->on('horoscopes')->onDelete('cascade');
            $table->foreign('with_id')->references('id')->on('horoscopes')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('horoscope_compatibilities_types')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('horoscope_compatibilities');
    }
}
