<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRanksTable extends Migration
{
    public function up()
    {
        Schema::create('user_ranks', function($table) 
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->integer('rank');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_ranks');
    }
}
