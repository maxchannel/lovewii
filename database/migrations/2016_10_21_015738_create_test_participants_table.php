<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestParticipantsTable extends Migration
{
    public function up()
    {
        Schema::create('test_participants', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('test_question_id')->unsigned();
            $table->integer('test_answer_id')->unsigned();


            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('test_question_id')->references('id')->on('test_questions');
            $table->foreign('test_answer_id')->references('id')->on('test_answers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('test_participants');
    }
}
