<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    public function up()
    {
        Schema::create('user_infos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('search_id')->unsigned()->nullable();
            $table->integer('para_id')->unsigned()->nullable();
            $table->integer('home')->nullable();//Hogareño
            $table->integer('kds')->nullable();//Kids
            $table->integer('hvkds')->nullable();//Have Kids
            $table->date('bth')->nullable();//Birthday
            $table->integer('tato')->default(0);//Tatoos
            $table->string('height')->nullable();
            $table->integer('timido')->nullable();
            $table->integer('sch')->unsigned()->nullable();//School
            $table->string('cp')->nullable();
            $table->integer('muse')->unsigned()->nullable();//Musica numerro 1 en tu ipod
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('search_id')->references('id')->on('genders')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('para_id')->references('id')->on('intentions')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('muse')->references('id')->on('music')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('sch')->references('id')->on('scholarships')->onDelete('set null');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_infos');
    }
}
