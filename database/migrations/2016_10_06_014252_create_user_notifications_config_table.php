<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNotificationsConfigTable extends Migration
{
    public function up()
    {
        Schema::create('user_notifications_config', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->boolean('not_follower')->default(1);
            $table->boolean('not_following')->default(1);
            $table->boolean('email_message')->default(1);
            $table->boolean('email_foll')->default(1);
            $table->boolean('email_horos')->default(1);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('user_notifications_config');
    }
}
