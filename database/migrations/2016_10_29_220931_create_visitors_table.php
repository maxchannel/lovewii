<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    public function up()
    {
        Schema::create('visitors', function($table) 
        {
            $table->increments('id');
            $table->string('ip')->nullable();
            $table->timestamps();

            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('visitors');
    }
}
