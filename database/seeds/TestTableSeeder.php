<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Test;
use App\TestAnswer;
use App\TestParticipant;
use App\TestQuestion;

class TestTableSeeder extends Seeder
{
    public function run()
    {
        //Respuestas
        $a = TestAnswer::create([
            'name'=> 'Algo'
        ]);
        $a = TestAnswer::create([
            'name'=> 'Mucho'
        ]);
        $a = TestAnswer::create([
            'name'=> 'Nada'
        ]);
        //Respuestas

        /*
        $testF = Test::create([
            'name'=> 'Fotografía',
            'description'=> 'Test para conocer tu perfil fotográfio y basar sugerencias en ellas.',
        ]);

        $testR = Test::create([
            'name'=> 'Lectura',
            'description'=> 'Test para conocer tu perfil lector y basar sugerencias en ellas.',
        ]);

        $testP = Test::create([
            'name'=> 'Personalidad',
            'description'=> 'Test para conocer tu perfil lector y basar sugerencias en ellas.',
        ]);
        */






        $test = Test::create([
            'name'=> 'Música',
            'description'=> 'Test para conocer tus gustos musicales y basar sugerencias en ellas.',
        ]);

        TestQuestion::create([
            'name'=> 'Soy una Persona que no puede pasar un día sin escuchar mi música preferida',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Me considero una persona que tiene un soundtrack especifico en cada etapa de mi vida',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Voy a conciertos al menos 5 veces al año',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Considero que tener una biblioteca musical física o digital es muy importante',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'El genero musical preferido de una persona es muy importante para mi',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Tengo como hobbie el aprender o tocar ya un instrumento músical',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Llevo siguiendo al menos a 3 grupos musicales al menos 10 años de mi vida',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Estoy al pendiente de corrientes nuevas dentro de mi género favorito',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Estoy al pendiente de corrientes nuevas dentro de mi género favorito',
            'test_id'=>$test->id
        ]);

        TestQuestion::create([
            'name'=> 'Al escucharl a muchos de mis artistas favoritos evocan buenos recuerdos del pasado',
            'test_id'=>$test->id
        ]);





        

        //Test2
        $test2 = Test::create([
            'name'=> 'Política',
            'description'=> 'Test para conocer tu Ideología y basar sugerencias en ellas.',
        ]);

        TestQuestion::create([
            'name'=> 'Estas interesado en el desempeño de tus Representantes despues de ser elegidos',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Participo en la elecciones de mi país en todas las contiendas',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Tengo afiliación a algún partido político',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Leo al menos 4 veces al mes sobre el desempeño de mis Representantes',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Analizo con criterío el voto por el presidente de mi país',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Distingo entre las Ideologias dentro de la política',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Me identifico más con una Ideología de Izquierda',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Me identifico más con una Ideología de Derecha',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Me identifico más con una Ideología de Centro',
            'test_id'=>$test2->id
        ]);

        TestQuestion::create([
            'name'=> 'Esta de acuerdo en que los medios de comunicación sean regulados',
            'test_id'=>$test2->id
        ]);





    }
    
}
