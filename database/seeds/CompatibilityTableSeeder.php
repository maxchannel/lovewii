<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Horoscope;
use App\HoroscopeDay;
use App\HoroscopeCompatibility;
use App\HoroscopeCompatibilityType;

class CompatibilityTableSeeder extends Seeder
{
    public function run()
    {
        //Types
        $love = HoroscopeCompatibilityType::create([
            'type'=> 'x'//Love
        ]);
        $less = HoroscopeCompatibilityType::create([
            'type'=> 'l'//Less favorable
        ]);
        $critical = HoroscopeCompatibilityType::create([
            'type'=> 'o'//Critical
        ]);


        //Aries
        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 2,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 4,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 6,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 8,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 10,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 1,
            'with_id'=> 12,
            'type_id'=> $less->id
        ]);



        //Tauro
        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 1,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 3,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 6,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 7,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 8,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 9,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 11,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 2,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);



        //Géminis
        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 2,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 4,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 6,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 8,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 10,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 3,
            'with_id'=> 12,
            'type_id'=> $critical->id
        ]);




        //Cancer
        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 1,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 3,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 5,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 6,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 8,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 9,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 11,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 4,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);



        //Leo
        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 2,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 4,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 6,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 8,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 10,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 5,
            'with_id'=> 12,
            'type_id'=> $critical->id
        ]);





        //Virgo
        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 1,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 3,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 5,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 6,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 7,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 8,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 9,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 11,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 6,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);




        //Libra
        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 2,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 4,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 6,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 8,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 10,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 7,
            'with_id'=> 12,
            'type_id'=> $less->id
        ]);





        //Escorpio
        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 1,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 3,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 5,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 6,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 7,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 8,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 9,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 11,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 8,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);







        //Sagiratio
        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 2,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 4,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 6,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 8,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 10,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 9,
            'with_id'=> 12,
            'type_id'=> $critical->id
        ]);






        //Capricornio
        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 1,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 3,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 5,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 6,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 7,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 8,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 9,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 11,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 10,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);




        //Acuario
        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 1,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 2,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 3,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 4,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 5,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 6,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 7,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 8,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 9,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 10,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 11,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 11,
            'with_id'=> 12,
            'type_id'=> $less->id
        ]);



        //Piscis
        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 1,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 2,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 3,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 4,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 5,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 6,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 7,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 8,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 9,
            'type_id'=> $critical->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 10,
            'type_id'=> $love->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 11,
            'type_id'=> $less->id
        ]);

        HoroscopeCompatibility::create([
            'horoscope_id'=> 12,
            'with_id'=> 12,
            'type_id'=> $love->id
        ]);

    }

    
}
