<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\UserHoroscope;
use App\UserInfo;
use App\UserView;
use App\UserAbout;
use App\UserHabit;
use App\UserSubscription;

class WelcomeTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        //Male
        $user = User::create([
            'name'=> $faker->name,
            'email'=> $faker->email,
            'city'=> $faker->city,
            'username'=> $faker->username,
            'avatar'=> 'noss.png',
            'gender_id'=>1,
            'country_id'=>140,
            'password'=>\Hash::make('secret'),
        ]);

        UserInfo::create([
            'user_id'=> $user->id,
            'search_id'=>2,
            'para_id'=>1,
            'bth'=>$faker->randomElement($array = array('1998-01-01','1940-01-01','1984-01-01','1998-01-01')),
        
        ]);

        UserHabit::create([
            'user_id'=> $user->id,
            
        ]);

        UserSubscription::create([
            'user_id'=> $user->id,
            
        ]);

        UserAbout::create([
            'user_id'=> $user->id,
            'description'=>"Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass."
        ]);

        UserHoroscope::create([
            'user_id'=> $user->id,
            'horoscope_id'=>$faker->randomElement($array = array (1,4)),
        ]);

        //Female
        $u2 = User::create([
            'name'=> $faker->name,
            'email'=> $faker->email,
            'city'=> $faker->city,
            'username'=> $faker->username,
            'avatar'=> 'you.png',
            'gender_id'=>2,
            'country_id'=>140,
            'password'=>\Hash::make('secret'),
        ]);

        UserInfo::create([
            'user_id'=> $u2->id,
            'search_id'=>2,
            'para_id'=>1,
            'bth'=>$faker->randomElement($array = array('1998-01-01','1940-01-01','1984-01-01','1998-01-01')),
        
        ]);

        UserHabit::create([
            'user_id'=> $u2->id,
            
        ]);

        UserSubscription::create([
            'user_id'=> $u2->id,
            
        ]);

        UserAbout::create([
            'user_id'=> $u2->id,
            'description'=>"Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass."
        ]);

        UserHoroscope::create([
            'user_id'=> $u2->id,
            'horoscope_id'=>$faker->randomElement($array = array (1,4)),
        ]);


    }

    
}
