<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountryTableSeeder::class);
        $this->call(GeneralTableSeeder::class);
        $this->call(WelcomeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CompatibilityTableSeeder::class);
        $this->call(TestTableSeeder::class);
        $this->call(HoroscopeSeeder::class);
    }
}
