<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Horoscope;
use App\HoroscopeDay;
use App\HoroscopeCompatibility;
use App\Contact;
use App\Scholarship;

class GeneralTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        Scholarship::create([
            'name'=> 'Secundaria',
        ]);

        Scholarship::create([
            'name'=> 'Preparatoria',
        ]);

        Scholarship::create([
            'name'=> 'Universidad',
        ]);

        Scholarship::create([
            'name'=> 'Maestría',
        ]);

        Scholarship::create([
            'name'=> 'Doctorado',
        ]);

        Scholarship::create([
            'name'=> 'Other',
        ]);

        Contact::create([
            'content'=> $faker->text(200),
            'email'=> $faker->email
        ]);

        Contact::create([
            'content'=> $faker->text(200),
            'email'=> $faker->email
        ]);

        Contact::create([
            'content'=> $faker->text(200),
            'email'=> $faker->email
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Aries',
            'elem'=> 'Fire',
            'planet'=>'Mars',
            'sym'=>'Ram',
            'stone'=>'Heliotrope'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Tauro',
            'elem'=> 'Earth',
            'planet'=>'Venus',
            'sym'=>'Bull',
            'stone'=>'Diamond'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Géminis',
            'elem'=> 'Air',
            'planet'=>'Mercury',
            'sym'=>'Twins',
            'stone'=>'x'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Cáncer',
            'elem'=> 'Water',
            'planet'=>'Moon',
            'sym'=>'Crab',
            'stone'=>'Moonstone, Pearl, Emerald, Ruby'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Leo',
            'elem'=> 'Fire',
            'planet'=>'Sun',
            'sym'=>'Lion',
            'stone'=>'x'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Virgo',
            'elem'=> 'Earth',
            'planet'=>'Mercury',
            'sym'=>'Maiden',
            'stone'=>'x'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Libra',
            'elem'=> 'Air',
            'planet'=>'Venus',
            'sym'=>'Scales',
            'stone'=>'x'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Scorpio',
            'elem'=> 'Water',
            'planet'=>'Mars',
            'sym'=>'Scorpion',
            'stone'=>'x'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Sagitario',
            'elem'=> 'Fire',
            'planet'=>'Jupiter',
            'sym'=>'Archer',
            'stone'=>'Topaz'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Capricornio',
            'elem'=> 'Earth',
            'planet'=>'Saturn',
            'sym'=>'Sea-Goat',
            'stone'=>'Garnet'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Acuario',
            'elem'=> 'Air',
            'planet'=>'Uranus',
            'sym'=>'Water-Beare',
            'stone'=>'Amethyst, Garnet, Hematite, Amber'
        ]);

        Horoscope::create([
            'de'=> '21/01',
            'hasta'=> '19/02',
            'name'=> 'Piscis',
            'elem'=> 'Water',
            'planet'=>'Neptune',
            'sym'=>'Fish',
            'stone'=>'x'
        ]);
        

    }

    
}
