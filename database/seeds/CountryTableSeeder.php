<?php

use Illuminate\Database\Seeder;
use App\Country;
use App\Gender;
use App\Frequence;
use App\Intention;
use App\Music;

class CountryTableSeeder extends Seeder
{
    public function run()
    {
        Music::create([
            'name'=> 'R&B'
        ]);

        Music::create([
            'name'=> 'Cumbia'
        ]);

        Music::create([
            'name'=> 'Rock'
        ]);

        Music::create([
            'name'=> 'Salsa'
        ]);

        Music::create([
            'name'=> 'Pop'
        ]);

        Music::create([
            'name'=> 'Electro'
        ]);

        Music::create([
            'name'=> 'Hip Hop'
        ]);

        Music::create([
            'name'=> 'Country'
        ]);

        Music::create([
            'name'=> 'Regional Mexicana'
        ]);

        Music::create([
            'name'=> 'Bachata'
        ]);

        Music::create([
            'name'=> 'Reggae'
        ]);

        Music::create([
            'name'=> 'Reggaetón'
        ]);

        Music::create([
            'name'=> 'Classis'
        ]);

        Music::create([
            'name'=> 'K-Pop'
        ]);

        Music::create([
            'name'=> 'K-Rock'
        ]);

        Music::create([
            'name'=> 'Tango'
        ]);

        Music::create([
            'name'=> 'Vallenato'
        ]);



        Frequence::create([
            'name'=> 'Si'
        ]);

        Frequence::create([
            'name'=> 'No'
        ]);

        Frequence::create([
            'name'=> 'Socialmente'
        ]);


        Intention::create([
            'name'=> 'Algo Casual'
        ]);

        Intention::create([
            'name'=> 'Relación Estable'
        ]);

        Intention::create([
            'name'=> 'Amistad'
        ]);

        Intention::create([
            'name'=> 'Matrimonio'
        ]);


        Gender::create([
            'name'=> 'male'
        ]);

        Gender::create([
            'name'=> 'female'
        ]);

        DB::table('countries')->delete();
        $d = '2016-10-09 17:04:28';

        $countries = array(
            array('code' => 'US', 'name' => 'United States', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CA', 'name' => 'Canada', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AF', 'name' => 'Afghanistan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AL', 'name' => 'Albania', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DZ', 'name' => 'Algeria', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AS', 'name' => 'American Samoa', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AD', 'name' => 'Andorra', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AO', 'name' => 'Angola', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AI', 'name' => 'Anguilla', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AQ', 'name' => 'Antarctica', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AG', 'name' => 'Antigua and/or Barbuda', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AR', 'name' => 'Argentina', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AM', 'name' => 'Armenia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AW', 'name' => 'Aruba', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AU', 'name' => 'Australia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AT', 'name' => 'Austria', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AZ', 'name' => 'Azerbaijan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BS', 'name' => 'Bahamas', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BH', 'name' => 'Bahrain', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BD', 'name' => 'Bangladesh', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BB', 'name' => 'Barbados', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BY', 'name' => 'Belarus', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BE', 'name' => 'Belgium', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BZ', 'name' => 'Belize', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BJ', 'name' => 'Benin', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BM', 'name' => 'Bermuda', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BT', 'name' => 'Bhutan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BO', 'name' => 'Bolivia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BA', 'name' => 'Bosnia and Herzegovina', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BW', 'name' => 'Botswana', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BV', 'name' => 'Bouvet Island', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BR', 'name' => 'Brazil', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IO', 'name' => 'British lndian Ocean Territory', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BN', 'name' => 'Brunei Darussalam', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BG', 'name' => 'Bulgaria', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BF', 'name' => 'Burkina Faso', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'BI', 'name' => 'Burundi', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KH', 'name' => 'Cambodia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CM', 'name' => 'Cameroon', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CV', 'name' => 'Cape Verde', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KY', 'name' => 'Cayman Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CF', 'name' => 'Central African Republic', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TD', 'name' => 'Chad', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CL', 'name' => 'Chile', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CN', 'name' => 'China', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CX', 'name' => 'Christmas Island', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CC', 'name' => 'Cocos (Keeling) Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CO', 'name' => 'Colombia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KM', 'name' => 'Comoros', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CG', 'name' => 'Congo', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CK', 'name' => 'Cook Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CR', 'name' => 'Costa Rica', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HR', 'name' => 'Croatia (Hrvatska)', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CU', 'name' => 'Cuba', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CY', 'name' => 'Cyprus', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CZ', 'name' => 'Czech Republic', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CD', 'name' => 'Democratic Republic of Congo', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DK', 'name' => 'Denmark', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DJ', 'name' => 'Djibouti', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DM', 'name' => 'Dominica', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DO', 'name' => 'Dominican Republic', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TP', 'name' => 'East Timor', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'EC', 'name' => 'Ecudaor', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'EG', 'name' => 'Egypt', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SV', 'name' => 'El Salvador', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GQ', 'name' => 'Equatorial Guinea', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ER', 'name' => 'Eritrea', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'EE', 'name' => 'Estonia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ET', 'name' => 'Ethiopia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FK', 'name' => 'Falkland Islands (Malvinas)', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FO', 'name' => 'Faroe Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FJ', 'name' => 'Fiji', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FI', 'name' => 'Finland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FR', 'name' => 'France', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FX', 'name' => 'France, Metropolitan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GF', 'name' => 'French Guiana', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PF', 'name' => 'French Polynesia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TF', 'name' => 'French Southern Territories', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GA', 'name' => 'Gabon', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GM', 'name' => 'Gambia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GE', 'name' => 'Georgia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'DE', 'name' => 'Germany', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GH', 'name' => 'Ghana', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GI', 'name' => 'Gibraltar', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GR', 'name' => 'Greece', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GL', 'name' => 'Greenland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GD', 'name' => 'Grenada', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GP', 'name' => 'Guadeloupe', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GU', 'name' => 'Guam', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GT', 'name' => 'Guatemala', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GN', 'name' => 'Guinea', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GW', 'name' => 'Guinea-Bissau', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GY', 'name' => 'Guyana', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HT', 'name' => 'Haiti', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HM', 'name' => 'Heard and Mc Donald Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HN', 'name' => 'Honduras', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HK', 'name' => 'Hong Kong', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'HU', 'name' => 'Hungary', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IS', 'name' => 'Iceland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IN', 'name' => 'India', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ID', 'name' => 'Indonesia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IR', 'name' => 'Iran (Islamic Republic of)', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IQ', 'name' => 'Iraq', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IE', 'name' => 'Ireland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IL', 'name' => 'Israel', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'IT', 'name' => 'Italy', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CI', 'name' => 'Ivory Coast', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'JM', 'name' => 'Jamaica', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'JP', 'name' => 'Japan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'JO', 'name' => 'Jordan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KZ', 'name' => 'Kazakhstan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KE', 'name' => 'Kenya', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KI', 'name' => 'Kiribati', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KP', 'name' => 'Korea, Democratic People\'s Republic of', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KR', 'name' => 'Korea, Republic of', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KW', 'name' => 'Kuwait', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KG', 'name' => 'Kyrgyzstan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LA', 'name' => 'Lao People\'s Democratic Republic', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LV', 'name' => 'Latvia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LB', 'name' => 'Lebanon', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LS', 'name' => 'Lesotho', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LR', 'name' => 'Liberia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LY', 'name' => 'Libyan Arab Jamahiriya', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LI', 'name' => 'Liechtenstein', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LT', 'name' => 'Lithuania', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LU', 'name' => 'Luxembourg', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MO', 'name' => 'Macau', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MK', 'name' => 'Macedonia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MG', 'name' => 'Madagascar', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MW', 'name' => 'Malawi', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MY', 'name' => 'Malaysia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MV', 'name' => 'Maldives', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ML', 'name' => 'Mali', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MT', 'name' => 'Malta', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MH', 'name' => 'Marshall Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MQ', 'name' => 'Martinique', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MR', 'name' => 'Mauritania', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MU', 'name' => 'Mauritius', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TY', 'name' => 'Mayotte', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MX', 'name' => 'México', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'FM', 'name' => 'Micronesia, Federated States of', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MD', 'name' => 'Moldova, Republic of', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MC', 'name' => 'Monaco', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MN', 'name' => 'Mongolia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MS', 'name' => 'Montserrat', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MA', 'name' => 'Morocco', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MZ', 'name' => 'Mozambique', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MM', 'name' => 'Myanmar', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NA', 'name' => 'Namibia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NR', 'name' => 'Nauru', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NP', 'name' => 'Nepal', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NL', 'name' => 'Netherlands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AN', 'name' => 'Netherlands Antilles', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NC', 'name' => 'New Caledonia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NZ', 'name' => 'New Zealand', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NI', 'name' => 'Nicaragua', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NE', 'name' => 'Niger', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NG', 'name' => 'Nigeria', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NU', 'name' => 'Niue', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NF', 'name' => 'Norfork Island', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'MP', 'name' => 'Northern Mariana Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'NO', 'name' => 'Norway', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'OM', 'name' => 'Oman', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PK', 'name' => 'Pakistan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PW', 'name' => 'Palau', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PA', 'name' => 'Panama', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PG', 'name' => 'Papua New Guinea', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PY', 'name' => 'Paraguay', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PE', 'name' => 'Peru', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PH', 'name' => 'Philippines', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PN', 'name' => 'Pitcairn', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PL', 'name' => 'Poland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PT', 'name' => 'Portugal', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PR', 'name' => 'Puerto Rico', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'QA', 'name' => 'Qatar', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SS', 'name' => 'Republic of South Sudan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'RE', 'name' => 'Reunion', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'RO', 'name' => 'Romania', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'RU', 'name' => 'Russian Federation', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'RW', 'name' => 'Rwanda', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'KN', 'name' => 'Saint Kitts and Nevis', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LC', 'name' => 'Saint Lucia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VC', 'name' => 'Saint Vincent and the Grenadines', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'WS', 'name' => 'Samoa', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SM', 'name' => 'San Marino', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ST', 'name' => 'Sao Tome and Principe', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SA', 'name' => 'Saudi Arabia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SN', 'name' => 'Senegal', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'RS', 'name' => 'Serbia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SC', 'name' => 'Seychelles', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SL', 'name' => 'Sierra Leone', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SG', 'name' => 'Singapore', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SK', 'name' => 'Slovakia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SI', 'name' => 'Slovenia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SB', 'name' => 'Solomon Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SO', 'name' => 'Somalia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ZA', 'name' => 'South Africa', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GS', 'name' => 'South Georgia South Sandwich Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ES', 'name' => 'Spain', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'LK', 'name' => 'Sri Lanka', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SH', 'name' => 'St. Helena', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'PM', 'name' => 'St. Pierre and Miquelon', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SD', 'name' => 'Sudan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SR', 'name' => 'Suriname', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SJ', 'name' => 'Svalbarn and Jan Mayen Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SZ', 'name' => 'Swaziland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SE', 'name' => 'Sweden', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'CH', 'name' => 'Switzerland', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'SY', 'name' => 'Syrian Arab Republic', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TW', 'name' => 'Taiwan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TJ', 'name' => 'Tajikistan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TZ', 'name' => 'Tanzania, United Republic of', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TH', 'name' => 'Thailand', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TG', 'name' => 'Togo', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TK', 'name' => 'Tokelau', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TO', 'name' => 'Tonga', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TT', 'name' => 'Trinidad and Tobago', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TN', 'name' => 'Tunisia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TR', 'name' => 'Turkey', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TM', 'name' => 'Turkmenistan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TC', 'name' => 'Turks and Caicos Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'TV', 'name' => 'Tuvalu', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'UG', 'name' => 'Uganda', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'UA', 'name' => 'Ukraine', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'AE', 'name' => 'United Arab Emirates', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'GB', 'name' => 'United Kingdom', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'UM', 'name' => 'United States minor outlying islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'UY', 'name' => 'Uruguay', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'UZ', 'name' => 'Uzbekistan', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VU', 'name' => 'Vanuatu', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VA', 'name' => 'Vatican City State', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VE', 'name' => 'Venezuela', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VN', 'name' => 'Vietnam', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VG', 'name' => 'Virgin Islands (British)', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'VI', 'name' => 'Virgin Islands (U.S.)', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'WF', 'name' => 'Wallis and Futuna Islands', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'EH', 'name' => 'Western Sahara', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'YE', 'name' => 'Yemen', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'YU', 'name' => 'Yugoslavia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ZR', 'name' => 'Zaire', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ZM', 'name' => 'Zambia', 'created_at'=>$d, 'updated_at'=>$d),
            array('code' => 'ZW', 'name' => 'Zimbabwe', 'created_at'=>$d, 'updated_at'=>$d),
        );
        
        DB::table('countries')->insert($countries);
    }

    
}
