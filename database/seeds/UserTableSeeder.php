<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\UserNotification;
use App\UserNotificationConfig;
use App\UserPrivacyConfig;
use App\UserHoroscope;
use App\UserInfo;
use App\UserView;
use App\UserAbout;
use App\UserImage;
use App\UserLogin;
use App\UserHabit;
use App\UserRank;
use App\UserReport;
use App\UserSubscription;
use App\Conversation;
use App\ConversationSee;
use App\Participant;
use App\Message;
use App\Verification;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        
        foreach(range(1, 3) as $index)//Minimo 6 sino truena
        {
            $user = User::create([
                'name'=> $faker->name,
                'email'=> $faker->email,
                'city'=> $faker->city,
                'username'=> $faker->username,
                'avatar'=> $faker->randomElement($array = array('noss.png','me.png','you.png')),
                'gender_id'=>$faker->randomElement($array = array (1,2)),
                'country_id'=>$faker->randomElement($array = array (1,40)),
                'password'=>\Hash::make('secret'),
            ]);

            //$faker->randomElement($array = array (1,2,3,4,5,6,7,8,9,10))
            UserHoroscope::create([
                'user_id'=> $user->id,
                'horoscope_id'=>$faker->randomElement($array = array (1,2,3,4,5,6,7,8,9,10))
            ]);

            UserInfo::create([
                'user_id'=> $user->id,
                'search_id'=>2,
                'para_id'=>1,
                'bth'=>$faker->randomElement($array = array('1998-01-01','1940-01-01','1984-01-01','1998-01-01')),
            
            ]);

            UserHabit::create([
                'user_id'=> $user->id,
                
            ]);

            UserSubscription::create([
                'user_id'=> $user->id,
                
            ]);

            UserImage::create([
                'user_id'=> $user->id,
                'route'=>'1.png'
            ]);
            UserImage::create([
                'user_id'=> $user->id,
                'route'=>'2.png'
            ]);
            UserImage::create([
                'user_id'=> $user->id,
                'route'=>'3.png'
            ]);

            UserAbout::create([
                'user_id'=> $user->id,
                'description'=>"Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass."
            ]);

            UserNotification::create([
                'm'=> 'Bienvenido',
                'user_id'=> $user->id,
                'v'=>0
            ]);    

            UserView::create([
                'user_id'=> $user->id,
                'from_id'=> 1,
                'v'=>0
            ]);

            UserLogin::create([
                'user_id'=> $user->id,
                'visitor'=> $faker->ipv4
            ]);

            UserRank::create([
                'user_id'=> $user->id,
                'name'=> 'music',
                'rank'=> 78
            ]);

        }

        //Admin
        $admin = User::create([
            'name'=> 'Max',
            'email'=> 'admin@hotmail.com',
            'username'=> 'max',
            'avatar'=> $faker->randomElement($array = array('noss.png','me.png','you.png')),
            'gender_id'=>1,
            't'=>'admin',
            'city'=>'Medellín',
            'country_id'=>140,
            'password'=>\Hash::make('secret'),
        ]);

        UserHabit::create([
            'user_id'=> $admin->id,
            
        ]);

        UserReport::create([
            'user_id'=> $admin->id,
            'report'=>2,
            'ip'=> $faker->ipv4,
        ]);

        Verification::create([
            'user_id'=> $admin->id,
            'route'=> 'default.png'
        ]);

        //Payment
        $subs = UserSubscription::create([
            'user_id'=> $admin->id,
            'ends_at'=> '2016-11-30',
            
        ]);
        //Payment

        UserInfo::create([
            'user_id'=> $admin->id,
            'search_id'=>2,
            'para_id'=>1,
            'bth'=>'1998-01-01',
            'height'=>'180'
        
        ]);

        UserImage::create([
            'user_id'=> $admin->id,
            'route'=>'1.png'
        ]);
        UserImage::create([
            'user_id'=> $admin->id,
            'route'=>'2.png'
        ]);
        UserImage::create([
            'user_id'=> $admin->id,
            'route'=>'3.png'
        ]);

        UserLogin::create([
            'user_id'=> $admin->id,
            'visitor'=> $faker->ipv4
        ]);

        UserAbout::create([
            'user_id'=> $admin->id,
            'description'=>"Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass."
        ]);

        UserNotification::create([
            'm'=> 'Bienvenido',
            'user_id'=> $admin->id,
            'v'=>0
        ]);

        UserHoroscope::create([
            'user_id'=> $admin->id,
            'horoscope_id'=>$faker->randomElement($array = array (1,4)),
        ]);

        UserView::create([
            'user_id'=> $admin->id,
            'from_id'=> 1,
            'v'=>0
        ]);

        UserNotificationConfig::create([
            'not_follower'=>0,
            'user_id'=> $admin->id
        ]);

        UserPrivacyConfig::create([
            'user_id'=> $admin->id
        ]);

        $converse = Conversation::create([
        ]);
        ConversationSee::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$admin->id,
        ]);
        ConversationSee::create([
            'conversation_id'=>$converse->id,
            'user_id' => 3,
        ]);

        Participant::create([
            'conversation_id'=>$converse->id,
            'user_id'=>$admin->id,
            
        ]);

        Participant::create([
            'conversation_id'=>$converse->id,
            'user_id'=>3,
            
        ]);

        Message::create([
            'body'=>'Hello world!',
            'conversation_id'=>$converse->id,
            'user_id'=>$admin->id,
        ]);

        Message::create([
            'body'=>'where are U?',
            'conversation_id'=>$converse->id,
            'user_id'=>3,
        ]);

        $converse2 = Conversation::create([
        ]);
        ConversationSee::create([
            'conversation_id'=>$converse2->id,
            'user_id'=>$admin->id,
        ]);
        ConversationSee::create([
            'conversation_id'=>$converse2->id,
            'user_id' => 4,
        ]);

        Participant::create([
            'conversation_id'=>$converse2->id,
            'user_id'=>$admin->id,
            
        ]);

        Participant::create([
            'conversation_id'=>$converse2->id,
            'user_id'=>4,
            
        ]);

        Message::create([
            'body'=>'W&Y',
            'conversation_id'=>$converse2->id,
            'user_id'=>$admin->id,
        ]);

        Message::create([
            'body'=>'Hi, im from italy',
            'conversation_id'=>$converse2->id,
            'user_id'=>4,
        ]);


    }

    
}
