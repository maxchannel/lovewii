<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\HoroscopeDay;

class HoroscopeSeeder extends Seeder
{
    public function run()
    {
        HoroscopeDay::create([
            'horoscope_id'=> 1,
            'content'=> 'Tienes oportunidad de crecer mucho más de lo que lo haz hecho hasta el momento, solo que debes alejar
             a la gente toxica y encontrar el camino, una nueva mentalidad de cara a los retos que te presentarán en el futuro cercano
             es indispensable, los proyectos al corto plazo requeriran toda tu concentración por lo que debes de preparate ya.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'content'=> 'Al final del día te preguntas si vas en la dirección correcto, eso que no te deja conciliar el sueño necesita una solución definitiva y de raiz y tu lo sabes, deja de dar vuelta en circulos a esa solución y hazlo de una ves, disfrutaras más el camino si esto ya no te aqueja más.',
            'horoscope_id'=> 1,
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 2,
            'content'=> 'Conocerás a alguien especial en el corto plazo, debes de mostrate como eres tan cual, el hecho de tratar de agradar a las personas fuerza las cosas y no termina nada bien. Empezar una nueva etapa en tu vida amorosa te llenara de energía nuevamanete, lo que te llevará a mejorar en todos los aspectos de tu vida.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 2,
            'content'=> 'Tienes que acabar con los limites que tu mismo te pones, el estar disponible y dispuesto a nuevas proyectos y no tenerle miedo al cambio, los cambios profundos son los más significativos y los más duraderos, por lo mismo cuestan más, es más dificil hacerlo pero a la larga diras cuanto lo haz conseguido que valio la pena, tienes todo el potencial para atraer cosas positivas a tu vida.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 3,
            'content'=> 'Aparte de tu trabajo hay un proyecto que esta en espera desde hace mucho tiempo, haz creado muchas excusas desde hace mucho tiempo, utiliza el fin de año para ir preparando la parte de planeación, contactos y desarollo para que la energía de un año nuevo te impulse a crearlo definitivamente, deja de utilizar energía en personas y actividades que sabes que no te dejan nada bueno, y verás como de manera natural el proyecto crecerá por si mismo. Entre dejar tu trabajo y no, no lo pienses demasiado pero si evalua los pros y contras de hacerlo.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 3,
            'content'=> 'Cuando ya no cabe ni una foto en tus recuerdos tal vez es momento de comenzar un nueva etapa en lo espiritual y en el amor, este se te dará de manera organica y por si mismo te guiara hacia lo que debes hacer, no cometas los errores del pasado ni con las personas ni con los proyecto en puerta que tienes, el crecimiento constante es tu mejor metodo y lo que te llevará a lograr lo que en verdad quieres.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 4,
            'content'=> 'No trates de ser el mejor, trata de ser diferente a los demás, en tu trabajo y con las persona que te rodean lo común aburre tu debes tomar la batuta para poder generar cambio primero en ti y despues en las personas que te rodean, nunca ayudes a los demás sin antes tu llegar a un estado y una posición donde puedes ayudar, diferenciate con los demás por las acciones y no por argumentos.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 4,
            'content'=> 'El mayor guionista de tu vida eres tu mismo, si un papel dice el destino y este no te gusta rompelo definitivamente, son las acciones las que valen más que 10mil palabras, en tu trabaja la energía no circula como debería toma energía desde tus pasiones y regalala a los que más quieres, esta terminara si lo haces de forma correcta volvio a ti y lo sentirás.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 5,
            'content'=> 'Tu mayor debilidad ha radicado que haz renunciado a tus sueños viejos y los nuevos ni hablar, convierte eso en energía vital para que te impulse a llevar a cabo los proyectos que no haz siquiera iniciado, si fracasas no lo uses como demotivación sino como inspiración la mayor constante de los ganadores ha sido que no se rindierón en el fracaso sino que lo usarón como plataforma para alcanzar la meta deseda.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 5,
            'content'=> 'Usa un recordatorio diario para recordarte a ti mismo que eres una influencia positiva para el mundo y no solo para tu trabajo, el gastar energía en actividades negativas desgasta demás el espiritu además de que gastas mucha más energía del cuerpo sin que esto te lleve a buen termino.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 6,
            'content'=> 'Las oportunidades se presentan en donde menos lo esperás no te aisles, ya que el aislamiento es un sueño autodestructivo si tienes sintomas de hacerlo es mejor superarlo rapidamente, ten en cuenta que para tus proyectos todos esta aportanto todo el tiempo incluso aunque parescan personas desinteresadas en tus intereses. No le cuentes ese proyecto a todos pero si trata de comentarlo con los cercanos que crees que aportarán algo bueno a él.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 6,
            'content'=> 'La palabra clave para este mes es perseverancia tomala en cuenta y usa un recordatorio para saber que no debes claudicar ante la primer turbulencia. No avances por presión de los demás pero si usa a quien te diga que no lo lograrás como fuente de energía para saber que crear y cuando hacerlo. Despues vuelve con aquellos que te dijeron que no lo lograrias y con respeto invitalos a tu mesa no para reprocharles sino para regalarles parte de tu nueva energía.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 7,
            'content'=> 'Haz caido en un bache que desde hace tiempo no te deja liberar todo el potencial que llevas dentro, tienes una voz interior que nunca se calla y que te hace ir por tus sueños. Usala como potencializador de nuevos proyectos y de los ya existententes el fracaso parece tabú pero la realidad es que nadie que tenga exito puede presumir de no tener al menos un pequeño fracaso en su vida.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 7,
            'content'=> 'Si dejas de soñar dejas de vivir más no te lleves todo el tiempo en solo soñar sino en realizar eso que haz soñado, no te limites por creencias heredadas y eliminalas de tu volcabulario. Haz lo que puedas desde donde este ya sea tu trabajo o de manera independiente pero nunca pongas la excusa de que es por tu entorno que no haz logrado las cosas que pensaste que a estas alturas ya habrías logrado.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 8,
            'content'=> 'El amor ha sido un poco duro contigo pero esto no debe de cuartar tu esperanza por seguir en la busqueda de ese alguien especial que sabes que te complementará. Relajate un poco ya que el estar siempre atento a con quien puedes salir crea un tensión imperseptible en el aire que la gente de alguna manera nota y terminan por darse cuenta de tu estado de animo, deja que las cosas fluyan a su propio ritmo y verás como de manera organica la persona que esperás llegá.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 8,
            'content'=> 'Los pensamientos negativos no solo te estan frenando sino que te jalan para atras, aprende a desaprender, suena a algo sin sentido pero te darás cuenta que aveces no hace falta aprender algunas nuevas cosas para crecer, sino que desaprender cosas toxicas te llevará más lejos que buscar la respuesta en el exterior.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 9,
            'content'=> 'Que historia puedes contar hoy en día que te haga grande?, si aún no la tienes enfocate en crear una, una nueva experiencia, aventura, viaje, etc... más que esto no te obsesiene y no te deje dormir. Enfocate ya en hacerte feliz a ti, haz pasado demasiado tiempo tratando de complacer a los demás para hacerlos felices.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 9,
            'content'=> 'Las personas que más quieren necesitan tu ayuda, pero antes de poder ayudarlos deberás de crearte una plataforma propia en la que estes en posición de ayudar, si aún no la tienes será mejor que aprendas a decir no antes situaciones en la que no estabas involucrado desde el principio.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 10,
            'content'=> 'Si te sientes obligado en el amor a veces es mejor dejarlo ir, el no tener muchas cosas en común con esa persona puede llevarte a la infelicidad a largo plazo, tu sabes al final del día te cuestionas si es persona es la ideal. En lo profundo de tu ser sabes que esa persona no es para ti si te siente obligado.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 10,
            'content'=> 'Solo cerrando las puertas detras de uno se abren las de adelante, si quieres avanzar no sigas encerrado por estar esclavo de tu pasado. Esas situaciones dolorosas ya necesitan ser cerradas. El postergar eso te desgasta permanentenentemente ya que el cerebro sigue desgastandose a causa de ello. Liberate definitivamente dejando ir todo el dolor del pasado y perdonando a las personas que te hicieron daño.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 11,
            'content'=> 'Un viejo dicho dice que solo esta derrotado quien se da por vencido, esto es tan cierto tanto en el trabajo, proyectos así como en el amor, ese proyecto no esperá que los resultados lleguen pronto pero no es motivo de tirar la toalla, en el amor no te rindas por la persona que en realidad sabes que es "la persona" con la que quieres estar, en la forma de pedir esta el dar muchas veces entonces la vida se fija muchas veces sobre todo en la insistencia con la que lo pides.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 11,
            'content'=> 'Llegaste al punto en que los dos se conoces y conocen lo que le gusta a cada quien, no te confies por que al llegar a esta situación creas una zona de confort como en lo economico tanto como en el amor, la zona de confort lleva a entrar en un estado alfa en el que no avanzas ni retrocedes pero no verás el cambio pasando lo que te puede dejar fuera del negocio.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 12,
            'content'=> 'En el corto plazo conociste a una persona de la que te llevaste una primera impresión no muy buena, no te cierres por completo por esa mala experiencia, ya que esa persona al largo plazo puede ser una persona con la que tengas muy buena química.',
            'fecha'=> '2016-11-07'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 12,
            'content'=> 'Se puede empezar pequeño pero pensando en grande y al largo plazo, ese proyecto que suena en tu cabeza desde hace tiempo en el que crees que necesitas toda una organización gigante para empezar no lo requiere tanto, llevalo poco a poco pero pensando en tocar a muchas personas. Y sobre todo piensa en ayudar a las personas y regalar lo que haces con amor, despues todo vendra de manera organica.',
            'fecha'=> '2016-11-07'
        ]);

        //Miercoles
        HoroscopeDay::create([
            'horoscope_id'=> 1,
            'content'=> 'Llevas un tiempo esperando la oportunidad correcta y el tiempo correcto, ese proyecto que haz querido empezar
            desde hace mucho tiempo debes de comenzarlo ya, por que nada tarda tanto como lo que no se empieza. En el amor te pasa lo mismo,
            Esperas tanto el decir las cosas a esa persona que terminas por mejor no decirla, atrevete a dar ese primer paso y veras como
            lo demás será disfrutar el camino a tu proyecto.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 2,
            'content'=> 'Se te aproxima una epoca de vacas flacas, no tienes que asustarte sino prevenirte para eso, deja de 
            despilfarrar el dinero en cosas que no necesitas, en el futuro este te será de mucha importancia, para salir de
            esta epoca mantente auténtico ante todo y tu sola personalidad te sacara adelante.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 3,
            'content'=> 'En el amor siempre se te complica, es por que hablas de más, eso te trae y trae problemas en todos los
            aspectos de tu vida desde hace mucho tiempo, empieza por ponerte en el lugar de las otras personas y piensa como te
            sentirías si te dijerán eso a ti. En el dinero manten la estabilidad de tu trabajo, derepente no es tu momento de 
            arriesgar de más, planea bien tus gastos y verás como te rinde más, y hasta un lujo de puedes dar.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 4,
            'content'=> 'Vienen días muy complicados para ti, no te enfoques en eso, tu fortaleza mental será de gran ayuda por si 
            sola, pero no es a lo único que te debes apegar. El llevar un control de tus finanzas te ayudará a no gastar de más,
            analizas poco las cosas, tu clave de aquí en adelante es analizar pero sin paralizar, ve por la oportunidad pero solo cuando
            creas que es real.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 5,
            'content'=> 'Este fin de año te servirá para reflexionar sobre si tu orgullo te ha estado llevando a buen puerto
            en el amor, dinero y hasta en la salud, el no tener la capacidad de perdonar te quita de mucha oportunidades, o perdonas
            tarde y para cuando llegas la oportunidad ya no esta, recuerda que estas tienen fecha de caducidad. Reflexiona sobre todo
            esto será importante para que el siguiente año ahora si emprendas, ahora si vallas por aquella persona con la que quieres estar.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 6,
            'content'=> 'Tu escenario en cuestiones de amor no es el mejor, puede que la persona que llegue en el futuro inmediato,
            no este del toda convencida sobre tu potencial y llegue hasta lastimarte, quedate con esa persona solo si la sientes
            del todo y ante todo auténtica, por que de lo contrario perderás libertad y sentirás que te equivocaste al escogerla a ella.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 7,
            'content'=> 'Conten un poco tu alegría, hay personas a tu alrededor que lejor de verlo con buenos ojos tienen envidía,
            no lo demuestran ni lo dicen, pero mejor enfocala en tu familia lugar donde volverá a ti en forma de energía. En el amor
            te haz equivocado últimamente, no ha sido tan grave pero sabes que gran parte de la culpa la tienes tu. Aprovecha el final 
            de año para pedir perdon a aquellos que más que con acciones con palabras llegaste a lastimar y te quedará una satifacción más duradera.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 8,
            'content'=> 'Sabes que lastimas a las personas con tus palabras por una satifacción efímera que solo te acarrea problemas,
            es mejor reconocer los logros de los otros y decir gracias, de vez en cuando realizar tu buena acción de la semana, esto te 
            dejará una satifacción más duradera y no un satifacción efímera por tan solo unas horas.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 9,
            'content'=> 'Un proyecto que ya dabas por hecho puede sufrir de turbulencia, no te desanimes al contrario apalancate 
            en el para poder llegar a mejor puerto. No invites a personas a tu vida si no sientes que te aporten energía, las 
            personas tóxicas te estarán rodeando en este fin de epoca. Abundan y es mejor que solo te alejes de ella, el si quiera 
            responder a sus ofensas te desgasta, mejor enfocate e invierte en ti.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 10,
            'content'=> 'Exiges demasiado en el aspecto físico de la persona con la que quieres estar, pero al mismo tiempo 
            tu no ofreces lo mismo ponte las pilas y activate en alguna actividad física te despejas del trabajo y además te 
            pones en forma, sabes que perdiste un poco el camino últimamente pero utiliza el fin de año para replantearte una 
            nueva estrategía y verás como el siguiente año si será tuyo.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 11,
            'content'=> 'Si atravisas por mucho estrés ultimamente es por tu personalidad apresiva, no te tomes tan apecho el trabajo,
            deja esos habitos alimenticios ya que te estan trayendo problemas de salud nada grave pero no dejes que estos empeoren.
            Tus finanzas no van a andar muy bien por estos días utiliza los bonos de fin de año para ahorrar, de momento no es tiempo 
            de gastar, puede que el proximo año ya te puedas dar tus lujitos pero mientras tanto, abstente.',
            'fecha'=> '2016-11-09'
        ]);

        HoroscopeDay::create([
            'horoscope_id'=> 12,
            'content'=> 'La rutina no va con tu personalidad pero el dejar de ir al trabajo no es opción para ti en este momento, 
            para salirte de la rutina tomate descansos de 5 minutos a solas, las necesitas debido a lo agotador que han sido estos últimos días,
            no lleves tus problemas a casa y verás como la relación con tu pareja mejorá.',
            'fecha'=> '2016-11-09'
        ]);


    }
   
}
